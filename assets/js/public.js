$( document ).ready(function() {
    var url = window.location.origin;
    
    $(function () {
        var pageLength = $('.table-sort').attr('data-pagelength') ? $('.table-sort').attr('data-pagelength') : 25;
        var leftColumns = $('.table-sort').attr('data-leftcolumns') ? $('.table-sort').attr('data-leftcolumns') : 2;
        $(".table-sort").DataTable({
            "pageLength" : Number(pageLength),
            deferRender:    true,
            // scrollY:        300,
            scrollX:        true,
            scrollCollapse: true,
            fixedColumns : {
                leftColumns : leftColumns
            },
            // scroller:       true,
            // fixedColumns: true,
        })
    });
    // $('.dropdown-toggle').dropdown()
        
    var color = ["default", "success", "warning", "warning", "danger", 'warning', 'info'];
    $(function() {
        for (var i = 0; i <= 6; i++)
        {
            $('select[name*="status"]').filter(function() {
                    return $(this).val() == (i);
            }).addClass('btn-'+color[i]);      // $("select[name*='status'][value="+(i+1)+"]").addClass('btn-'+(color[i-1]))
        }
    });

    $("select[data-name*='status'], select[name*='status']").change(function() {
        var id = parseInt($(this).val());
        for (var i = color.length - 1; i >= 0; i--)
        {
            $(this).removeClass('btn-'+color[i]);
        }
        $(this).addClass('btn-'+(color[id]));

        var myurl = window.location.href.split("/");
        var dataString = 
        {
            'data':
            [
                {
                    'name' : $(this).attr('name'),
                    'status' : $(this).val(),
                    'tahun' : $('input[name=tahun]').val(),
                    'guru_matpel_id' : myurl.pop()
                }
            ]
        };
        toastr.info('Please wait');
        $.post(url+'/absensi/public/ajax/update_absen', dataString, function(data, textStatus, xhr) {
            toastr.success('Data has been updated');
            $('#cek').html(data);
        });
    });


    $("select[data-name*='-1']").change(function() {
        var bulan = $(this).attr("data-month");
        var date = $(this).attr("data-date");
        var hours = $(this).attr("data-hours");
        var id = $(this).val();
        if (id >= 0)
        {
            $('select[name*="status['+bulan+']['+date+']['+hours+']"').val(id);
            for (var i = color.length - 1; i >= 0; i--)
            {
                $('select[name*="status['+bulan+']['+date+']['+hours+']"').removeClass('btn-'+color[i]);
            }
            $('select[name*="status['+bulan+']['+date+']['+hours+']"').addClass('btn-'+(color[id]));
        }
        var myurl = window.location.href.split("/");
        var guru_matpel_id = myurl.pop();
        var dataString = 
        {
            'data': []
        };

        $('select[name*="status['+bulan+']['+date+'"]').each(function(){
            dataString.data.push({
                    'name' : $(this).attr('name'),
                    'status' : $(this).val(),
                    'tahun' : $('input[name=tahun]').val(),
                    'guru_matpel_id' : guru_matpel_id
                })
        });

        toastr.info('Please wait');
        $.post(url+'/absensi/public/ajax/update_absen', dataString, function(data, textStatus, xhr) {
            toastr.success('Data has been updated');
            $('#cek').html(data);
        });
    });

    $('#detailAbsensi').click(function() {
        var guru_matpel_id = $(this).attr('data-guru-matpel-id');
        var tanggal = $(this).attr('data-tanggal');
        var jadwal = $(this).attr('data-jadwal');
        var dataString = {
            'guru_matpel_id' : guru_matpel_id,
            'tanggal' : tanggal,
            'jadwal' : jadwal,
        }
        $.post(url+'/absensi/public/ajax/get_berita_acara', dataString, function(data, textStatus, xhr) {
            
        });
    });

});