# Absensi

### Version
0.6 Beta - Clean Build (The code obviously)

### Resource Used
1. Back-end Framework : [Codeigniter] v3.0.4
2. Front-end Framework : [Bootstrap] v3.3.6
3. Libraries : [Ion Auth] 2.6.0 , [Codeigniter Template]
4. Other : [Codeigniter MY Model] v3

### How to Deploy [v 0.1 beta]
1. Download zip file here : [zip]
2. Dowbload sql file here : [sql]
2. Extract zip on your server
3. Execute sql file
3. Edit hostname in /application/config/config.php
4. Edit database details in /application/config/database.php

### Initial Feature Request
  - Absensi terintegrasi
  - Guru bisa mengisi absensi
  - Siswa bisa liat prosentase kehadiran dan deskripsi dari kehadiran
  - Kegiatan termasuk : KBM, Ekskul, Mabit, Solat Ashar

### Implemented
- Database terintegrasi untuk siswa, guru dan mata pelajaran
- Jumlah kehadiran dengan deskripsi per tanggal
- Manage absensi untuk guru
- _3 Level Authority_
    - Admin
        - Edit guru
        - Edit siswa
        - Edit list matpel
        - Edit hubungan guru dan matpel
        - Edit absensi
        - Melihat Kehadiran dan deskripsi
    - Guru
        - Edit akun miliknya
        - Edit hubungan guru dan matpel miliknya
        - Edit absensi sesuai dengan jadwal yang telah ditentukan
        - Melihat kehadiran dirinya dan anggota kelas lainnya dalam masing masing matpel
    - Siswa
        - Edit akun miliknya
        - Melihat persentase kehadiran dirinya
        - Melihat deskripski kehadiran dirinya

#### Beta Milestone
    - CRUD dengan konfirmasi
    - TU Manager (optional)
    - Kegiatan
    - Multiple date dan kegiatan bulanan
    
#### Final Feature Request
    - Suggest Edit
    - Request manage kegiatan

## Changelog Resume
- [0.1 Beta] : 
    - Initial upload, basic function
- [0.2 Beta] :
    - UI upgrade
    - Code re-built for better extension support
    - Change [Codeigniter base model] by jamierumbelow to [Codeigniter MY Model] by avenirer
    - 80% documented code
    - Several Bug fix and improvement
- [0.5 Beta - Dirty Build] :
    - Pretty much code rework
    - Now support extending Controller for additional auth
    - Some Database update to fit jadwal with hours
    - Major Database update for Data Siswa
    - Add admin section with (hopefully) full-functionality
    - Change absensi system for Guru (now they can only absen when its time)
    - Change view absensi system for Guru and Siswa (table rework)
    - ~30% Documented Code
    - Minor ui improvement, like coloring status hadir :D, 
- [0.6 Beta - Clean Build] :
    - Delete many unused function
    - Merge many duplicate function
    - 95% Documented main code (Controller)
    - Bugfix and improvement
    - New kelas system
    - Add handler for tahun ajaran
    - Add handler for semester

[zip]: <https://goo.gl/LrWqmR>
[sql]: <https://goo.gl/CCGXCd>
[codeigniter]: <https://github.com/bcit-ci/CodeIgniter>
[bootstrap]: <https://github.com/twbs/bootstrap>
[codeigniter base model]: <https://github.com/jamierumbelow/codeigniter-base-model>
[codeigniter my model]: <https://github.com/avenirer/CodeIgniter-MY_Model>
[ion auth]: <https://github.com/benedmunds/CodeIgniter-Ion-Auth>
[codeigniter template]: <https://github.com/jenssegers/codeigniter-template-library>