<?php defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Install_ion_auth extends CI_Migration
{
  public function up()
  {
    // Drop table 'group' if it exists
    $this->dbforge->drop_table('group', true);

    // Table structure for table 'group'
    $this->dbforge->add_field(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'auto_increment' => true
      ),
      'name' => array(
        'type' => 'VARCHAR',
        'constraint' => '20',
      ),
      'description' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      )
    ));
    $this->dbforge->add_key('id', true);
    $this->dbforge->create_table('group');

    // Dumping data for table 'group'
    $data = array(
      array(
        'id' => '1',
        'name' => 'admin',
        'description' => 'Administrator'
      ),
      array(
        'id' => '2',
        'name' => 'siswa',
        'description' => 'Siswa'
      ),
      array(
        'id' => '3',
        'name' => 'guru',
        'description' => 'Guru'
      )
    );
    $this->db->insert_batch('group', $data);


    // Drop table 'account' if it exists
    $this->dbforge->drop_table('account', true);

    // Table structure for table 'account'
    $this->dbforge->add_field(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'auto_increment' => true
      ),
      'ip_address' => array(
        'type' => 'VARCHAR',
        'constraint' => '16'
      ),
      'username' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
      ),
      'password' => array(
        'type' => 'VARCHAR',
        'constraint' => '80',
      ),
      'salt' => array(
        'type' => 'VARCHAR',
        'constraint' => '40'
      ),
      'email' => array(
        'type' => 'VARCHAR',
        'constraint' => '100'
      ),
      'activation_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '40',
        'null' => true
      ),
      'forgotten_password_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '40',
        'null' => true
      ),
      'forgotten_password_time' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'null' => true
      ),
      'remember_code' => array(
        'type' => 'VARCHAR',
        'constraint' => '40',
        'null' => true
      ),
      'created_on' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
      ),
      'last_login' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'null' => true
      ),
      'active' => array(
        'type' => 'TINYINT',
        'constraint' => '1',
        'unsigned' => true,
        'null' => true
      )
    ));
    $this->dbforge->add_key('id', true);
    $this->dbforge->create_table('account');

    // Dumping data for table 'account'
    $data = array(
      'id' => '1',
      'ip_address' => '127.0.0.1',
      'username' => 'administrator',
      'password' => '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36',
      'salt' => '',
      'email' => 'admin@admin.com',
      'activation_code' => '',
      'forgotten_password_code' => null,
      'created_on' => '1268889823',
      'last_login' => '1268889823',
      'active' => '1',
    );
    $this->db->insert('account', $data);


    // Drop table 'account_group' if it exists
    $this->dbforge->drop_table('account_group', true);
    $foreign = '
      SET FOREIGN_KEY_CHECKS = 0;
    ';
    $sql = '
      CREATE TABLE `account_group` (
        `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
        `account_id` int(11) unsigned NOT NULL,
        `group_id` int(11) unsigned NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `uc_account_group` (`account_id`,`group_id`),
        KEY `fk_account_group_account1_idx` (`account_id`),
        KEY `fk_account_group_group1_idx` (`group_id`),
        CONSTRAINT `fk_account_group_group1` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
        CONSTRAINT `fk_account_group_account1` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ';

    $this->db->query($foreign);
    $this->db->query($sql);

    // Dumping data for table 'account_group'
    $data = array(
      array(
        'id' => '1',
        'account_id' => '1',
        'group_id' => '1',
      ),
      array(
        'id' => '2',
        'account_id' => '1',
        'group_id' => '2',
      )
    );
    $this->db->insert_batch('account_group', $data);

    // Drop table 'login_attempt' if it exists
    $this->dbforge->drop_table('login_attempt', true);

    // Table structure for table 'login_attempt'
    $this->dbforge->add_field(array(
      'id' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'auto_increment' => true
      ),
      'ip_address' => array(
        'type' => 'VARCHAR',
        'constraint' => '16'
      ),
      'login' => array(
        'type' => 'VARCHAR',
        'constraint' => '100',
        'null', true
      ),
      'time' => array(
        'type' => 'INT',
        'constraint' => '11',
        'unsigned' => true,
        'null' => true
      )
    ));
    $this->dbforge->add_key('id', true);
    $this->dbforge->create_table('login_attempt');

  }

  public function down()
  {
    $this->dbforge->drop_table('account_group', true);
    $this->dbforge->drop_table('account', true);
    $this->dbforge->drop_table('group', true);
    $this->dbforge->drop_table('login_attempt', true);
  }
}
