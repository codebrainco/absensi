<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    tr {
        height: 30px;
    }
    .btn {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Guru Badge</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <div class="col-sm-6"><b>Nama Guru <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->guru ?></div>
                        <div class="col-sm-6"><b>Kelas <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->kelas ?></div>
                        <div class="col-sm-6"><b>Mata Pelajaran <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->matpel ?></div>
                        <div class="col-sm-6"><b>Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->tahun_ajaran ?></div>
                        <div class="col-sm-6"><b>Semester <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->semester ?></div>
                        <div class="col-sm-6"><b>Tanggal Awal Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_begin_date ?></div>
                        <div class="col-sm-6"><b>Tanggal Akhir Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_end_date ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="panel panel-default">
            <div class="panel-heading">Berita Acara</div>
            <div class="panel-body">
                <div class="col-sm-12">
                    <?php echo form_open(current_url(), array('class' => 'form-horizontal')) ?>
                        <?php if (isset($input_hidden)) : ?>
                            <?php foreach ($input_hidden as $value) : ?>
                                <?php echo $value ?>
                            <?php endforeach ?>
                        <?php endif ?>
                        <?php foreach ($input as $key => $value) : ?>
                            <div class="form-group">
                                <?php echo form_label($key, '', array('class' => 'control-label')); ?>
                                <?php echo $value; ?>
                            </div>
                        <?php endforeach; ?>
                        <div class="form-group">
                            <?php echo form_submit('submit', 'Submit', array('class' => 'btn btn-success pull-right')) ?>
                        </div>
                    <?php echo form_close(); ?>
                </div>
         </div>
        </div>
    </div>
</div>

<div class="row" id="table">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Absensi</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo form_open(current_url()) ?>
                            <?php echo $form_tahun ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-condensed table-striped table-bordered table-sort" width='100%' data-pagelength="50" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="fixed" rowspan="2">No</th>
                                                <th class="fixed" rowspan="2">Siswa</th>
                                                <?php foreach ($all_dates as $bulan => $tanggal) : ?>
                                                    <th colspan="<?php echo count($tanggal) ?>"><?php echo $bulan ?></th>
                                                <?php endforeach; ?>
                                            </tr>
                                                <?php foreach ($all_dates as $bulan => $list_tanggal) : ?>
                                                    <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                                        <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                            <td class="text-center"><?php echo $all_day[$bulan][$tanggal].', '.$tanggal.' @ '.$jadwal ?></td>
                                                        <?php endforeach ?>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <!-- START mass -->
                                        <tr>
                                            <td class="warning text-center">0</td>
                                            <td class="warning">All</button></td>
                                            <?php foreach ($all_dates as $bulan => $value) : ?>
                                                <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                                    <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                        <td class="warning text-center"><select data-month="<?php echo $bulan ?>" data-date="<?php echo $tanggal ?>" data-hours="<?php echo $jadwal ?>" data-name="-1" class="btn btn-sm btn-default original-select"><option value="-1">Mass</option><option value="0">None</option><option value="1">Hadir</option><option value="2">Sakit</option><option value="3">Izin</option><option value="4">Alfa</option></select></td>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            <?php endforeach; ?>
                                        </tr>
                                        <!-- END mass -->
                                            <!-- START Foreach untuk setiap siswa -->
                                            <?php $x = 1; foreach ($all_siswa as $siswa) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $x++ ?></td>
                                                    <td><span class=""><?php echo $siswa->nama ?></span></td>
                                                    <?php foreach ($all_dates as $bulan => $value) : ?>
                                                        <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                                            <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                                <?php echo ( ! empty($absensi) ? form_hidden('id['.$bulan.']['.$tanggal.']['.$siswa->account->id.']', $absensi[$bulan][$tanggal][$jadwal][$siswa->account->id]['id']) : '') ?><!--
                                                             --><td class="text-center" ><!--
                                                                --><?php if (isset($status_dropdown[$bulan][$tanggal][$jadwal]) && ! is_array($status_dropdown[$bulan][$tanggal][$jadwal])) : ?>
                                                                        <?php echo $status_dropdown[$bulan][$tanggal][$jadwal] ?><!--
                                                                --><?php elseif (isset($status_dropdown[$bulan][$tanggal][$jadwal][$siswa->account_id])) : ?>
                                                                        <?php echo $status_dropdown[$bulan][$tanggal][$jadwal][$siswa->account_id] ?><!--
                                                                --><?php else: ?><!--
                                                                    --><select name="status[<?php echo $bulan ?>][<?php echo $tanggal ?>][<?php echo $jadwal ?>][<?php echo $siswa->account->id ?>]" class="btn btn-sm btn-default original-select"><!--
                                                                        --><?php echo $option_none; ?><!--
                                                                    --></select><!--
                                                                --><?php endif; ?><!--
                                                            --></td>
                                                            <?php endforeach; ?>
                                                        <?php endforeach; ?>
                                                    <?php endforeach; ?>
                                                </tr>
                                            <?php endforeach; ?>
                                            <!-- END Foreach untuk setiap siswa -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php echo form_close() ; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="cek">
</div>
