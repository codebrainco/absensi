
<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row">
  <div class="col-sm-6">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">Profile Badge</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-xs-12 table-responsive custom-badge">
            <?php echo $this->template->profile_badge ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title"><?php echo $this->template->title ?></div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-xs-12 table-responsive nowrap">
            <?php echo $table_matpel ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
