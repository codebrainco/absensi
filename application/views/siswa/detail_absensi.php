<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
  #manage-content
  {
    padding-top: 20px;
  }
  .profile-badge div
  {
    padding-top: 5px;
    padding-bottom: 5px;
  }
  .jadwal
  {
    padding-bottom: 10px;
  }

  .table-absen-wrapper
  {
    overflow: auto;
  }

  .table-absen
  {
    width: <?php echo count($all_dates)*100+235 ?>px !important;
  }

  .table-absen td, .table-absen th
  {
    text-align: center !important;
    vertical-align: middle !important;
  }

</style>

<div class="row" id="manage-content">
  <div class="col-sm-8 profile-badge well">
    <?php echo $this->template->guru_badge ?>
  </div>
  <div class="col-sm-4">
    <?php echo img('assets/img/profile_placeholder.svg', false, 'class="img-rounded img-responsive center-block"'); ?>
  </div>
</div>
  <div class="col-sm-3">
    <!-- BEGIN fixed left table -->
    <table class="table table-striped">
      <thead>
        <tr>
          <th class="fixed">No</th>
          <th class="fixed">Siswa</th>
        </tr>
        <tr>
          <th>&nbsp;</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>
        <?php $x = 0; foreach ($all_siswa as $siswa) : ?>
          <tr>
            <td><button class="btn btn-info"><?php echo $x+1 ?></button></td>
            <td><button class="btn btn-default"><span class="pull-left"><?php echo $siswa->nama ?></span></button></td>
          </tr>
        <?php $x++; endforeach ?>
      </tbody>
    </table>
    <!-- END fixed left table -->
  </div>
  <div class="col-sm-9 table-absen-wrapper">
    <!-- BEGIN relateive right table -->
    <table class="table table-absen table-striped">
      <thead>
        <tr>
          <!-- BEGIN all month -->
          <?php foreach ($all_dates as $bulan => $tanggal) : ?>
            <th colspan="<?php echo count($tanggal) ?>" width="300"><?php echo $bulan ?></th>
          <?php endforeach ?>
          <!-- END all month -->
        </tr>
        <tr>
          <!-- BEGIN all dates in month -->
          <?php foreach ($all_dates as $bulan => $value) : ?>
            <?php foreach ($value as $key => $tanggal) : ?>
              <th><?php echo $all_day[$bulan][$key]?>, <?php echo $tanggal ?></th>
            <?php endforeach ?>
          <?php endforeach ?>
          <!-- END all dates in month -->
        </tr>
      </thead>
      <tbody>
      <?php foreach ($all_siswa as $siswa) : ?>
        <tr>
          <!-- BEGIN absensi per tanggal -->
          <?php foreach ($all_dates as $bulan => $value) : ?>
            <?php foreach ($value as $key => $tanggal) : ?>
              <td data-original-title="<?php echo $siswa->nama ?>" data-container="body" data-tooltip="true" data-placement="bottom" title="<?php echo $siswa->nama ?>"><?php if (isset($absensi[$bulan][$tanggal])) : ?><button class="btn btn-<?php echo $absensi[$bulan][$tanggal][$siswa->account->id]['color'] ?>"><?php echo $absensi[$bulan][$tanggal][$siswa->account->id]['kehadiran']; ?></button><?php else: ?><button class="btn btn-default">None</button><?php endif; ?></td>
            <?php endforeach ?>
          <?php endforeach ?>
          <!-- END absensi per tanggal -->
        </tr>
      <?php endforeach ?>
      </tbody>
    </table>
    <!-- END relateive right table -->
  </div>
