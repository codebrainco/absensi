<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<h2><?php echo $this->template->title ?></h2>
<div class="col-xs-12 col-sm-3 col-xs-center">
  <br>
  <?php echo img('assets/img/profile_placeholder.svg', false, 'class="img-circle img-responsive center-block"'); ?>
  <br>
</div>
<?php echo form_open(current_url(), array('class' => 'form-horizontal')) ?>
<div class="col-sm-9">
  <div class="row">
    <!-- BEGIN Display message -->
    <?php foreach ($messages as $message) : ?>
      <div class="alert alert-<?php echo $message['severity_color']?> ">
        <b><?php echo $message['severity_message'] ?></b> <?php echo $message['message_body']; ?>
      </div>
    <?php endforeach ?>
    <!-- END Display message -->
    <!-- BEGIN Personal Info -->
    <h3>Personal info</h3>
    <div class="col-sm-8">
      <?php foreach ($input_personal as $key => $value) : ?>
        <div class="form-group">
            <?php echo form_label($key, '', array('class' => 'col-sm-3 control-label')); ?>
          <div class="col-sm-9">
            <?php echo $value; ?>
          </div>
        </div>
      <?php endforeach ?>
    </div>
    <!-- END Personal Info -->
  </div>
  <div class="row">
    <!-- BEGIN Account Info -->
    <h3>Account info</h3>
    <div class="col-sm-8">
      <?php foreach ($input_account as $key => $value) : ?>
        <div class="form-group">
            <?php echo form_label($key, '', array('class' => 'col-sm-3 control-label')); ?>
          <div class="col-sm-9">
            <?php echo $value; ?>
          </div>
        </div>
      <?php endforeach ?>
      <button name="submit_edit" type="submit" class="btn btn-primary pull-right">Save Changes</button>
      <span style="color:blue">Fill Password &amp; Password Confirmation ONLY if you want to change password</span>
    </div>
    <!-- END Account Info -->
  </div>
</div>
<?php echo form_close() ?>
