<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
  .profile-badge div
  {
    padding-top: 5px;
    padding-bottom: 5px;
  }
</style>
<h2><?php echo $this->template->title ?></h2>

<div class="row">
  <div class="col-sm-8 profile-badge well">
    <?php echo $this->template->profile_badge ?>
  </div>
  <div class="col-sm-4">
    <?php echo img('assets/img/profile_placeholder.svg', false, 'class="img-rounded img-responsive center-block"'); ?>
  </div>
</div>
<div class="row">
  <div class="col-sm-8 responsive-table">
    <?php echo $table_matpel ?>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="deskHadirModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalLabel">Detail</h4>
      </div>
      <div class="modal-body">
        <table class="table">
          <thead>
            <th>Tanggal</th>
            <th>Status</th>
          </thead>
          <tbody id="isiHadir"></tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
