<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <div class="col-sm-6"><b>Nama <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $siswa->nama ?></div>
                        <div class="col-sm-6"><b>NISN <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $siswa->nisn ?></div>
                        <div class="col-sm-6"><b>Kelas <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $siswa->kelas ?></div>
                        <div class="col-sm-6"><b>Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $ta_aktif->tahun_ajaran ?></div>
                        <div class="col-sm-6"><b>Semester <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $ta_aktif->semester ?></div>
                        <div class="col-sm-6"><b>Tanggal Awal Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $ta_aktif->begin_date ?></div>
                        <div class="col-sm-6"><b>Tanggal Akhir Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6">&nbsp;<?php echo $ta_aktif->end_date ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
