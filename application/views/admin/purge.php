<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    tr {
        height: 30px;
    }
    .btn {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<?php echo form_open(current_url()); ?>
<p><b>Warning!</b> Semua data yang di purge adalah tahun ajaran yang sedang aktif (implementasi pada guru_matpel)</p>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Siswa</h3>
                <p>Semua data siswa termasuk : profil, akun, absensi</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/siswa') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Guru</h3>
                <p>Semua data guru termasuk : profil, akun, guru_matpel, jadwal_piket, absensi</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/guru') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Kelas</h3>
                <p>Semua data kelas termasuk : guru_matpel &amp; kelas lintas minat</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/kelas') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Guru-Matpel</h3>
                <p>Semua data guru_matpel termasuk guru_matpel lintas minat</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/guru_matpel') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Jurusan</h3>
                <p>Semua data jurusan termasuk : kelas</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/jurusan') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-body">
                <h3>Mata Pelajaran</h3>
                <p>Semua data mata pelajaran termasuk : guru_matpel</p>
                <div class="text-center"><a href="<?php echo base_url('admin/purge/kelas') ?>" class="btn btn-default red white-text">Purge</a></div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
