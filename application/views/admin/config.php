<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($sct_conf['section_column'])) {
    if (is_numeric($sct_conf['section_column'])) {
        $column = $sct_conf['section_column'];
    } elseif ($sct_conf['section_column'] == 'full') {
        $column = 12;
    } elseif ($sct_conf['section_column'] == 'half') {
        $column = 6;
    }
} else {
    $column = 12;
}

if (isset($sct_conf['section_title'])) {
    $title = $sct_conf['section_title'];
} else {
    $title = $this->template->title;
}
?>
<style>
    tr {
        height: 30px;
    }
    .btn {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<?php echo form_open(current_url()); ?>
<div class="row" id="table">
    <div class="col-sm-<?php echo $column; ?>">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"><?php echo $title ?></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <?php if (isset($interaction) || isset($filter)) : ?>
                        <?php if (isset($filter)) : ?>
                            <div class="col-xs-12" style="margin-bottom: 0;">
                                <?php echo form_open(current_url(), array('method' => 'get', 'class' => 'form-inline')) ?>
                                    <?php foreach ($filter as $heading => $dropdown) : ?>
                                            <div class="form-group">
                                                <?php echo $dropdown ?>
                                            </div>
                                    <?php endforeach ?>
                                    <button type="submit" class="btn btn-sm btn-primary">Do Filter !</button>
                                    <a href="<?php echo $site_url ?>" class="btn btn-sm btn-default btn-space">Hapus Filter</a>
                                    <?php echo isset($interaction) ? $interaction : '' ?>
                                <?php echo form_close() ?>
                            </div>
                        <?php else: ?>
                            <div class="col-xs-12" style="margin-bottom: 0;">
                                <?php echo isset($interaction) ? $interaction : '' ?>
                            </div>
                        <?php endif; ?>
                    <?php endif ?>
                    <div class="col-xs-12"><br></div>
                    <div class="col-xs-12 table-responsive nowrap">
                        <?php echo $this->template->prefix ?>
                        <hr>
                        <?php echo $table ?>
                        <hr>
                        <?php echo $this->template->postfix ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>
