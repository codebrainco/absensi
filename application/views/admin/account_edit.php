<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php echo form_open(current_url(), array('class' => '')) ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading indigo white-text">Akun</div>
                <div class="panel-body">
                    <?php foreach ($input_account as $key => $value) : ?>
                        <div class="form-group">
                            <?php echo form_label($key, ''); ?>
                            <?php echo $value ?>
                        </div>
                    <?php endforeach ?>
                </div><!-- end div.panelbody -->
            </div><!-- end div.panel.panel-default -->
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h3>Aksi</h3>
                        <button name="submit" type="submit" class="btn btn-success">Simpan</button>
                        <?php echo isset($interaction) ? $interaction : '' ?>
                    </div>
                    <div class="col-xs-12">
                        <h3>Status</h3>
                        <?php foreach ($groups as $group) : ?>
                            <a class="btn blue white-text"><?php echo $group ?></a>
                        <?php endforeach ?>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        <br>
                    </div>
                </div><!-- end div.panelbody -->
            </div><!-- end div.panel.panel-default -->
        </div>
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading indigo white-text">Profil</div>
                <div class="panel-body">
                    <?php foreach ($input_personal as $key => $value) : ?>
                        <div class="form-group col-sm-6">
                            <?php echo form_label($key, ''); ?>
                            <?php if ($key == 'Tinggi') : ?>
                                <div class="input-group">
                                    <?php echo $value; ?>
                                    <span class="input-group-addon">cm</span>
                                </div>
                            <?php elseif ($key == 'Berat') : ?>
                                <div class="input-group">
                                    <?php echo $value; ?>
                                    <span class="input-group-addon">kg</span>
                                </div>
                            <?php else: ?>
                                <?php echo $value; ?>
                            <?php endif; ?>
                        </div>
                    <?php endforeach ?>
             </div><!-- end div.panelbody -->
            </div><!-- end div.panel.panel-default -->
        </div>
    </div>
<?php echo form_close() ?>
