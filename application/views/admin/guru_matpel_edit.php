<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php echo form_open(current_url(), array('class' => '')) ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading indigo white-text">Guru Matpel</div>
                <div class="panel-body">
                    <?php foreach ($input as $key => $value) : ?>
                        <div class="form-group">
                            <?php echo form_label($key, ''); ?>
                            <?php echo $value ?>
                        </div>
                    <?php endforeach ?>
                </div><!-- end div.panelbody -->
            </div><!-- end div.panel.panel-default -->
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="col-xs-12">
                        <h3>Aksi</h3>
                        <button name="submit" type="submit" class="btn btn-success">Simpan</button>
                        <?php echo isset($interaction) ? $interaction : '' ?>
                    </div>
                    <div class="col-xs-12">
                        <h3>Edit</h3>
                        <?php echo isset($derived_edit) ? $derived_edit : '' ?>
                    </div>
                    <div class="col-xs-12">
                        <br>
                        <br>
                    </div>
                </div><!-- end div.panelbody -->
            </div><!-- end div.panel.panel-default -->
        </div>
    </div>
<?php echo form_close() ?>
