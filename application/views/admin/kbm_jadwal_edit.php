<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    td input
    {
        display: inline-block !important;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Guru Badge</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <?php echo $this->template->guru_badge ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="table">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Absensi</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo form_open(current_url()) ?>
                            <div class="row">
                                <div class="col-sm-12 table-responsive nowrap text-center">
                                    <?php echo $table_jadwal ?>
                                    <button name="submit" type="Simpan" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        <?php echo form_close() ; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
