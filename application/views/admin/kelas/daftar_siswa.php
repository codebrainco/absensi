<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Kelas Badge</div>
                </div>
            </div>
            <div class="card-body">
            <div class="row">
                <div class="col-xs-12 custom-badge">
                    <?php echo $this->template->kelas_badge ?>
                </div>
            </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Tambah Siswa</div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-xs-12">
                    <p><b>Warning !</b> siswa yang yang ditambahkan akan dipindahkan dari kelas sebelumnya</p>
                </div>
                <div class="col-xs-12">
                    <?php echo form_open(current_url(), array('class' => 'form-horizontal',)) ?>
                        <div class="form-group">
                            <?php echo $form_siswa ?>
                        </div>
                        <div class="form-group">
                        <?php echo form_submit('submit', 'Submit', array('class' => 'form-control',)) ?>
                        </div>
                    <?php echo form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Siswa</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12 table-responsive nowrap">
                        <?php echo $table_siswa ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
