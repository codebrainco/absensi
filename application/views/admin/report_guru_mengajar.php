<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"><?php echo $this->template->title ?></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <?php echo form_open(current_url(), array('method' => 'get', 'class' => 'form-inline')) ?>
                            <?php foreach ($filter as $heading => $dropdown) : ?>
                                    <div class="form-group">
                                        <?php echo $dropdown ?>
                                    </div>
                            <?php endforeach ?>
                            <button type="submit" class="btn btn-sm btn-primary">Do Filter !</button>
                            <a href="<?php echo base_url('admin/report') ?>" class="btn btn-sm btn-default btn-space">Clear Filter</a>
                        <?php echo form_close() ?>
                    </div>
                    <div class="col-xs-12">
                        <hr>
                    </div>
                    <div class="col-xs-12 table-responsive nowrap">
                        <?php echo $table_report ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
