<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php echo form_open(current_url(), array('class' => '')) ?>
  <div class="row">
    <div class="col-sm-6">
      <div class="panel panel-default">
        <div class="panel-heading">Guru-matpel</div>
        <div class="panel-body">
          <?php foreach ($input_add as $key => $value) : ?>
            <div class="form-group">
              <?php echo form_label($key, ''); ?>
              <?php if ($key == 'Tinggi') : ?>
                <div class="input-group">
                  <?php echo $value; ?>
                  <span class="input-group-addon">cm</span>
                </div>
              <?php elseif ($key == 'Berat') : ?>
                <div class="input-group">
                  <?php echo $value; ?>
                  <span class="input-group-addon">kg</span>
                </div>
              <?php else: ?>
                <?php echo $value; ?>
              <?php endif; ?>
            </div>
          <?php endforeach ?>
       </div><!-- end div.panelbody -->
      </div><!-- end div.panel.panel-default -->
    <button name="submit" type="submit" class="btn btn-block btn-primary pull-right">Submit</button>
    </div>
  </div>
<?php echo form_close() ?>
