<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row">
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Upload</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="sub-title">WARNING!</div>
                        <p>Fitur ini memerlukan kehatihatian ! kesalahan dalam penginputan data dapat berakibat fatal</p>
                        <?php echo form_open_multipart(current_url());?>
                        <div class="form-group">
                            <label for="upload">Type</label>
                            <select name="type" id="label" class="form-control">
                                <!-- <option value="1">Type 1</option> -->
                                <option value="guru_1">Guru | Type 1</option>
                                <option value="siswa_2">Siswa | Type 2</option>
                                <option value="jadwal_1">Jadwal | Type 1</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="massUpdateFile">File input</label>
                            <input type="file" id="massUpdateFile" name="massUpdateFile">
                        </div>
                        <div class="form-group text-center">
                            <button name="submit" type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <?php echo form_close() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Type</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
<!--                     <div class="col-xs-12">
                        <div class="sub-title">Type 1</div>
                        <p>Tipe 1 merupakan jenis form minimalis yang hanya memerlukan nama, angkatan dan tanggal lahir. Untuk kebutuhan login, maka data account termasuk username, email dan password akan dijadikan default</p>
                        <p>Username : kata pertama dari nama + angkatan + no urut | Ex : abdul150001</p>
                        <p>email : admin@sma-almuttaqin-tasikmalaya.sch.id</p>
                        <p>password : tanggal lahir | Ex : 29021998</p>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th>Property</th>
                                <th>Format</th>
                                <th>Example</th>
                            </tr>
                            <tr>
                                <td>nama</td>
                                <td>{string[255]}</td>
                                <td>Jonathan Scott</td>
                            </tr>
                            <tr>
                                <td>angkatan</td>
                                <td>{int[4]}/{int[4]}</td>
                                <td>2012/2013</td>
                            </tr>
                            <tr>
                                <td>tanggal_lahir</td>
                                <td>{dd/mm/yyyy}</td>
                                <td>29/02/1908</td>
                            </tr>
                        </table>
                        <p><a href="<?php echo asset_url('doc/account_data_minimal_v1.xls') ?>" class="btn btn-success">Download Type 1</a></p>
                    </div> -->
                    <div class="col-xs-12">
                        <div class="sub-title">Siswa Type 2</div>
                        <p>Tipe 2 merupakan jenis form simpel yang memerlukan nis, nama, jenis kelamin, kelas. Untuk kebutuhan login, maka data account termasuk username, email dan password akan dijadikan default</p>
                        <p><b>Warning!</b> Setiap data harus terisi, tidak boleh kosong</p>
                        <p><b>Warning!</b> Pastikan kelas telah terdaftar</p>
                        <p><b>Warning!</b> Pastikan format sama</p>
                        <p>Username : kata pertama dari nama + 6 digit terakhir nis | Ex : abdul610003</p>
                        <p>email : admin@sma-almuttaqin-tasikmalaya.sch.id</p>
                        <p>password : nis | Ex : 151610003</p>
                        <table class="table table-bordered table-condensed">
                            <tr>
                                <th>Property</th>
                                <th>Format</th>
                                <th>Example</th>
                            </tr>
                            <tr>
                                <td>nis</td>
                                <td>{int[9]}</td>
                                <td>151610003</td>
                            </tr>
                            <tr>
                                <td>nama</td>
                                <td>{string[255]}</td>
                                <td>Jonathan Scott</td>
                            </tr>
                            <tr>
                                <td>jenis_kelamin</td>
                                <td>{char[L|P]}</td>
                                <td>L</td>
                            </tr>
                            <tr>
                                <td>kelas</td>
                                <td>{tingkat-jurusan-kelas}</td>
                                <td>10-IIS-1</td>
                            </tr>
                        </table>
                        <p><a href="<?php echo asset_url('doc/account_data_minimal_v2.xls') ?>" class="btn btn-success">Download Type 2</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
