<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?php echo $this->template->title ?></title>
  <!-- Template -->
  <?php
    echo $this->template->meta;
    echo $this->template->stylesheet;
    echo $this->template->favicon;
    ?>
</head>
<body>
  <?php
    echo $this->template->content;
    echo $this->template->javascript;
    ?>
</body>
</html>
