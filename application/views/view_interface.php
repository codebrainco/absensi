<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>


  #btnLogin
  {
    float:left;
  }
  #sideLink
  {
    padding-top: 10px;
    float:right;
  }

  .container
  {
    padding-top: 50px;
  }
</style>

<div class="container">
  <div class="row vertical-center">
    <div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Interface Setup</h3>
        </div>
        <div class="panel-body">
          <div class="row">
            <div class="col-xs-12 text-center">
              <a href="<?php echo $admin ?>" class="btn btn-primary">Admin</a>
              <a href="<?php echo $group ?>" class="btn btn-success">Normal</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
