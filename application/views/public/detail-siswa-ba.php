<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    tr {
        height: 30px;
    }
    .btn {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12 custom-badge">
            <?php echo $this->template->siswa_badge ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12 custom-badge">
            <?php echo $this->template->ta_badge ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12 table-responsive">
            <table data-pagelength="50" class="table table-condensed table-striped table-bordered table-sort" width='100%' data-leftColumns="1">
              <thead>
                <tr>
                  <th class="text-center" style="vertical-align:middle" rowspan="2">No</th>
                  <!-- <th class="text-center" style="vertical-align:middle" rowspan="2">Pertemuan-ke</th> -->
                  <!-- <th class="text-center" style="vertical-align:middle" rowspan="2">Total Pertemuan</th> -->
                  <th class="text-center" colspan="3">Jadwal</th>
                  <th class="text-center" style="vertical-align:middle" rowspan="2">Materi</th>
                  <th class="text-center" style="vertical-align:middle" rowspan="2">Tugas Siswa</th>
                  <th class="text-center" style="vertical-align:middle" rowspan="2">Kehadiran</th>
                </tr>
                <tr>
                  <td class="text-center">Tanggal</td>
                  <td class="text-center">Hari</td>
                  <td class="text-center">Pukul</td>
                </tr>
              </thead>
              <tbody>
                <?php $x = 0; foreach ($all_pertemuan as $pertemuan) : ?>
                  <tr>
                    <td class="text-center"><?php echo $x+1 ?></td>
                    <td><span class="pull-left"><?php echo $pertemuan['tanggal']; ?></span></td>
                    <td><span class="pull-left"><?php echo $pertemuan['hari']; ?></span></td>
                    <td><span class="pull-left"><?php echo $pertemuan['pukul']; ?></span></td>
                    <td><span class="pull-left"><?php echo $pertemuan['materi']; ?></span></td>
                    <td><span class="pull-left"><?php echo $pertemuan['tugas_siswa']; ?></span></td>
                    <td><span class="pull-left"><?php echo $pertemuan['kehadiran']; ?></span></td>
                  </tr>
                <?php $x++; endforeach ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
