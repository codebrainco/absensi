<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
    tr {
        height: 30px;
    }
    .btn {
        margin-top: 0 !important;
        margin-bottom: 0 !important;
    }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Guru Badge</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <?php echo $this->template->guru_badge ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Other Information</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <?php foreach ($guru_matpel->jadwal as $key => $value) : ?>
                            <div class="col-sm-6"><b>Jadwal <span class="pull-right">:</span> </b></div><div class="col-md-6"><?php echo $value['hari'].', '.$value['begin_hrs'].' - '.$value['end_hrs'] ?></div>
                        <?php endforeach ?>
                        <div class="col-sm-6"><b>Total Pertemuan <span class="pull-right">:</span> </b></div><div class="col-md-6"><?php echo $guru_matpel->total_pertemuan ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Rekapan</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table data-pagelength="50" class="table table-condensed table-striped table-bordered table-sort" width='100%'>
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">No</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">NIS</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Nama Siswa</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Persentase<br>Per <?php $tanggal = new DateTime(); echo $tanggal->format('d M Y') ?></th>
                                    <th class="text-center" colspan="5">Kehadiran</th>
                                </tr>
                                <tr>
                                    <td class="text-center">H</td>
                                    <td class="text-center">S</td>
                                    <td class="text-center">I</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">T</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 0; foreach ($all_siswa as $siswa) : ?>
                                    <tr>
                                        <td><?php echo $x+1 ?></td>
                                        <td><span class="pull-left"><?php echo $siswa->nis; ?></span></td>
                                        <td><span class="pull-left"><?php echo $siswa->nama; ?></span></td>
                                        <td class="text-center"><?php echo $all_properties['persentase'][$siswa->account_id]; ?>%</td>
                                        <td class="text-center"><?php echo $all_properties['Hadir'][$siswa->account_id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Sakit'][$siswa->account_id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Izin'][$siswa->account_id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Alfa'][$siswa->account_id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Terlambat'][$siswa->account_id]; ?></td>
                                    </tr>
                                <?php $x++; endforeach ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row" id="table">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Detail</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <form action="<?php echo current_url().'#table' ?>" method="get">
                            <div class="col-sm-6">
                                <?php echo $available_bulan ?>
                            </div>
                            <div class="col-sm-6">
                                <?php echo form_submit('', 'Submit', array('class' => 'form-control btn-primary',)) ?>
                            </div>
                        </form>
                    </div>
                    <div class="col-sm-12 custom-badge table-responsive">
                        <table class="table table-absen table-striped table-bordered table-condensed">
                            <thead>
                                <tr>
                                    <th class="fixed" rowspan="2">No</th>
                                    <th class="fixed" rowspan="2">NIS</th>
                                    <th class="fixed" rowspan="2">Siswa</th>
                                    <?php foreach ($all_dates as $bulan => $tanggal) : ?>
                                        <th class="no-wrap text-center" colspan="<?php echo count($tanggal) ?>" width="300"><?php echo $bulan ?></th>
                                    <?php endforeach ?>
                                </tr>
                                <tr>
                                    <?php foreach ($all_dates as $bulan => $list_tanggal) : ?>
                                        <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                            <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                    <td class="text-center"><?php echo $all_day[$bulan][$tanggal][$key].', '.$tanggal.' @ '.$jadwal ?></td>
                                                <?php endforeach ?>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 1; foreach ($all_siswa as $siswa) : ?>
                                    <tr>
                                        <td><button class="btn btn-xs btn-info"><?php echo $x++ ?></button></td>
                                        <td><button class="btn btn-xs btn-default"><?php echo $siswa->nis; ?></button></td>
                                        <td><?php echo $siswa->nama; ?></td>
                                        <?php foreach ($all_dates as $bulan => $value) : ?>
                                            <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                                <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                        <td class="text-center"><!--
                                                            --><?php if (isset($absensi[$bulan][$tanggal][$jadwal]) && isset($absensi[$bulan][$tanggal][$jadwal]['kehadiran'])) : ?><!--
                                                                --><button class="btn btn-xs btn-<?php echo $absensi[$bulan][$tanggal][$jadwal]['color'] ?>"><?php echo $absensi[$bulan][$tanggal][$jadwal]['kehadiran']; ?></button><!--
                                                            --><?php elseif (isset($absensi[$bulan][$tanggal][$jadwal][$siswa->account_id])) : ?><!--
                                                                --><button class="btn btn-xs btn-<?php echo $absensi[$bulan][$tanggal][$jadwal][$siswa->account->id]['color'] ?>"><?php echo $absensi[$bulan][$tanggal][$jadwal][$siswa->account->id]['kehadiran']; ?></button><!--
                                                            --><?php else: ?><!--
                                                                --><button class="btn btn-xs btn-default">None</button><!--
                                                            --><?php endif; ?><!--
                                                        --></td>
                                                <?php endforeach ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </tr>
                                <?php endforeach ?>
                                <tr>
                                    <td><button class="btn btn-xs btn-info">-</button></td>
                                    <td colspan="2"><button class="btn btn-xs btn-default"><span class="pull-left">Berita Acara</span></button></td>
                                    <?php foreach ($all_dates as $bulan => $value) : ?>
                                        <?php foreach ($list_tanggal as $tanggal => $list_jadwal) : ?>
                                            <?php foreach ($list_jadwal as $key => $jadwal) : ?>
                                                <td class="text-center"><button data-toggle="modal" data-target="#beritaAcaraModal" class="btn btn-xs btn-primary" data-guru-matpel-id="<?php echo $guru_matpel->id ?>" data-tanggal="<?php echo $tanggal.'-'.$bulan.'-'.$tahun ?>" data-jadwal="<?php echo $jadwal ?>" id="detailAbsensi">Detail</button></td>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    <?php endforeach ?>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="beritaAcaraModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Berita Acara</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Tanggal</div>
                            <div id="panelTanggal" class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Jam</div>
                            <div id="panelJam" class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Materi</div>
                            <div id="panelMateri" class="panel-body">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Tugas Siswa</div>
                            <div id="panelTugas" class="panel-body">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            <button class="btn btn-xs btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
