<?php
defined('BASEPATH') or exit('No direct script access allowed');
if (isset($sm_column)) {
    if (is_numeric($sm_column)) {
        $column = $sm_column;
    } elseif ($sm_column == 'normal') {
        $column = 8;
    } elseif ($sm_column == 'half') {
        $column = 6;
    }
} else {
    $column = 5;
}

if (isset($section_title)) {
    $title = $section_title;
} else {
    $title = $this->template->title;
}
?>
<?php if ( ! isset($norow) || $norow == 0) : ?>
<div class="row">
<?php endif ?>
<div class="col-sm-<?php echo $column ?> col-md-6">
    <div class="panel panel-default">
        <div class="panel-heading"><?php echo $title ?></div>
        <div class="panel-body">
            <div class="col-sm-12">
                <?php echo form_open(current_url(), array('class' => 'form-horizontal')) ?>
                    <?php if (isset($input_hidden)) : ?>
                        <?php foreach ($input_hidden as $value) : ?>
                            <?php echo $value ?>
                        <?php endforeach ?>
                    <?php endif ?>
                    <?php foreach ($input as $key => $value) : ?>
                        <div class="form-group">
                            <?php echo form_label($key, '', array('class' => 'control-label')); ?>
                            <?php echo $value; ?>
                        </div>
                    <?php endforeach; ?>
                    <div class="form-group">
                        <?php echo form_submit('submit', 'Simpan', array('class' => 'btn btn-success pull-right')) ?>
                    </div>
                <?php echo form_close(); ?>
            </div>
     </div>
    </div>
</div>
<?php if ( ! isset($norow) || $norow == 0) : ?>
</div>
<?php endif ?>
