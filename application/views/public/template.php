<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $this->template->title ?></title>

    <!-- Template -->
    <?php
        echo $this->template->meta;
        echo $this->template->stylesheet;
        echo $this->template->favicon;
        ?>
</head>
<body class="flat-green">
    <div class="app-container">
        <div class="row content-container">
            <nav class="navbar navbar-default navbar-fixed-top navbar-top">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-expand-toggle">
                            <i class="fa fa-bars icon"></i>
                        </button>
                        <ol class="breadcrumb navbar-breadcrumb hidden-xs">
                        <?php if (isset($breadcrumbs)) : ?>
                            <?php foreach ($breadcrumbs as $key => $breadcrumb) : ?>
                                <li><?php echo $breadcrumb ?></li>
                            <?php endforeach ?>
                        <?php endif ?>
                        </ol>
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-th icon"></i>
                        </button>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <button type="button" class="navbar-right-expand-toggle pull-right visible-xs">
                            <i class="fa fa-times icon"></i>
                        </button>
                        <li class="dropdown profile">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><?php echo $profile->nama ?> <span class="caret"></span></a>
                            <ul class="dropdown-menu animated fadeInDown">
                                <li>
                                    <div class="profile-info">
                                        <h4 class="username"><?php echo $profile->nama ?></h4>
                                        <p><?php echo $this->session->email ?></p>
                                        <div class="btn-group margin-bottom-2x" role="group">
                                            <?php if ($profile->group == 'admin') : ?>
                                                <a href="<?php echo base_url('/'.$profile->group_not_admin) ?>" type="button" class="btn btn-default"><i class="fa fa-user"></i> Change</a>
                                            <?php endif ?>
                                            <a href="<?php echo base_url($profile->group.'/logout') ?>" type="button" class="btn btn-default"><i class="fa fa-sign-out"></i> Logout</a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- Sidebar Menu -->
            <div class="side-menu sidebar-inverse">
                <nav class="navbar navbar-default" role="navigation">
                    <div class="side-menu-container">
                        <div class="navbar-header">
                            <a class="navbar-brand" href="#">
                                <div class="icon fa fa-circle-thin"></div>
                                <div class="title">Absensi</div>
                            </a>
                            <button type="button" class="navbar-expand-toggle pull-right visible-xs">
                                <i class="fa fa-times icon"></i>
                            </button>
                        </div>
                        <ul class="nav navbar-nav">
                            <?php foreach ($main_nav as $value) : ?>
                                <?php if (isset($value['dropdown'])) : ?>
                                    <li class="panel panel-default dropdown">
                                        <a data-toggle="collapse" href="#<?php echo $value['id'] ?>">
                                            <span class="icon fa fa-<?php echo $value['fa_icon'] ?> "></span><span class="title"><?php echo $value['html'] ?></span>
                                        </a>
                                        <div id="<?php echo $value['id'] ?>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <ul class="nav navbar-nav">
                                                <?php foreach ($value['dropdown'] as $dropdown) : ?>
                                                    <li>
                                                        <a href="<?php echo $dropdown['link'] ?>"><?php echo $dropdown['html'] ?></a>
                                                    </li>
                                                <?php endforeach ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                <?php else: ?>
                                    <li>
                                        <a href="<?php echo $value['link'] ?>">
                                            <span class="icon fa fa-<?php echo $value['fa_icon'] ?>"></span><span class="title"><?php echo $value['html'] ?></span>
                                        </a>
                                    </li>
                                <?php endif ?>
                            <?php endforeach ?>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>
            </div>
            <!-- Main Content -->
            <div class="container-fluid">
                <div class="side-body">
                    <?php foreach ($messages as $message) : ?>
                        <div class="alert fresh-color alert-<?php echo $message['severity_color']?> ">
                            <b><?php echo $message['severity_message'] ?></b> <?php echo $message['message_body']; ?>
                        </div>
                    <?php endforeach ?>
                    <div class="page-title">
                            <span class="title"><?php echo $this->template->title ?></span>
                            <div class="description"><?php echo $this->template->subtitle ?></div>
                    </div>
                    <?php echo $this->template->content; ?>
                </div>
            </div>
        </div>
    </div>
    <?php
        echo $this->template->javascript;
        ?>
</body>
</html>
