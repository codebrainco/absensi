<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="col-xs-6 col-sm-4 col-md-2">
Keterangan Grade :
    <table class="table">
        <thead>
            <tr>
                <th>Huruf</th>
                <th>Angka</th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($list_grade as $huruf => $angka) : ?>
            <tr>
                <td><?php echo $huruf ?></td>
                <td><?php echo $angka ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
</div>
