<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="col-sm-6 col-sm-offset-3">
  <form action="<?php echo current_url() ?>" method="get">
    <div class="col-sm-6">
      <?php echo $available_bulan ?>
    </div>
    <div class="col-sm-6">
      <?php echo form_submit('', 'Submit', array('class' => 'form-control btn-primary',)) ?>
    </div>
  </form>
</div>
