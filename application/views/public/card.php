<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($sct_conf['section_column'])) {
    if (is_numeric($sct_conf['section_column'])) {
        $column = $sct_conf['section_column'];
    } elseif ($sct_conf['section_column'] == 'full') {
        $column = 12;
    } elseif ($sct_conf['section_column'] == 'half') {
        $column = 6;
    }
} else {
    $column = 12;
}

if (isset($sct_conf['section_title'])) {
    $title = $sct_conf['section_title'];
} else {
    $title = $this->template->title;
}
?>
<div class="row" id="table">
    <div class="col-sm-<?php echo $column; ?>">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title"><?php echo $title ?></div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-xs-12">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
