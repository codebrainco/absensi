<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (isset($sct_conf['section_column'])) {
    if (is_numeric($sct_conf['section_column'])) {
        $column = $sct_conf['section_column'];
    } elseif ($sct_conf['section_column'] == 'full') {
        $column = 12;
    } elseif ($sct_conf['section_column'] == 'half') {
        $column = 6;
    }
} else {
    $column = 12;
}

if (isset($sct_conf['section_title'])) {
    $title = $sct_conf['section_title'];
} else {
    $title = $this->template->title;
}

if (isset($sct_conf['two_columns']) && $sct_conf['two_columns'] === true) {
    $two_columns = true;
} else {
    $two_columns = false;
}
?>

<?php if ( ! isset($sct_conf['norow']) || $sct_conf['norow'] == 0) : ?>
<div class="row">
<?php endif ?>
    <div class="col-sm-<?php echo $column; ?>">
        <div class="card">
            <div class="card-header">
                <div class="card-title">
                    <div class="title">Guru Badge</div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-<?php echo $two_columns ? '6' : '12' ?> custom-badge">
                        <div class="col-sm-6"><b>Nama Guru <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->guru ?></div>
                        <div class="col-sm-6"><b>Kelas <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->kelas ?></div>
                        <div class="col-sm-6"><b>Mata Pelajaran <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->matpel ?></div>
                    <?php if ($two_columns === true) : ?>
                        </div>
                        <div class="col-sm-6 custom-badge">
                    <?php endif ?>
                        <div class="col-sm-6"><b>Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->tahun_ajaran ?></div>
                        <div class="col-sm-6"><b>Semester <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->semester ?></div>
                        <div class="col-sm-6"><b>Tanggal Awal Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_begin_date ?></div>
                        <div class="col-sm-6"><b>Tanggal Akhir Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_end_date ?></div>
                        <?php if (isset($referral_url)) : ?>
                            <div class="col-sm-12"><hr></div>
                            <div class="col-sm-12 text-center"><a href="<?php echo $referral_url ?>" class="btn btn-space btn-sm btn-success"> Kembali ke Guru Matpel</a></div>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php if ( ! isset($norow) || $norow == 0) : ?>
</div>
<?php endif ?>
