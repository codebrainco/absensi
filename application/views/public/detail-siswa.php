<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
        tr {
                height: 30px;
        }
        .btn {
                margin-top: 0 !important;
                margin-bottom: 0 !important;
        }
</style>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <?php echo $this->template->siswa_badge ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 custom-badge">
                        <?php echo $this->template->ta_badge ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 table-responsive">
                        <table data-pagelength="50" class="table table-condensed table-striped table-bordered table-sort" width='100%'>
                            <thead>
                                <tr>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">No</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Mata Pelajaran</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Total Pertemuan</th>
                                    <th class="text-center" colspan="5">Kehadiran</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Persentase <br>(per <?php echo $today ?>)</th>
                                    <th class="text-center" style="vertical-align:middle" rowspan="2">Aksi</th>
                                </tr>
                                <tr>
                                    <td class="text-center">H</td>
                                    <td class="text-center">S</td>
                                    <td class="text-center">I</td>
                                    <td class="text-center">A</td>
                                    <td class="text-center">T</td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $x = 0; foreach ($all_guru_matpel as $guru_matpel) : ?>
                                    <tr>
                                        <td class="text-center"><?php echo $x+1 ?></td>
                                        <td><span class="pull-left"><?php echo $guru_matpel->matpel; ?></span></td>
                                        <td class="text-center"><?php echo $guru_matpel->total_pertemuan; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Hadir'][$guru_matpel->id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Sakit'][$guru_matpel->id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Izin'][$guru_matpel->id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Alfa'][$guru_matpel->id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['Terlambat'][$guru_matpel->id]; ?></td>
                                        <td class="text-center"><?php echo $all_properties['persentase'][$guru_matpel->id]; ?>%</td>
                                        <td class="text-center"><?php echo $guru_matpel->action; ?></td>
                                    </tr>
                                <?php $x++; endforeach ?>
                            </tbody>
                        </table>
                        <hr>
                        <div class="col-xs-6 col-sm-4 col-md-2">
                                <table class="table">
                                        <thead>
                                                <tr>
                                                        <th>Huruf</th>
                                                        <th>Keterangan</th>
                                                </tr>
                                        </thead>
                                        <tbody>
                                                <tr>
                                                        <td>H</td>
                                                        <td>Hadir</td>
                                                </tr>
                                                <tr>
                                                        <td>S</td>
                                                        <td>Sakit</td>
                                                </tr>
                                                <tr>
                                                        <td>I</td>
                                                        <td>Izin</td>
                                                </tr>
                                                <tr>
                                                        <td>A</td>
                                                        <td>Alfa</td>
                                                </tr>
                                                <tr>
                                                        <td>T</td>
                                                        <td>Terlambat</td>
                                                </tr>
                                        </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
