<div class="col-sm-6"><b>Nama Guru <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->guru ?></div>
<div class="col-sm-6"><b>Kelas <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->kelas ?></div>
<div class="col-sm-6"><b>Mata Pelajaran <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->matpel ?></div>
<div class="col-sm-6"><b>Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->tahun_ajaran ?></div>
<div class="col-sm-6"><b>Semester <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->semester ?></div>
<div class="col-sm-6"><b>Tanggal Awal Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_begin_date ?></div>
<div class="col-sm-6"><b>Tanggal Akhir Tahun Akademik <span class="pull-right">:</span> </b></div><div class="col-sm-6"><?php echo $guru_matpel->ta_end_date ?></div>
<?php if (isset($referral_url)) : ?>
    <div class="col-sm-12"><hr></div>
    <div class="col-sm-12 text-center"><a href="<?php echo $referral_url ?>" class="btn btn-space btn-sm btn-success"> Kembali ke Guru Matpel</a></div>
<?php endif ?>
