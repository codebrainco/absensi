<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">Guru Badge</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-12 custom-badge">
            <?php echo $this->template->guru_badge ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">
          <div class="title">Berita Acara</div>
        </div>
      </div>
      <div class="card-body">
        <div class="row">
          <div class="col-xs-12">
            <?php echo form_open(current_url()) ?>
              <div class="row">
                <div class="col-sm-12">
                  <?php echo form_open(current_url(), array('class' => 'form-horizontal')) ?>
                    <?php foreach ($input as $key => $value) : ?>
                      <div class="form-group">
                        <?php echo form_label($key, '', array('class' => 'control-label')); ?>
                        <?php echo $value; ?>
                      </div>
                    <?php endforeach; ?>
                    <div class="form-group">
                      <?php echo form_submit('submit', 'Submit', array('class' => 'btn btn-success pull-right')) ?>
                    </div>
                  <?php echo form_close(); ?>
                </div>
              </div>
            <?php echo form_close() ; ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
