<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>


  #btnLogin
  {
    float:left;
  }
  #sideLink
  {
    padding-top: 10px;
    float:right;
  }

  .row
  {
    padding-top: 50px;
  }
</style>

<div class="container">
  <div class="row vertical-center">
    <div class="col-xs-12 col-sm-6 col-md-4 col-sm-offset-3 col-md-offset-4">
      <?php echo form_open(current_url()); ?>
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Login</h3>
        </div>
        <div class="panel-body">
        <?php foreach ($messages as $message) : ?>
          <div class="alert alert-<?php echo $message['severity_color']?> ">
            <b><?php echo $message['severity_message'] ?></b> <?php echo $message['message_body']; ?>
          </div>
        <?php endforeach ?>
          <?php foreach ($input as $key => $value) : ?>
            <div class="form-group">
              <?php echo form_input($input[$key]); ?>
            </div>
          <?php endforeach ?>

          <button name="submit" type="submit" class="btn btn-success btn-block">Login</button>
        </div>
      </div>

      <?php echo form_close(); ?>
    </div>
  </div>
</div>
