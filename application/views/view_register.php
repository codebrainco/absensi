<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<style>
  #btnLogin
  {
    float:left;
  }
  #sideLink
  {
    padding-top: 10px;
    float:right;
  }
</style>

<div class="container">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <h1><?php echo $this->template->title ?></h1>
      <h6 style="color:red"><?php echo validation_errors(); ?></h6>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4">
      <?php echo form_open(current_url()); ?>

        <?php foreach ($input as $key => $value) : ?>
          <div class="form-group">
              <?php echo form_label($key, $input[$key]['id']); ?>
              <?php echo form_input($input[$key]); ?>
          </div>
        <?php endforeach ?>

        <div id="btnLogin">
          <button name="submit" type="submit" class="btn">Daftar</button>
        </div>
        <div id="sideLink">
          <a href="#">Lupa Password</a> | <a href="<?php echo base_url('login') ?>">Login</a>
        </div>

      <?php echo form_close(); ?>
    </div>
  </div>
</div>

<div class="modal fade" id="registStatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Register Status</h4>
      </div>
      <div class="modal-body">
      <?php if ($registStatus) : ?>
        Proses registrasi telah berhasil, silahkan kembali ke halaman login.
      <?php else: ?>
        Terdapat kesalahan dalam proses pendaftaran, silahkan mencoba setelah beberapa saat.
      <?php endif; ?>
      </div>
      <div class="modal-footer">
      <?php if ($registStatus) : ?>
        <button class="btn btn-default" data-dismiss="modal">Close</button>
      <?php endif ?>
        <a href="<?php echo base_url('login'); ?>" class="btn btn-primary" >Login</a>
      </div>
    </div>
  </div>
</div>

<div id="regToken" hidden="true" value="<?php echo $registStatus ?>"></div>
