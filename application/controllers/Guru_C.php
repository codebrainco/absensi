<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
* -----------------------------------------------------------------------------
* Manage_Controller Class
* -----------------------------------------------------------------------------
*
* This controller will take care of 2nd branch of 3 level Auth System
*
* TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
* ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
*
* TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
*
* Function
* -----------------------------------------------------------------------------
* index                  = Fetch common items
* view_dashboard         = View for dashboard
* view_profile           = View for profile, as well as editing
* view_attendance        = View for attendance, view only
* view_detail_attendance = View for detail attendance, view only
* view_manage            = View for manage thingy
* view_manage_matpel     = View for matpel manage, can do "Absen!"
* view_manage_absensi    = View for editing absensi
* get_guru_matpel        = Get guru matpel specific for Gur
*/
class Guru extends Guru_Controller
{
    public function __construct()
    {
        parent::__construct();
    }


    // ---------------------------------------------------------------------------

    /**
    * Manage Matpel, just to absen matpel that guru teach
    *
    * @see Absensi_Controller::man_nav()
    * @see Absensi_Controller::get_profile()
    * @see Absensi_Controller::guru_matpel_table_generate()
    * @see Absensi_Controller::get_semester()
    * @return void
    */


    // ---------------------------------------------------------------------------

    /**
    * Manage absensi with validation of time and guru
    *
    * @see Guru::get_guru_matpel()
    * @see Absensi_Controller::is_allowed_to_edit()
    * @see Absensi_Controller::get_jadwal()
    * @see Absensi_Controller::get_profile()
    * @see Absensi_Controller::generate_absensi()
    * @see Absensi_Controller::get_semester()
    * @param  int $guru_matpel_id
    * @return void
    */

    // ---------------------------------------------------------------------------
}
