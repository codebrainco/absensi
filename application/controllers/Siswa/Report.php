<?php
class Report extends Siswa_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function attendance()
    {
        $this->loader->model('guru_matpel', 'absensi_kbm');
        $siswa = $this->siswa_m->where('account_id', $this->acc_id)->get();
        $siswa->kelas = $this->kelas_m->get_kelas($siswa->kelas_id);
        $list_guru_matpel = $this->get_guru_matpel();
        // $list_kegiatan = $this->kegiatan_siswa_m->with_kegiatan()->where('account_id', $profile->account_id)->get_all() ?: array();
        $list_absensi_kbm = $this->get_absensi_kbm($this->acc_id);
        // $list_absensi_kegiatan = $this->get_absensi_kegiatan();

        // build an array with $absensi[guru_matpel_id] = absensi object
        $absensi = array();
        foreach ($list_absensi_kbm as $one_absensi) {
            $absensi[$one_absensi->guru_matpel_id] = $one_absensi;
        }

        // build a table with kehadiran
        $heading = array(
            'Nama Matpel' => 'matpel',
            'Guru' => 'guru',
            'Kehadiran' => array('field' => 'kehadiran', 'data' => $absensi),
            );
        $action = anchor(base_url('siswa/report/attendance/detail/(:guru_matpel_id)'), 'Detail Absensi', array('class' => 'btn btn-sm btn-default'));
        $all_guru_matpel = $this->get_guru_matpel();
        $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading, $action);

        // $heading = array(
        //   'Nama Kegiatan' => 'kegiatan',
        //   );
        // if (count($list_kegiatan) >= 1)
        // {
        //   $table_kegiatan = $this->populate_kegiatan($list_absensi_kegiatan, $list_kegiatan, $heading);
        //   $this->add_prop($table_kegiatan, 'table_kegiatan');
        // }

        $ta_aktif = $this->get_ta_aktif();

        // publish Section
        $this->set_title('Absensi');
        $this->add_prop($this->get_ta_aktif(), 'ta_aktif');
        $this->add_prop($table_matpel, 'table');
        $this->add_prop($ta_aktif, 'ta_aktif');
        $this->add_prop($siswa, 'siswa');
        $this->append_page($this->get_site_root('profile_badge'));
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // ---------------------------------------------------------------------------

    /**
     * Special attendance function for siswa
     *
     * @param  int $guru_matpel_id
     * @return void
     */
    public function detail_attendance($guru_matpel_id)
    {
        $this->loader->model('guru_matpel', 'absensi_kbm', 'tahun_ajaran', 'disabled_date');
        // fetch common item
        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        $all_siswa = $this->siswa_m->with_account()->where('account_id', $this->acc_id)->get_all();

        $acc_id = $this->acc_id;

        $tmp_abs = $this->abs_kbm_m->get_absensi_all($guru_matpel->tahun_ajaran, null, $guru_matpel->id, $acc_id);

        $periode = $this->input->get('periode');
        if (isset($periode)) {
            // check if periode is outside range
            if ($this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0) {
                ($periode >= 1 && $periode <= 6) or redirect('admin/kbm/guru_matpel');
            } else {
                ($periode >= 7 && $periode <= 12) or redirect('admin/kbm/guru_matpel');
            }
        } else {
            $periode = $this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        }

        $all_properties = array();

        $tmp_siswa_absensi = $this->abs_kbm_m->count_absensi($this->acc_id, $guru_matpel->id);
        $all_siswa_absensi = array();
        foreach ($tmp_siswa_absensi as $value) {
            $all_siswa_absensi[$value->account_id] = $value;
        }
        $total_pertemuan = $this->count_pertemuan($guru_matpel);
        $all_properties['persentase'][$this->acc_id] = isset($all_siswa_absensi[$this->acc_id]) ? round($all_siswa_absensi[$this->acc_id]->kehadiran / $total_pertemuan * 100) : 0;

        foreach ($this->get_status_hadir() as $key => $value) {
            // $tmp = $this->abs_kbm_m->count_absensi($this->acc_id, $guru_matpel->id, $key);
            if (isset($all_siswa_absensi[$this->acc_id])) {
                $all_properties[$value][$this->acc_id] = $all_siswa_absensi[$this->acc_id]->{strtolower($value)};
            } else {
                $all_properties[$value][$this->acc_id] = 0;
            }
        }

        // built dependencies
        $all_siswa = array($this->siswa_m->get_one());
        $all_dates = $this->generate_tanggal($guru_matpel, $periode);
        $all_absensi = $this->generate_absensi($tmp_abs);

        $guru_matpel->jadwal = $this->get_jadwal($guru_matpel_id);
        foreach ($guru_matpel->jadwal as $key => $value) {
            $guru_matpel->jadwal[$key]['hari'] = $this->get_hari($guru_matpel->jadwal[$key]['hari']);
            $guru_matpel->jadwal[$key]['begin_hrs'] = substr($guru_matpel->jadwal[$key]['begin_hrs'], 0, 5);
            $guru_matpel->jadwal[$key]['end_hrs'] = substr($guru_matpel->jadwal[$key]['end_hrs'], 0, 5);
        }


        // fetch bulan, if semester is Ganjil start from July to end, January otherwise
        $begin = $this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        $bulan_opt = array();
        for ($i=$begin; $i < $begin+6; $i++) {
            $bulan_opt[$i] = $this->get_bulan_eng($i);
        }
        $available_bulan = form_dropdown('periode', $bulan_opt, $periode, array('class' => 'form-control'));

        $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;

        // publish section
        $this->set_title('Detail Absensi');
        $this->add_prop($all_absensi, 'absensi');
        $this->add_prop($all_properties, 'all_properties');
        $this->add_prop($available_bulan, 'available_bulan');
        $this->add_prop($all_siswa, 'all_siswa');
        $this->add_prop($tahun, 'tahun');
        $this->add_prop($all_dates['date'], 'all_dates');
        $this->add_prop($all_dates['day'], 'all_day');
        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->compile_page('public/guru-badge', 'guru_badge');
        $this->compile_page('public/detail_absensi');
        $this->publish();
    }
}
