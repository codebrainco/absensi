<?php
class Profile extends Siswa_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->loader->model('kelas', 'account');
        $siswa = $this->siswa_m->get_one();
        $siswa_pair = $this->get_siswa_pair();

        if (isset($_POST['submit']) === true)
        {
            if ($this->do_edit($this->acc_id)) {
                redirect('siswa/profile','refresh');
            }
        }

        // build structured array for form_generator
        $siswa_field = array();
        foreach ($siswa_pair as $heading => $field)
        {
            $siswa_field[$heading] = $field.'|text';
        }

        // special for tanggal it will be rendered as date
        $siswa_field['Tanggal Lahir'] = 'tanggal_lahir|date';
        $siswa_field['Tanggal Pendaftaran'] = 'tanggal_pendaftaran|date';

        // special for NIS it will be disabled
        $siswa_field['NIS'] = 'nis|text|disabled[]';
        $input_personal = $this->form_generator($siswa_field, $siswa, 'form-control');

        // special for some properties it will be rendered as dropdown
        $siswa_options = $this->get_siswa_opt();
        foreach ($siswa_options as $field => $value)
        {
            $input_personal[$field] = form_dropdown($siswa_pair[$field], $value, $siswa ? $siswa->{$siswa_pair[$field]}  : '', array('class' => 'form-control'));
        }

        $input_account = $this->form_input_account($siswa);

        if ($this->acc_id != 0) {
            foreach ($this->ion_auth->get_users_groups($this->acc_id)->result() as $ion_group) {
                $groups[] = $ion_group->description;
            }
        }

        // publish Section
        $this->set_title('Edit Profile');
        $this->add_prop($groups, 'groups');
        $this->add_prop($input_personal, 'input_personal');
        $this->add_prop($input_account, 'input_account');
        $this->compile_page('public/profile');
        $this->publish();
    }

    public function do_edit($acc_id)
    {
        $this->loader->model('account');
        if ($_POST['submit'] === null) {
            return false;
        }

        $siswa = $this->siswa_m->get_one();
        $siswa_pair = $this->get_siswa_pair();

        $field_personal = array();
        // get all field
        foreach ($siswa_pair as $field)
        {
            $field_personal[] = $field;
        }

        if ($this->acc_m->edit_account($siswa))
        {
            $input_personal = $this->prep_input($field_personal);
            $input_personal['nis'] = $siswa->nis;

            if (empty($input_personal['lintas_minat1'])) {
                $input_personal['lintas_minat1'] = null;
            }
            if (empty($input_personal['lintas_minat2'])) {
                $input_personal['lintas_minat2'] = null;
            }

            // Now update the personal;
            $this->siswa_m->edit_data($this->acc_id, $input_personal);
            $this->add_message('Profil data berhasil di update', true, 1);
            return true;
        } else {
            return false;
        }
    }
}
