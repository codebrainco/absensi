<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
* ------------------------------------------------------------------------------
* Siswa Class
* ------------------------------------------------------------------------------
*
* This controller will take care of Siswa-related function, it extends
* Absensi_Controller as the first branch of the 3 level auth
*
* TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
* ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
*
* TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
*
* Function
* ------------------------------------------------------------------------------
* index           = Fetch data needed and redirect to dashboard
* view_dashboard  = view for dashboard
* view_profile    = view for profile
* view_manage     = view for manage
* matpel_m->get_matpel      = get matpel based on kelas
*
* NOTE
* ------------------------------------------------------------------------------
* View attendance is derived from parent controller
*/
class Siswa_C extends Absensi_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->check_auth(2);
        $this->root = 'siswa';
        $this->site_root = 'siswa/';


        // load navigation
        $nav = $this->main_nav();
        $this->add_prop($nav, 'main_nav');

        $profile = $this->get_profile();
        $this->add_prop($profile, 'profile');
    }

    // ---------------------------------------------------------------------------

    /**
     * Fetch profile, config and tahun ajaran and assign to session
     * 'profile', * 'config' and 'ta_aktif' respectively then redirect to
     * dashboard
     *
     * @see Absensi_Controller::fetch_profile()
     * @see Absensi_Controller::fetch_config()
     * @see Absensi_Controller::fetch_tahun_ajaran()
     * @return void
     */
    public function index()
    {
        $this->fetch_profile('siswa');
        $this->fetch_config();
        $this->fetch_tahun_ajaran();
        redirect(base_url('siswa/dashboard'));
    }

    // ---------------------------------------------------------------------------

    /**
     * Dashboard, still nothing
     *
     * @todo fill something here, like a lookup or something
     * @todo - with warning "Please check for date [x]"
     * @return void
     */
    public function view_dashboard()
    {
        // publish Section
        $this->set_title('Dashboard');
        $this->compile_page($this->get_site_root('siswa_dashboard'));
        $this->publish();
    }

    // ---------------------------------------------------------------------------

    /**
     * Profile section, handle editing account info and personal info
     *
     * @see Absensi_Controller::get_profile()
     * @see Absensi_Controller::get_siswa_pair()
     * @see Absensi_Controller::edit_account()
     * @see Absensi_Controller::form_generator()
     * @see Absensi_Controller::form_input_account()
     * @see Base_Controller::prep_input()
     * @return void
     */
    public function view_profile()


    // ---------------------------------------------------------------------------

    /**
     * Attendance section, contain only view for kbm and ought-to-be kegiatan
     * attendance
     *
     * @see Absensi_Controller::get_profile()
     * @see Absensi_Controller::get_kelas()
     * @see Absensi_Controller::get_absensi_kbm()
     * @see Absensi_Controller::guru_matpel_table_generate()
     * @see Siswa::get_guru_matpel()
     * @todo kegiatan
     * @return void
     */


    // ---------------------------------------------------------------------------

    /**
     * 'Manage' section
     *
     * @return void
     */
    public function view_manage()
    {
        // publish Section
        $this->compile_page('siswa/siswa_manage');
        $this->publish();
    }

    // ----------------------------------------------------------------------------------
}
