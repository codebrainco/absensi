<?php
class Kelas extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->loader->model('kelas', 'siswa');
        $this->site_root = $this->site_root.'/kelas/';
    }

    public function kelas_base($data_kelas, $type)
    {
        $this->loader->library('table');
        $this->table->set_template(array('table_open' => '<table class="table table-bordered table-sort" width=\'100%\'>'));
        $this->table->set_heading('Kelas', 'Jumlah Siswa', 'Aksi');
        foreach ($data_kelas as $kelas) {
            $action = '';
            $action .= anchor(base_url("admin/master/$type/edit/".$kelas->id), "Edit", array('class' => 'btn btn-default btn-space btn-sm'));
            $action .= anchor(base_url("admin/master/$type/remove/".$kelas->id), "Hapus", array('class' => 'btn btn-default btn-space btn-sm red white-text'));
            $action .= anchor(base_url("admin/master/$type/siswa/".$kelas->id), "Lihat Siswa", array('class' => 'btn btn-default btn-space btn-sm indigo white-text'));
            if (isset($kelas->siswa)) {
                $jumlah_siswa = $kelas->siswa->counted_rows;
            } else {
                $jumlah_siswa = 0;
            }
            $row = array(
                $kelas->kelas,
                array('data' => $jumlah_siswa, 'class' => 'text-center'),
                array('data' => $action, 'class' => 'text-center'),
                );
            $this->table->add_row($row);
        }
        $table_kelas = $this->table->generate();

        // publish section
        $this->add_prop($table_kelas, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    /**
     * View for kelas normal, handle title and interaction
     * @return void
     */
    public function kelas_normal()
    {
        $data_kelas = $this->kelas_m->get_all_kelas($count = true) ?: array();

        $interaction = anchor(base_url('admin/master/kelas/add'), 'Tambah Kelas', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->add_prop($interaction, 'interaction');
        $this->set_title('Kelas', 'Daftar kelas beserta jumlah siswa dan detail siswa nya');
        $this->set_breadcrumbs('Data Master', 'Kelas');

        $this->kelas_base($data_kelas, 'kelas');
    }

    // -------------------------------------------------------------------------

    /**
     * View for kelas lintas minat, handle title and interaction
     * @return void
     */
    public function kelas_lm()
    {
        $data_kelas_lm = $this->kelas_m->get_all_kelas_lm($count = true) ?: array();


        foreach ($data_kelas_lm as $key => $kelas) {
            $data_kelas_lm[$key]->siswa = new stdClass();
            $data_kelas_lm[$key]->siswa->counted_rows = (isset($data_kelas_lm[$key]->siswa_lm1) ? $data_kelas_lm[$key]->siswa_lm1->counted_rows : 0) + (isset($data_kelas_lm[$key]->siswa_lm2) ? $data_kelas_lm[$key]->siswa_lm2->counted_rows : 0);
        }

        $interaction = anchor(base_url('admin/master/kelas_lm/add'), 'Tambah Kelas Lintas Minat', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->add_prop($interaction, 'interaction');
        $this->set_title('Kelas Lintas Minat', 'Daftar kelas lintas minat beserta jumlah siswa dan detail siswa nya');
        $this->set_breadcrumbs('Data Master', 'Kelas Lintas Minat');

        $this->kelas_base($data_kelas_lm, 'kelas_lm');
    }

    public function kelas_siswa_base($kelas_data, $data_siswa, $siswa_outer)
    {
        $this->loader->library('table');
        $this->loader->helper('form');

        // build siswa table
        $this->table->set_template(array('table_open' => '<table data-pagelength="50" class="table table-bordered table-sort" width=\'100%\'>'));
        $this->table->set_heading('Account ID', 'NIS', 'Nama', 'Kelas', 'Angkatan', 'Aksi');
        $all_kelas = $this->kelas_m->as_dropdown('kelas')->get_all();
        foreach ($data_siswa as $siswa) {
            $action = '';
            $action .= anchor(base_url('admin/account/siswa/edit/'.$siswa->account_id), 'Lihat', array('class' => $this->get_html_prop('button-default-xs')));

            $row = array(
                $siswa->account_id,
                $siswa->nis,
                $siswa->nama,
                $all_kelas[$siswa->kelas_id],
                $siswa->angkatan,
                array('data' => $action, 'class' => 'text-center')
                );
            $this->table->add_row($row);
        }
        $table_siswa = $this->table->generate();

        // dropdown for adding siswa to class
        $siswa_opt = array();
        $kelas_list = $this->kelas_m->as_dropdown('kelas')->get_all();
        foreach ($siswa_outer as $siswa) {
            $siswa_opt[$siswa->account_id] = $kelas_list[$siswa->kelas_id].' | '.$siswa->nama;
        }
        $form_siswa = form_dropdown('siswa', $siswa_opt, '', array('class' => 'form-control'));


        // publish section
        $this->set_title('Siswa', 'Daftar siswa dalam kelas tertentu');
        $this->set_breadcrumbs('Data Master', 'Kelas', 'Siswa');
        $this->add_prop($form_siswa, 'form_siswa');
        $this->add_prop($table_siswa, 'table_siswa');
        $this->compile_page($this->get_site_root('daftar_siswa'));
        $this->publish();
    }

    /**
     * View for kelas with siswa table, handle table and adding siswa to its kelas
     * @fixme !
     * @param  int $kelas_id
     * @return void
     */
    public function kelas_siswa($kelas_id)
    {
        $fields = 'nama, nis, angkatan, kelas_id';
        $available_angkatan = $this->get_allowed_angkatan();

        $tmp_kelas = $this->kelas_m->get($kelas_id);
        $data_siswa = $this->siswa_m->get_filtered($fields, array('kelas_id' => $kelas_id, 'angkatan' => $available_angkatan));
        $siswa_outer = $this->siswa_m->get_except_kelas($kelas_id, $available_angkatan);

        $kelas_data = $this->slice_kelas($tmp_kelas->kelas);
        $kelas_data->id = $kelas_id;
        $kelas_data->jumlah_siswa = $this->kelas_m->count_siswa($kelas_id);
        $this->add_prop($kelas_data, 'kelas');

        if (isset($_POST['submit'])) {
            $post = $this->prep_input_obj('siswa');
            $this->siswa_m->update(array('kelas_id' => $kelas_data->id), array('account_id' => $post->siswa));
            $this->add_message('Data berhasil di update', true, 1);
            redirect('admin/master/kelas/siswa/'.$kelas_data->id, 'refresh');
        }

        $this->compile_page('public/kelas_badge', 'kelas_badge');

        $this->kelas_siswa_base($kelas_data, $data_siswa, $siswa_outer);
    }

    /**
     * View for kelas with siswa table, handle table and adding siswa to its kelas
     * @fixme !
     * @param  int $kelas_id
     * @return void
     */
    public function kelas_lm_siswa($kelas_id)
    {
        $fields = 'nama, nis, angkatan, kelas_id';
        $available_angkatan = $this->get_allowed_angkatan();

        $tmp_kelas = $this->kelas_m->get($kelas_id);
        $data_siswa = $this->siswa_m->get_filtered($fields, array('lintas_minat1` = \''.$kelas_id.'\' OR `lintas_minat2 =' => $kelas_id, 'angkatan' => $available_angkatan));
        $siswa_outer = array();

        $kelas_data = $this->slice_kelas_lm($tmp_kelas->kelas);
        $kelas_data->id = $kelas_id;
        $kelas_data->jumlah_siswa = $this->kelas_m->count_siswa_lm($kelas_id);
        $this->add_prop($kelas_data, 'kelas');

        $this->compile_page('public/kelas_lm_badge', 'kelas_badge');

        $this->kelas_siswa_base($kelas_data, $data_siswa, $siswa_outer);
    }

    // -------------------------------------------------------------------------

    /**
     * Function to edit kelas
     * @param  mixed[]  $post     of posted data
     * @param  int      $kelas_id kelas id to be updated
     * @param  boolean  $lm       is lintas minat ?
     * @return boolean            false if validation return false
     */
    public function kelas_edit_base($post, $kelas_id, $lm = false)
    {
        $checked_pair = array('kelas' => $post['kelas']);
        if ( ! $this->kelas_m->is_unique($checked_pair, $kelas_id)) {
            $this->add_message('Kelas tidak unique', false, 2);
            return false;
        }

        if ($lm) {
            $post['lm'] = 1;
        }

        if ($kelas_id == 0) {
            $this->add_message('Kelas : '.$post['kelas'].' berhasil ditambahkan', true, 1);
            $this->kelas_m->insert($post);
        } else {
            $this->add_message('Kelas : '.$post['kelas'].' berhasil dirubah', true, 1);
            $this->kelas_m->update($post, $kelas_id);
        }

        return true;
    }

    /**
     * View for adding and editting kelas, handle 2 dropdown and 1 text
     * @param  int $kelas_id
     * @return void
     */
    public function kelas_edit($kelas_id)
    {
        $this->loader->model('jurusan');
        $this->loader->helper('form');

        if (isset($_POST['submit'])) {
            $post = $this->prep_input('tingkat', 'jurusan_id', 'kelas');
            $post['jurusan_id'] = $this->jurusan_m->get_jurusan($post['jurusan_id']);
            $post['kelas'] = implode('-', $post);

            if ($this->kelas_edit_base($post, $kelas_id, $lm = false)) {
                redirect('admin/master/kelas');
            }
        }

        // fetch common items
        $kelas = $this->kelas_m->get_kelas($kelas_id);
        $kelas = ! empty($kelas) ? $this->slice_kelas($kelas) : null;

        $tmp_kelas = $this->kelas_m->get($kelas_id) ?: null;

        if ($tmp_kelas !== null) {
            if ($tmp_kelas->lm == 1) {
                $this->add_message('Kelas tersebut merupkan kelas lintas minat', true, 3);
                redirect('admin/master/kelas', 'refresh');
            }
        } else {
            if ($kelas_id != 0) {
                $this->add_message('Invalid Kelas ID', true, 3);
                redirect('admin/master/kelas', 'refresh');
            }
        }

        // build form
        $tingkat = array(
            '10' => '10',
            '11' => '11',
            '12' => '12',
            );
        $jurusan = $this->jurusan_m->get_all_jurusan();
        $kelas_prop = array(
            'name' => 'kelas',
            'type' => 'text',
            'value' => ! empty($kelas) ? $kelas->kelas : '',
            'class' => 'form-control'
            );

        $input = array();
        $input['Tingkat'] = form_dropdown('tingkat', $tingkat, ! empty($kelas) ? $kelas->tingkat : '', array('class' => 'form-control'));
        $input['Jurusan'] = form_dropdown('jurusan_id', $jurusan, ! empty($kelas) ? array_search($kelas->jurusan, $jurusan) : '', array('class' => 'form-control'));
        $input['Kelas'] = form_input($kelas_prop);

        // publish section
        $this->set_title($this->title_add_or_edit($kelas_id, 'Kelas'), 'Menambahkan atau melakukan perubahan pada data jurusan');
        $this->set_breadcrumbs('Data Master', 'Kelas', 'Edit');
        $this->add_prop($input, 'input');
        $sct_conf = array('section_title' => 'Kelas');
        $this->add_prop($sct_conf, 'sct_conf');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }

    /**
     * View for adding and editting kelas lintas minat, same as kelas_edit
     * @param  int $kelas_id
     * @return void
     */
    public function kelas_lm_edit($kelas_id)
    {
        $this->loader->model('matpel');
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            $post = $this->prep_input('tingkat', 'matpel_id');
            $post['matpel_id'] = $this->matpel_m->get($post['matpel_id'])->matpel;
            $post['kelas'] = implode('-', $post);
            if ($this->kelas_edit_base($post, $kelas_id, $lm = true)) {
                redirect('admin/master/kelas_lm');
            }
        }

        // fetch common items
        $kelas_lm = $this->kelas_m->get_kelas($kelas_id) ?: null;
        $tmp_kelas = $this->kelas_m->get($kelas_id) ?: null;

        if ($tmp_kelas === null) {
            if ($kelas_id != 0) {
                $this->add_message('Invalid Kelas ID', true, 3);
                redirect('admin/master/kelas', 'refresh');
            }
        }

        $kelas_lm = ! empty($kelas_lm) ? $this->slice_kelas_lm($kelas_lm) : null;

        // build form
        $tingkat = array(
            '10' => '10',
            '11' => '11',
            '12' => '12',
            );
        $matpel_opt = $this->matpel_m->as_dropdown('matpel')->get_all();
        $input = array();
        $input['Tingkat'] = form_dropdown('tingkat', $tingkat, ! empty($kelas_lm) ? $kelas_lm->tingkat : '', array('class' => 'form-control'));
        $input['Mata Pelajaran'] = form_dropdown('matpel_id', $matpel_opt, ! empty($kelas_lm) ? array_search($kelas_lm->matpel, $matpel_opt) : '', array('class' => 'form-control'));

        // publish section
        $this->set_title($this->title_add_or_edit($kelas_id, 'Kelas Lintas Minat'), 'Menambahkan atau melakukan perubahan pada data kelas lintas minat');
        $this->set_breadcrumbs('Data Master', 'Kelas Lintas Minat', 'Edit');
        $this->add_prop($input, 'input');
        $sct_conf = array('section_title' => 'Kelas Lintas Minat');
        $this->add_prop($sct_conf, 'sct_conf');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }


    /**
     * Remove kelas
     *
     * @param  int $kelas_id
     * @return void
     */
    public function remove($kelas_id, $lm = false)
    {
        $kelas_name = $this->kelas_m->get($kelas_id)->kelas;
        $this->add_message('Kelas : '.$kelas_name.' berhasil di hapus', true, 1);
        $this->kelas_m->delete($kelas_id);
        redirect('admin/master/kelas'.($lm ? '_lm' : ''));
    }
}
