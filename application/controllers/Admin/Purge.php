<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Purge extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->loader->helper('form');
        $this->set_title('Purge');
        $this->compile_page('admin/purge');
        $this->publish();
    }

    public function siswa()
    {
        $this->loader->model('siswa', 'account');
        $all_siswa_id = array_column($this->siswa_m->get_all() ?: array(), 'account_id');
        if ( ! empty($all_siswa_id)) {
            $this->acc_m->where('id', $all_siswa_id)->delete();
        }
        $this->add_message('Proses pengosongan berhasil', true, 1);
        redirect('admin/purge','refresh');
    }

    public function guru()
    {
        $this->loader->model('guru', 'account');
        $all_guru_id = array_column($this->guru_m->get_all() ?: array(), 'account_id');
        $my_id = array_search($this->session->user_id, $all_guru_id);
        unset($all_guru_id[$my_id]);
        if ( ! empty($all_guru_id)) {
            $this->acc_m->where('id', $all_guru_id)->delete();
        }
        $this->add_message('Proses pengosongan berhasil', true, 1);
        redirect('admin/purge','refresh');
    }
}
