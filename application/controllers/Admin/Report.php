<?php
class Report extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('guru_matpel', 'absensi_kbm', 'tahun_ajaran');
        $this->loader->library('table', 'filter_manager');
        $this->loader->helper('form');
    }

    /**
     * View untuk Laporan => kehadiran guru mengajar
     * @return void
     */
    public function guru_mengajar()
    {
        // filter
        $filter = $this->init_filter();
        $get = $this->input->get();
        if (empty($get) !== false) {
            $filter = array('tahun_ajaran_id' => $this->get_ta_aktif()->id);
        }

        // populating
        $all_guru_matpel = $this->get_guru_matpel_all(null, $filter);
        $all_guru_matpel_id = array_column($all_guru_matpel, 'account_id');
        $site_url = base_url('admin/report/guru');

        // table
        $heading_pair = array(
            'Nama Guru'       => 'guru',
            'Mapel di Ampu'   => 'matpel',
            'Kelas'           => 'kelas',
            'Jadwal'          => 'jadwal',
            );
        $action = '';
        $action .= anchor(base_url("admin/attendance/detail/(:guru_matpel_id)"), " Detail Absensi", array('class' => 'btn btn-default btn-space btn-sm'));
        $action .= anchor(base_url("admin/report/guru/detail/(:account_id)"), " Kehadiran Guru", array('class' => 'btn btn-default btn-space btn-sm'));
        $table_report = $this->guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action, $filter);

        // filter
        $filter_arr = $this->filter_manager
            ->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())
            ->load('guru', $this->guru_m->get_all())
            ->load('kelas', $this->kelas_m->order_by('kelas', 'ASC')->get_all())
            ->get();


        // publish section
        $this->set_title('Laporan Guru Mengajar');
        $this->set_breadcrumbs('Laporan', 'Laporan Guru Mengajar');
        $this->add_prop($filter_arr, 'filter');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_report, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function kehadiran_guru_mengajar($acc_id)
    {
        $filter = $this->init_filter();

        $get = $this->input->get();

        if (empty($get) !== false) {
            $filter = array('tahun_ajaran_id' => $this->get_ta_aktif()->id);
        }
        $ta_aktif = $this->get_ta_aktif();
        $data = $this->guru_matpel_filter($this->guru_matpel_m->with_guru()->with_matpel()->with_kelas()->where('account_id', $acc_id)->where('tahun_ajaran_id', $ta_aktif->id)->get_all()) ?: array();

        $action = anchor(base_url("admin/attendance/detail/(:guru_matpel_id)"), " Detail Absensi", array('class' => 'btn btn-default btn-space btn-sm'));
        $heading_array = array('ID', 'Nama Guru', 'Mapel di Ampu', 'Kelas', 'Total Kehadiran', 'Total Pertemuan', 'Kehadiran', 'Grade');

        $this->counter_init();
        $this->table->set_template(array('table_open' => '<table class="table table-sort" width=\'100%\'>', 'heading_cell_start'    => '<th style="vertical-align:middle; text-align:center">',));
        $this->table->set_heading($heading_array);
        foreach ($data as $guru_matpel) {
            $periode = $this->input->get('periode');
            if (isset($periode)) {
                // check if periode is outside range
                if ($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0) {
                    ($periode >= 1 && $periode <= 6) or show_error('Invalid Periode', 403, 'Invalid');
                }
                else {
                    ($periode >= 7 && $periode <= 12) or show_error('Invalid Periode', 403, 'Invalid');
                }
            }
            else {
                $periode = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
            }

            $absensi = array();
            $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;

            $absensi = $this->abs_kbm_m->get_absensi_all($tahun.'-'.str_pad($periode, 2, '0', STR_PAD_LEFT), null, $guru_matpel->id, $guru_matpel->account_id);

            $total_kehadiran = count($absensi);

            $tanggal_per_bulan = $this->generate_tanggal($guru_matpel, $periode);

            if (empty($tanggal_per_bulan['date'])) {
                $total_pertemuan_per_bulan = 0;
                $percentage = 0;
            } else {
                $total_pertemuan_per_bulan = count($tanggal_per_bulan['date'][$this->get_bulan_eng($periode)]);
                $percentage = ($total_kehadiran / $total_pertemuan_per_bulan) * 100;
            }


            $row = array(
                $guru_matpel->id,
                $guru_matpel->guru,
                $guru_matpel->matpel,
                $guru_matpel->kelas,
                $total_kehadiran,
                $total_pertemuan_per_bulan,
                $percentage.'%',
                $this->get_grade($percentage)
                // $guru_matpel->
                );
            $this->table->add_row($row);
        }
        $table_report = $this->table->generate();

        // untuk keterangan grade
        $list_grade = $this->get_all_grade();
        $this->counter_init();
        foreach ($list_grade as $huruf => $angka) {
            if ($this->get_counter() == 0) {
                $grade_opt[$huruf] = $angka.'-'.'100';
            } elseif ($this->get_counter() == count($list_grade)) {
                $grade_opt[$huruf] = $angka;
            } else {
                $grade_opt[$huruf] = $angka.'-'.($list_grade[$huruf_tmp]-1);
            }
            $huruf_tmp = $huruf;
            $this->counter_inc();
        }

        // publish section
        $this->set_title('Laporan Guru Mengajar');
        $this->set_breadcrumbs('Laporan', 'Laporan Guru Mengajar');
        $this->add_prop($grade_opt, 'list_grade');
        $this->add_prop($table_report, 'table');

        $begin = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        $bulan_opt = array();
        for ($i=$begin; $i < $begin+6; $i++) {
            $bulan_opt[$i] = $this->get_bulan_eng($i);
        }
        $available_bulan = form_dropdown('periode', $bulan_opt, $periode, array('class' => 'form-control'));

        $this->add_prop($available_bulan, 'available_bulan');
        $this->compile_page('public/micro-periode', 'prefix');
        $this->compile_page('public/micro-keterangan-grade', 'postfix');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function siswa()
    {
        $this->loader->model('siswa', 'kelas');
        $filter = $this->init_filter();
        $fields = 'nama, nis';
        $selected = $get = $this->input->get();

        // check for filter
        if ($get === null || empty($get) !== false) {
            // siswa will have current avaliable angkatan (3 last tahun ajaran) as default
            $available_angkatan = $this->get_allowed_angkatan();
            $list_all_siswa = $this->siswa_m->get_by_angkatan($fields, $available_angkatan);
        } else {
            $list_all_siswa = $this->siswa_m->get_filtered($fields, $filter);
        }

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-condensed table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading(array('No', 'NIS', 'Nama', 'Kelas', 'Aksi'));

        $all_kelas = $this->kelas_m->as_dropdown('kelas')->get_all();

        $this->counter_init();
        foreach ($list_all_siswa as $siswa) {
            $action = '';
            $action .= '<a href="'.base_url('admin/report/siswa/detail/'.$siswa->account_id).'" class="btn btn-default btn-sm" >Rincian</a>';
            $row = array(
                $this->counter_inc(),
                $siswa->nis,
                $siswa->nama,
                $all_kelas[$siswa->kelas_id],
                array('data' => $action, 'class' => 'text-center')
                );
            $this->table->add_row($row);
        }
        $table_report = $this->table->generate();

        $filter_arr = $this->filter_manager->load('kelas', $this->kelas_m->get_all_kelas())->get();

        // angkatan filter_arr, from tahun ajaran
        // $list_all_angkatan = $this->ta_m->get_all_ta();
        // $angkatan_options['all'] = 'Semua Angkatan';
        // foreach ($list_all_angkatan as $angkatan) {
        //     $angkatan_options[$angkatan->tahun_ajaran] = $angkatan->tahun_ajaran;
        // }
        // $dropdown_selected = isset($selected['angkatan']) ? $selected['angkatan'] : $ta_aktif->tahun_ajaran;
        // $filter_arr['Angkatan'] = form_dropdown('angkatan', $angkatan_options, $dropdown_selected, array('class' => 'form-control input-sm'));

        $site_url = base_url('admin/report/siswa');
        // publish section
        $this->set_title('Laporan Siswa');
        $this->set_breadcrumbs('Laporan', 'Laporan Siswa');
        $this->add_prop($filter_arr, 'filter');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_report, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function siswa_detail($acc_id)
    {
        $this->loader->model('siswa', 'kelas');

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-condensed table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading(array('No', 'Mata Pelajaran', 'Total Pertemuan', 'Kelas', 'Aksi'));

        $today = new DateTime();

        $siswa = $this->siswa_m->get_one($acc_id);
        $siswa->kelas = $this->kelas_m->get_kelas($siswa->kelas_id);
        $ta_aktif = $this->get_ta_aktif();

        if ($siswa !== null) {
            $kelas = $siswa->kelas_id;
        } else {
            show_error('Invalid Acccount ID');
        }

        $all_guru_matpel = $this->guru_matpel_filter($this->guru_matpel_m->get_by('kelas_id', $kelas));
        $all_guru_matpel_id = array_column($all_guru_matpel, 'id');

        $tmp_siswa_absensi = $this->abs_kbm_m->count_absensi($siswa->account_id, $all_guru_matpel_id);

        $all_siswa_absensi = array();
        foreach ($tmp_siswa_absensi as $siswa_absensi) {
            $all_siswa_absensi[$siswa_absensi->guru_matpel_id] = $siswa_absensi;
        }

        $all_properties = array();
        foreach ($all_guru_matpel as $key => $guru_matpel) {
            $total_pertemuan = $this->count_pertemuan($guru_matpel);
            if (isset($all_siswa_absensi[$guru_matpel->id])) {
                $all_properties['persentase'][$guru_matpel->id] = round(($all_siswa_absensi[$guru_matpel->id]->kehadiran / $total_pertemuan) * 100);
            } else {
                $all_properties['persentase'][$guru_matpel->id] = 0;
            }

            $action = '';
            $action = anchor(base_url('admin/report/siswa/detail/'.$siswa->account_id).'/'.$guru_matpel->id, 'Details', array('class' => 'btn btn-default btn-sm'));

            $all_guru_matpel[$key]->action = $action;
            foreach ($this->get_status_hadir() as $key => $value) {
                if (isset($all_siswa_absensi[$guru_matpel->id])) {
                    $all_properties[$value][$guru_matpel->id] = $all_siswa_absensi[$guru_matpel->id]->{strtolower($value)};
                }
                else {
                    $all_properties[$value][$guru_matpel->id] = 0;
                }
            }
        }

        $table_report = $this->table->generate();

        $this->set_title('Laporan Kehadiran Siswa');
        $this->set_breadcrumbs('Laporan', 'Laporan Siswa', 'Details');
        $this->add_prop($siswa, 'siswa');
        $this->add_prop($all_guru_matpel, 'all_guru_matpel');
        $this->add_prop($ta_aktif, 'ta_aktif');
        $this->add_prop($today->format('d M Y'), 'today');
        $this->add_prop($table_report, 'table');
        $this->add_prop($all_properties, 'all_properties');
        $this->compile_page('public/ta-badge', 'ta_badge');
        $this->compile_page('public/siswa-badge', 'siswa_badge');
        $this->compile_page('public/detail-siswa');
        $this->publish();
    }

    public function siswa_ba($acc_id, $guru_matpel_id)
    {
        $this->loader->model('siswa', 'kelas', 'matpel', 'berita_acara', 'disabled_date');
        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-condensed table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading(array('No', 'Mata Pelajaran', 'Total Pertemuan', 'Kelas', 'Aksi'));

        $today = new DateTime();

        $siswa = $this->siswa_m->get_one($acc_id);
        $siswa->kelas = $this->kelas_m->get_kelas($siswa->kelas_id);
        $ta_aktif = $this->get_ta_aktif();

        if ($siswa !== null) {
            $kelas = $siswa->kelas_id;
            $siswa = $this->assign_dropdown($siswa);
        } else {
            show_error('Invalid Acccount ID');
        }

        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        if ($guru_matpel === null) {
            show_error('Invalid Acccount ID');
        }

        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        $tanggal_opt = array();
        $tmp_berita_acara = $this->ba_m->get_all_ba($guru_matpel_id);
        $berita_acara = $this->generate_ba($tmp_berita_acara);

        $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;

        $tmp_abs = $this->abs_kbm_m->get_absensi_all($tahun, null, $guru_matpel->id, array('guru_matpel_id' => $guru_matpel_id, 'account_id' => $siswa->account_id));

        $all_absensi = array();
        foreach ($tmp_abs as $abs) {
            $all_absensi[$abs->tanggal][$abs->hours] = $abs->hadir;
        }

        $all_pertemuan = array();
        $this->table->set_template(array('table_open' => '<table class="table table-condensed table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading('No', 'Tanggal', 'Materi', 'Tugas Siswa', 'Aksi');
        $this->counter_init();
        for ($i=1; $i < 13; $i++) {
            $tmp_tanggal = $this->generate_tanggal($guru_matpel, $i);
            foreach ($tmp_tanggal['date'] as $bulan => $list_tanggal) {
                foreach ($list_tanggal as $tanggal => $list_jadwal) {
                    foreach ($list_jadwal as $key => $jadwal) {
                        $tmp_ba = isset($berita_acara[$bulan][$tanggal][$jadwal]) ? $berita_acara[$bulan][$tanggal][$jadwal] : '';

                        $date = new DateTime($tanggal.' '.mdate('%M', strtotime($bulan)).' '.$tahun);
                        $tmp['tanggal'] = $date->format('d-M-Y');
                        $tmp['hari'] = $this->get_hari($date->format('N'));
                        $tmp['pukul'] = $jadwal;
                        $tmp['materi'] = $tmp_ba ? $tmp_ba['materi'] : '';
                        $tmp['tugas_siswa'] = $tmp_ba ? $tmp_ba['tugas_siswa'] : '';

                        if (isset($all_absensi[$date->format('Y-m-d')][$jadwal])) {
                            $tmp['kehadiran'] = $this->get_status_hadir($all_absensi[$date->format('Y-m-d')][$jadwal]);
                        } else {
                            $tmp['kehadiran'] = '';
                        }
                        $all_pertemuan[] = $tmp;
                    }
                }
            }
        }

        $table_report = $this->table->generate();

        $this->set_title('Laporan Kehadiran Siswa');
        $this->set_breadcrumbs('Laporan', 'Laporan Siswa', 'Details');
        $this->add_prop($siswa, 'siswa');
        $this->add_prop($all_pertemuan, 'all_pertemuan');
        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->add_prop($ta_aktif, 'ta_aktif');
        $this->add_prop($today->format('F j, Y'), 'today');
        $this->add_prop($table_report, 'table');
        $this->compile_page('public/ta-badge', 'ta_badge');
        $this->append_page('public/guru-badge-minimal', 'ta_badge');
        $this->compile_page('public/siswa-badge', 'siswa_badge');
        $this->compile_page('public/detail-siswa-ba');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    /**
     * View for detail absensi
     *
     * @param  int $guru_matpel_id
     * @return void
     */
    public function detail_attendance($guru_matpel_id)
    {
        $this->detail_absensi($guru_matpel_id);
    }
}
