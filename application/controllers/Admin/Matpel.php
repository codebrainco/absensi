<?php
class Matpel extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('matpel');
    }

    public function index()
    {
        $this->loader->library('table');
        $all_matpel = $this->matpel_m->get_all() ?: array();

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading(array('ID', 'Nama Matpel', 'Aksi'));
        $this->counter_init();
        foreach ($all_matpel as $matpel) {
            $action = '';
            $action .= anchor(base_url('admin/master/matpel/edit/'.$matpel->id), 'Edit', array('class' => 'btn btn-sm btn-space btn-default'));
            $action .= anchor(base_url('admin/master/matpel/remove/'.$matpel->id), 'Hapus', array('class' => 'btn btn-sm btn-space btn-default red white-text'));
            $row = array(
                $matpel->id,
                $matpel->matpel,
                array('data' => $action, 'class' => 'text-center')
                );
            $this->table->add_row($row);
        }
        $table_matpel = $this->table->generate();

        // publish section
        $interaction = anchor(base_url('admin/master/matpel/add'), 'Tambah Mata Pelajaran', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->add_prop($interaction, 'interaction');
        $this->set_title('Mata Pelajaran');
        $this->set_breadcrumbs('Data Master', 'Mata Pelajaran');
        $this->add_prop($table_matpel, 'table');
        $this->append_page('public/card-table');
        $this->publish();
    }

    /**
     * Remove matpel from matpel_table
     *
     * @param  int $matpel_id
     * @return void
     */
    public function remove($matpel_id, $lm = false)
    {
        $matpel_name = $this->matpel_m->get($matpel_id)->matpel;
        $this->add_message('Matpel : '.$matpel_name.' berhasil di hapus', true, 1);
        $this->matpel_m->delete($matpel_id);
        redirect('admin/master/matpel'.($lm ? '_lm' : ''));
    }

    // -------------------------------------------------------------------------

    public function do_edit($matpel_id)
    {
        $post = $this->prep_input_obj('matpel');
        $checked_pair = array('matpel' => $post->matpel);

        if ($this->matpel_m->is_unique($checked_pair, $matpel_id) === true) {
            if ($matpel_id == 0) {
                $this->matpel_m->insert($post);
            } else {
                $this->matpel_m->update($post, $matpel_id);
            }
            $this->add_message('Data successfully inputted', true, 1);
            return true;
        } else {
            $this->add_message('Duplicate found', false, 2);
            return false;
        }
    }

    public function edit($matpel_id)
    {
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            if ($this->do_edit($matpel_id, 'matpel')) {
                redirect('admin/master/matpel');
            }
        }

        // build form
        $matpel = $this->matpel_m->get($matpel_id);
        $heading_field = array(
            'Nama Mata Pelajaran' => 'matpel|text',
            );
        $input = $this->form_generator($heading_field, $matpel, 'form-control');

        // publish section
        $this->set_title($this->title_add_or_edit($matpel_id, ' Mata Pelajaran'));
        $this->set_breadcrumbs('Data Master', 'Mata Pelajaran', 'Edit');
        $this->add_prop($input, 'input');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }
}
