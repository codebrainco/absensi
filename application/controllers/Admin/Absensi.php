<?php
class Absensi extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('absensi_kbm');
    }

    // -------------------------------------------------------------------------

    /**
     * View for edtting absensi
     * @param  int $guru_matpel_id
     * @return void
     */
    public function index($guru_matpel_id, $lm = false)
    {
        $this->loader->model('guru_matpel', 'tahun_ajaran', 'disabled_date', 'siswa');
        $this->loader->helper('form');
        // fetch common items
        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        // get periode from GET but set default with begining of semester (January/July)
        $periode = $this->input->get('periode');
        if (isset($periode)) {
            // check if periode is outside range
            if ($this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0) {
                ($periode >= 1 && $periode <= 6) or redirect('admin/kbm/guru_matpel');
            } else {
                ($periode >= 7 && $periode <= 12) or redirect('admin/kbm/guru_matpel');
            }
        } else {
            $periode = $this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        }

        // form hidden for tahun since its not exist in url
        $form_tahun = form_hidden('tahun', $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun);

        $available_angkatan = $this->get_allowed_angkatan();

        // get dependencies

        // all absensi based on tahun ajaran and guru matpel id
        $tmp_abs = $this->abs_kbm_m->get_absensi_all($guru_matpel->tahun_ajaran, null, $guru_matpel_id);

        // all siswa from avaliable angkatan, and kelas and lintas minat
        if ($lm) {
            $all_siswa = $this->siswa_m->get_siswa_lm($guru_matpel->kelas_id, $available_angkatan);
        } else {
            $all_siswa = $this->siswa_m->with_account()->where('angkatan', $available_angkatan)->where('kelas_id', $guru_matpel->kelas_id)->get_all() ?: array();
        }

        // all tanggal if based on periode
        $all_dates = $this->generate_tanggal($guru_matpel, $periode);

        // fetch dropdown
        $all_dropdown = $this->generate_absensi($tmp_abs, true);

        // and status hadir
        $status_hadir = $this->get_status_hadir();

        // fetch bulan, if semester is Ganjil start from July to end, January otherwise
        $begin = $this->ta_m->get($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        $bulan_opt = array();
        for ($i=$begin; $i < $begin+6; $i++) {
            $bulan_opt[$i] = $this->get_bulan_eng($i);
        }
        $available_bulan = form_dropdown('periode', $bulan_opt, $periode, array('class' => 'form-control'));

        // build option none for null absensi
        $option_none = '';
        foreach ($status_hadir as $key => $hadir) {
            $option_none .= '<option value="'.($key).'">'.$hadir.'</option>';
        }

        // publish section
        $this->set_title('Edit Absensi');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel', 'Edit Absensi');
        $this->add_prop($form_tahun, 'form_tahun');
        $this->add_prop($all_dropdown, 'status_dropdown');
        $this->add_prop($available_bulan, 'available_bulan');
        $this->add_prop($option_none, 'option_none');
        $this->add_prop($this->get_status_hadir(), 'status_hadirs');
        $this->add_prop($this->session->referral_url, 'referral_url');
        $this->add_prop($all_siswa, 'all_siswa');
        $this->add_prop($all_dates['date'], 'all_dates');
        $this->add_prop($all_dates['day'], 'all_day');
        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->compile_page('public/guru-badge', 'guru_badge');
        $this->compile_page($this->get_site_root('kbm_absensi_edit'));
        $this->publish();
    }
}
