<?php
class Jadwal extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('guru_matpel', 'guru_matpel_jadwal');
    }

    public function do_edit($guru_matpel, $lm)
    {
        $post = $this->input->post('jadwal');
        $token = true;

        // do validation for jadwal
        $post = $this->jadwal_validate($post, $guru_matpel);

        if ( ! empty($post)) {
            // prepare inset data
            foreach ($post as $hari => $jadwal_ke) {
                foreach ($jadwal_ke as $key => $jadwal) {
                    $insert_data[] = array(
                        'guru_matpel_id' => $guru_matpel->id,
                        'hari' => $hari,
                        'begin_hrs' => $jadwal['start'],
                        'end_hrs' => $jadwal['end'],
                        );
                }
            }

            // do 'replace', delete first then add new
            $this->guru_matpel_jadwal_m->where('guru_matpel_id', $guru_matpel->id)->delete();
            $this->guru_matpel_jadwal_m->insert_jadwal($insert_data);
            $this->add_message('Data berhasil dirubah', true, 1);
            redirect($lm ? 'admin/kbm/guru_matpel_lm' : 'admin/kbm/guru_matpel');
        }
    }

    public function edit($guru_matpel_id, $lm = false)
    {
        $this->loader->library('table');
        $this->loader->helper('form');

        // fetch common items
        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        if (isset($_POST['submit'])) {
            if ($this->do_edit($guru_matpel, $lm)) {
                redirect('admin/kbm/guru_matpel'.$lm ? "_lm" : '', 'refresh');
            }
        }

        // fetch items
        $list_hari = $this->get_hari();
        $list_jadwal = $this->get_jadwal($guru_matpel_id);

        $jadwal_arr = array();
        foreach ($list_jadwal as $jadwal) {
            $jadwal_arr[$jadwal['hari']][] = $jadwal;
        }

        // build input table
        $heading = array('No');
        $heading = array_merge($heading, $list_hari);
        $this->table->set_template(array(
            'table_open' => '<table class="table table-absen table-striped table-bordered table-condensed">',
            'cell_alt_start' => '<td class="info">'
            ));
        $this->table->set_heading($heading);
        $this->counter_init();
        for ($i = 0; $i < 10; $i++) {
            $row = array($this->counter_inc());
            foreach ($list_hari as $key => $hari) {
                $opt1 = array(
                    'name' => 'jadwal['.$key.']['.$i.'][start]',
                    'type' => 'time',
                    'class' => 'form-control',
                    'value' => isset($jadwal_arr[$key][$i]) ? $jadwal_arr[$key][$i]['begin_hrs'] : '',
                    );
                $opt2 = array(
                    'name' => 'jadwal['.$key.']['.$i.'][end]',
                    'type' => 'time',
                    'class' => 'form-control',
                    'value' => isset($jadwal_arr[$key][$i]) ? $jadwal_arr[$key][$i]['end_hrs'] : '',
                    );
                $row[] = form_input($opt1).'<br>s/d<br>'.form_input($opt2);
            }
            $this->table->add_row($row);
        }

        $table_jadwal = $this->table->generate();

        // publish section
        $this->set_title('Edit Jadwal', 'Edit jadwal, absen lama tidak akan terpengaruh');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel', 'Edit Jadwal');
        $this->add_prop($this->session->referral_url, 'referral_url');
        $this->add_prop($table_jadwal, 'table_jadwal');
        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->compile_page('public/guru-badge', 'guru_badge');
        $this->compile_page($this->get_site_root('kbm_jadwal_edit'));
        $this->publish();
    }

    /**
     * Validation for jadwal if it has conflict with other matpel or conflict time
     * with itself
     *
     * @param  object $post        post_data
     * @param  object $guru_matpel guru_matpel data
     * @return object|bool              return filtered post_data or false if fail
     */
    public function jadwal_validate($post, $guru_matpel)
    {
        $input = $post;

        // unset uneeded post (zero value jam)
        foreach ($post as $hari => $jam_ke) {
            foreach ($jam_ke as $key => $jam) {
                // unset it if its hari is empty
                if (empty($jam['start']) !== false) {
                    unset($post[$hari][$key]);
                    unset($input[$hari][$key]);
                } else {
                    // check for backward time
                    if (strtotime($post[$hari][$key]['start']) >= strtotime($post[$hari][$key]['end'])) {
                        $this->add_message('Terjadi "waktu mundur" !, pastikan jam nya benar !', false, 2);
                        return false;
                    } else {
                        // make it completelty opearator-friendly comparison
                        $post[$hari][$key]['start'] = strtotime($post[$hari][$key]['start']);
                        $post[$hari][$key]['end'] = strtotime($post[$hari][$key]['end']);
                    }
                }
            }
            // and empty hari if didnt have any jadwal
            if (empty($post[$hari]) !== false) {
                unset($post[$hari]);
                unset($input[$hari]);
            }
        }

        $data = $this->guru_matpel_m->where(array('account_id' => $guru_matpel->account_id, 'tahun_ajaran_id' => $guru_matpel->tahun_ajaran_id))->get_all();

        $guru_matpel_arr = array();

        foreach ($data as $one_guru_matpel) {
            if ($one_guru_matpel->id !== $guru_matpel->id) {
                $guru_matpel_arr[] = $one_guru_matpel->id;
            }
        }

        if (empty($guru_matpel_arr)) {
            $possible_double_jadwal = array();
        } else {
            $possible_double_jadwal = $this->guru_matpel_jadwal_m->where('guru_matpel_id', $guru_matpel_arr)->get_all() ?: array();
        }

        // check every posted data with possible conflict (outter)
        foreach ($post as $hari => $jadwal_ke) {
            foreach ($jadwal_ke as $key => $post_jadwal) {
                foreach ($possible_double_jadwal as $jadwal) {
                    // operator-friendly comparison
                    $jadwal->unix_begin_hrs = strtotime($jadwal->begin_hrs);
                    $jadwal->unix_end_hrs = strtotime($jadwal->end_hrs);

                    // if it has same day
                    $in_days = ($jadwal->hari == $hari);
                    // if one of jam posted is in between someone class
                    // and if both of jam posted is outfill someone class
                    $in_time = ($post_jadwal['start'] > $jadwal->unix_begin_hrs && $post_jadwal['start'] < $jadwal->unix_end_hrs) ||
                                         ($post_jadwal['end'] > $jadwal->unix_begin_hrs && $post_jadwal['end'] < $jadwal->unix_end_hrs) ||
                                         ($post_jadwal['start'] <= $jadwal->unix_begin_hrs && $post_jadwal['end'] >= $jadwal->unix_end_hrs);

                    if ($in_days && $in_time) {
                        $this->add_message('Guru Tersebut sudah mengajar untuk guru_matpel_id : '.$jadwal->guru_matpel_id.' pada hari : '.$this->get_hari($hari)." <a target='_blank' href='".base_url('admin/kbm/jadwal/edit/'.$jadwal->guru_matpel_id)."'>Klik untuk melihat</a>", false, 2);
                        return false;
                    }
                }
            }
        }


        // get possible jadwal conflict from guru_matpel with same kelas and same
        // tahun ajaran but not itself (outter)
        $data = $this->guru_matpel_m->where(array('kelas_id' => $guru_matpel->kelas_id, 'tahun_ajaran_id' => $guru_matpel->tahun_ajaran_id))->get_all();

        $guru_matpel_arr = array();

        foreach ($data as $one_guru_matpel) {
            if ($one_guru_matpel->id !== $guru_matpel->id) {
                $guru_matpel_arr[] = $one_guru_matpel->id;
            }
        }
        if ( ! empty($guru_matpel_arr)) {
            $possible_jadwal_conflict = $this->guru_matpel_jadwal_m->where('guru_matpel_id', $guru_matpel_arr)->get_all() ?: array();
        } else {
            $possible_jadwal_conflict = array();
        }

        // check every posted data with possible conflict (outter)
        foreach ($post as $hari => $jadwal_ke) {
            foreach ($jadwal_ke as $key => $post_jadwal) {
                foreach ($possible_jadwal_conflict as $jadwal) {
                    // operator-friendly comparison
                    $jadwal->unix_begin_hrs = strtotime($jadwal->begin_hrs);
                    $jadwal->unix_end_hrs = strtotime($jadwal->end_hrs);

                    // if it has same day
                    $in_days = ($jadwal->hari == $hari);

                    // if one of jam posted is in between someone class
                    // and if both of jam posted is outfill someone class
                    $in_time = ($post_jadwal['start'] > $jadwal->unix_begin_hrs && $post_jadwal['start'] < $jadwal->unix_end_hrs) ||
                                         ($post_jadwal['end'] > $jadwal->unix_begin_hrs && $post_jadwal['end'] < $jadwal->unix_end_hrs) ||
                                         ($post_jadwal['start'] <= $jadwal->unix_begin_hrs && $post_jadwal['end'] >= $jadwal->unix_end_hrs);

                    if ($in_days && $in_time) {
                        $this->add_message('Terjadi konflik dengan guru_matpel ID : '.$jadwal->guru_matpel_id.' pada hari : '.$this->get_hari($hari)." <a target='_blank' href='".base_url('admin/kbm/jadwal/edit/'.$jadwal->guru_matpel_id)."'>Klik untuk melihat</a>", false, 2);
                        return false;
                    }
                }
            }
        }

        // Check for inside conflict (inner)
        foreach ($post as $hari => $jadwal_ke) {
            for ($i = 0; $i < count($jadwal_ke); $i++) {
                for ($j = ($i+1); $j < count($jadwal_ke); $j++) {
                    // if one of jam posted is in between other itself class
                    // and if both of jam posted is outfill other itself class
                    if (($jadwal_ke[$i]['start'] > $jadwal_ke[$j]['start'] && $jadwal_ke[$i]['start'] < $jadwal_ke[$j]['end']) ||
                         ($jadwal_ke[$i]['end'] > $jadwal_ke[$j]['start'] && $jadwal_ke[$i]['end'] < $jadwal_ke[$j]['end']) ||
                         ($jadwal_ke[$i]['start'] <= $jadwal_ke[$j]['start'] && $jadwal_ke[$i]['end'] >= $jadwal_ke[$j]['end'])) {
                        $this->add_message('Terjadi konflik dalam !, pastikan setiap jam nya benar !', false, 2);
                        return false;
                    }
                }
            }
        }

        // return false;

        return $input;
    }
}
