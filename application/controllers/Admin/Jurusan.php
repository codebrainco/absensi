<?php
class Jurusan extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('jurusan');
    }

    public function index()
    {
        // fetch common items
        $this->loader->library('table');
        $list_all_jurusan = $this->jurusan_m->get_all() ?: array();

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-striped">'));
        $this->table->set_heading('No', 'Jurusan', 'Deskripsi', 'Aksi');
        $this->counter_init();
        foreach ($list_all_jurusan as $jurusan) {
            $action = '';
            $action .= anchor(base_url("admin/master/jurusan/edit/".$jurusan->id), "Edit", array('class' => 'btn btn-space btn-sm btn-default'));
            $action .= anchor(base_url("admin/master/jurusan/remove/".$jurusan->id), "Hapus", array('class' => 'btn btn-space btn-sm btn-default red white-text'));
            $row = array(
                $this->counter_inc(),
                $jurusan->jurusan,
                $jurusan->deskripsi,
                array('data' => $action, 'class' => 'text-center')
                );
            $this->table->add_row($row);
        }
        $table_jurusan = $this->table->generate();

        // publish section
        $interaction = anchor(base_url('admin/master/jurusan/add'), 'Tambah Jurusan', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->set_title('Jurusan', 'Daftar jurusan dan deskripsinya');
        $this->set_breadcrumbs('Data Master', 'Jurusan');
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($table_jurusan, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    public function do_edit($jurusan_id)
    {
        $post = $this->prep_input('jurusan', 'deskripsi');
        $checked_pair = array('jurusan' => $post['jurusan']);
        if ( ! $this->jurusan_m->is_unique($checked_pair, $jurusan_id)) {
            $this->add_message('Jurusan tidak unique', false, 2);
            return false;
        }
        if ($jurusan_id == 0) {
            $this->jurusan_m->insert($post);
        } else {
            $this->jurusan_m->update($post, $jurusan_id);
        }
        return true;
    }

    // -------------------------------------------------------------------------

    /**
     * View for editting and adding jurusan
     * @param  int $jurusan_id
     * @return void
     */
    public function edit($jurusan_id)
    {
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            if ($this->do_edit($jurusan_id)) {
                $this->add_message('Perubahan berhasil disimpan', true, 1);
                redirect('admin/master/jurusan');
            }
        }

        // build form
        $jurusan = array();
        if ($jurusan_id != 0) {
            $jurusan = $this->jurusan_m->get($jurusan_id);
        }
        $field = array(
            'Nama Jurusan' => 'jurusan|text',
            'Deskripsi' => 'deskripsi|text',
            );
        $input = $this->form_generator($field, isset($jurusan) ? $jurusan : null, 'form-control');

        // publish section
        $this->set_title($this->title_add_or_edit($jurusan_id, ' Jurusan'), 'Jurusan', 'Menambahkan atau melakukan perubahan pada data jurusan');
        $this->set_breadcrumbs('Data Master', 'Jurusan', 'Edit');
        $this->add_prop($input, 'input');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    /**
     * Remove jurusan
     *
     * @param  int $jurusan_id
     * @return void
     */
    public function remove($jurusan_id)
    {
        $jurusan_name = $this->jurusan_m->get($jurusan_id)->jurusan;
        $this->add_message('Jurusan : '.$jurusan_name.' berhasil di hapus', true, 1);
        $this->jurusan_m->delete($jurusan_id);
        redirect('admin/master/jurusan');
    }
}
