<?php
class Tahun_ajaran extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('tahun_ajaran');
    }

    public function index()
    {
        $this->loader->library('table');

        // fetch common items
        $all_ta = $this->ta_m->order_by('begin_date', 'ASC')->get_all_ta();
        $ta_aktif = $this->get_ta_aktif();

        // build table
        $template = array('table_open' => '<table class="table table-bordered">');
        $heading = array('ID', 'Tahun Akademik', 'Semester', 'Tanggal Mulai', 'Tanggal Berakhir', 'Status', 'Aksi');
        $this->table->set_template($template);
        $this->table->set_heading($heading);
        foreach ($all_ta as $ta) {
            $action = anchor(base_url("admin/master/ta/edit/".$ta->id), "Edit", array('class' => $this->get_html_prop('button-default-sm')));
            $action .= anchor(base_url("admin/master/ta/remove/".$ta->id), "Hapus", array('class' => 'red white-text '.$this->get_html_prop('button-default-sm')));
            $action .= anchor(base_url("admin/master/ta/activate/".$ta->id), " Aktifkan", array('class' => 'teal lighten-1 white-text '.$this->get_html_prop('button-default-sm')));

            $tmp_row = array(
                $ta->id,
                $ta->tahun_ajaran,
                $this->get_semester($ta->semester),
                nice_date($ta->begin_date, 'd F Y'),
                nice_date($ta->end_date, 'd F Y'),
                '',
                $action
                );

            // is active ?, add info class to it's <td>
            $row = array();
            if ($ta_aktif && $ta_aktif->id == $ta->id) {
                foreach ($tmp_row as $value) {
                    $row[] = array('data' => $value, 'class' => 'info');
                }
                $action = array_pop($row);
                array_pop($row);
                $row[] = array('data' => 'Aktif', 'class' => 'info');
                $row[] = $action;
            } else {
                $row = $tmp_row;
            }

            $this->table->add_row($row);
        }
        $table_ta = $this->table->generate();
        $interaction = anchor(base_url('admin/master/ta/add'), 'Tambah Tahun Akademik', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));

        // publish section
        $this->set_title('Tahun Akademik', 'Memuat semua tahun akademik dan konfigurasi default untuk setiap data yang berhubungan dengan tahun akademik');
        $this->set_breadcrumbs('Data Master', 'Tahun Akademik');
        $this->add_prop($table_ta, 'table');
        $this->add_prop($interaction, 'interaction');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    public function do_edit($ta_id)
    {
        $this->loader->library('form_validation');

        $post = $this->prep_input('tahun_ajaran', 'semester', 'begin_date', 'end_date');

        if ($this->form_validation->run('tahun_ajaran') === true) {
            if ($ta_id != 0) {
                $this->ta_m->update($post, $ta_id);
            } else {
                $this->ta_m->insert($post);
            }
            $this->add_message('Data berhasil di update', true, 1);
            return true;
        } else {
            $this->add_message(validation_errors(), false, 2);
            return false;
        }

    }

    /**
     * View for tahun ajaran editting
     *
     * @param  int $ta_id tahun_ajaran id
     * @return void
     */
    public function edit($ta_id)
    {
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            if ($this->do_edit($ta_id)) {
                redirect('admin/master/ta', 'refresh');
            }
        }

        // build form
        $tahun = $this->ta_m->get($ta_id);
        $field = array(
            'Tahun Akademik' => 'tahun_ajaran|text|placeholder[2015/2016]',
            'Tanggal Mulai' => 'begin_date|date',
            'Tanggal Berakhir' => 'end_date|date',
            );

        $input = $this->form_generator($field, $tahun, 'form-control');
        $input['Semester'] = form_dropdown('semester', $this->get_semester(), isset_val($tahun, 'semester'), array('class' => 'form-control custom-select select select-primary select-block mbl',));

        // publish section
        $this->set_title($this->title_add_or_edit($ta_id, 'Tahun Akademik'), 'Menambahkan atau melakukan perubahan pada data tahun akademik');
        $this->set_breadcrumbs('Data Master', 'Tahun Akademik', 'Edit');
        $this->add_prop($input, 'input');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }

    /**
     * Activate current ta by changing config
     *
     * @param  int $ta_id
     * @return void
     */
    public function activate($ta_id)
    {
        $this->loader->model('config');
        $update_data = array(
            'ta_aktif' => $ta_id
            );

        // update config then fetch again to refresh
        $this->config_m->update_config($update_data);

        redirect(base_url('admin/master/ta'));
    }

    // -------------------------------------------------------------------------

    /**
     * Remove tahun_ajaran
     *
     * @param  int $ta_id
     * @return void
     */
    public function remove($ta_id)
    {
        // is active ?
        if ($this->get_ta_aktif()->begin_date == $this->ta_m->get($ta_id)->begin_date) {
            $this->add_message('Tahun Akademik sedang aktif', true, 3);
        } else {
            $this->add_message('Tahun ajaran berhasil di hapus', true, 1);
            $this->ta_m->delete($ta_id);
        }
        redirect(base_url('admin/master/ta'));
    }
}
