<?php
class Config extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('config');
    }

    public function index()
    {
        $this->loader->library('table');
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            $input = $this->prep_input_obj('config');
            $this->config_m->update_config($input->config);
            $this->add_message('Config Updated', false, 1);
        }

        // build table
        $template = array('table_open' => '<table class="table table-striped table-condensed">');
        $heading = array('No', 'Nama', 'Value', 'Deskripsi');
        $this->table->set_template($template);
        $this->table->set_heading($heading);
        $data = $this->config_m->get_all();
        $this->counter_init();
        foreach ($data as $ta) {
            $row = array(
                $this->counter_inc(),
                $ta->prop_name,
                form_input('config['.$ta->prop_name.']', $ta->prop_value, 'class="form-control" size="25"'),
                $ta->prop_desc,
                );
            $this->table->add_row($row);
        }
        $table_config = $this->table->generate();
        $interaction = form_submit('submit', 'Submit Edit', array('class' => $this->get_html_prop('button-success-sm')));
        // publish section
        $this->set_title('Config', 'Mengatur hal yang memerlukan konfigurasi <b>WARNING !</b> berinteraksi langsung dengan database, harap lakukan dengan hati hati');
        $this->set_breadcrumbs('Config');
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($table_config, 'table');
        $this->compile_page('admin/config');
        $this->publish();
    }
}
