<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bulk_add extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->site_root = 'admin/';
        $this->load->library('Excel');
    }

    public function account()
    {
        $this->loader->helper('form');
        if (isset($_POST['submit'])) {
            $this->loader->model('kelas', 'siswa', 'account');
            $post = $this->input->post();
            $config['upload_path']          = 'assets/tmp';
            $config['allowed_types']        = 'xls';
            $config['file_ext_tolower']     = true;
            $config['encrypt_name']        = true;

            $this->load->library('upload', $config);
            $this->load->helper('file');

            ini_set('max_execution_time', 0);
            ini_set('memory_limit', '2048M');

            if ( ! $this->upload->do_upload('massUpdateFile')) {
                $error = $this->add_message($this->upload->display_errors());
            } else {
                $upload_data = $this->upload->data();

                $file = ($upload_data['full_path']);
                // $file = ('assets/tmp/d0e054dddc3e46e9bfb3a9d8c22bb39c.xls');

                //read file from path
                if ($post['type'] == 'guru_1' || $post['type'] == 'siswa_2') {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);

                    //get only the Cell Collection
                    $cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

                    //extract to a PHP readable array format
                    foreach ($cell_collection as $cell) {
                        $column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
                        $row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
                        $data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
                        //header will/should be in row 1 only. of course this can be modified to suit your need.
                        if ($row == 1) {
                            $header[$row][$column] = $data_value;
                        } else {
                            $arr_data[$row][$column] = $data_value;
                        }
                    }

                    //send the data in an array format
                    $data['header'] = $header;
                    $data['values'] = $arr_data;

                    $account_data = array();

                    // Unset row 2 and 3, it still contain header and contain example data
                    unset($data['values'][2]);

                    $post = $this->prep_input_obj('type', 'account');

                    if ($post->type == 'guru_1') {
                        $this->add_guru($data);
                    } elseif ($post->type == 'siswa_2') {
                        $this->add_siswa($data);
                    }
                } elseif ($post['type'] == 'jadwal_1') {
                    $objPHPExcel = PHPExcel_IOFactory::load($file);

                    //get only the Cell Collection
                    $cell_collection_jadwal = $objPHPExcel->getSheet(0)->getCellCollection();

                    //extract to a PHP readable array format
                    foreach ($cell_collection_jadwal as $cell) {
                        $column = $objPHPExcel->getSheet(0)->getCell($cell)->getColumn();
                        $row = $objPHPExcel->getSheet(0)->getCell($cell)->getRow();
                        $data_value = $objPHPExcel->getSheet(0)->getCell($cell)->getValue();
                        //header will/should be in row 1 only. of course this can be modified to suit your need.
                        if ($row == 1) {
                            $header[$column] = $data_value;
                        } else {
                            $arr_data[$row][$column] = $data_value;
                        }
                    }

                    //send the data in an array format
                    $jadwal['header'] = $header;
                    unset($jadwal['header']['A']);
                    $jadwal['values'] = $arr_data;

                    //get only the Cell Collection
                    $cell_collection_guru = $objPHPExcel->getSheet(1)->getCellCollection();

                    $header = array();
                    $arr_data = array();
                    //extract to a PHP readable array format
                    foreach ($cell_collection_guru as $cell) {
                        $column = $objPHPExcel->getSheet(1)->getCell($cell)->getColumn();
                        $row = $objPHPExcel->getSheet(1)->getCell($cell)->getRow();
                        $data_value = $objPHPExcel->getSheet(1)->getCell($cell)->getValue();
                        //header will/should be in row 1 only. of course this can be modified to suit your need.
                        if ($row == 1) {
                            $header[$row][$column] = $data_value;
                        } else {
                            $arr_data[$row][$column] = $data_value;
                        }
                    }

                    $guru['header'] = $header;
                    $guru['values'] = $arr_data;

                    $guru_arr = array();
                    foreach ($guru['values'] as $guru_pair) {
                        $guru_arr[strtolower($guru_pair['A'])] = array(
                            'account_id' => $guru_pair['B'],
                            'matpel_id' => $guru_pair['C'],
                            );
                    }

                    // Mulai populating
                    $jadwal_arr = array();
                    $eol = false;
                    $guru_matpel_arr = array();
                    $list_all_kelas = $this->kelas_m->get_all_kelas();
                    $kelas_arr = array();
                    foreach ($list_all_kelas as $kelas) {
                        $kelas_arr[$kelas->id] = $kelas->kelas;
                    }

                    foreach ($jadwal['values'] as $jdw) {
                        if (isset($jdw['A']) && $jdw['A'] == 'EOL') {
                            $eol = true;
                            continue;
                        }

                        if ($eol) {
                            $hari = isset_val($jdw, 'A');
                            $eol = false;
                            continue;
                        }

                        if ($eol === false) {
                            $jam = $jdw['A'];
                            $jam = explode(' - ', str_replace('.', ':', $jam));
                            unset($jdw['A']);
                            foreach ($jdw as $col_no => $kolom) {
                                $guru_matpel_arr[] = array(
                                    'hari' => $hari,
                                    'kelas_id' => array_search($jadwal['header'][$col_no], $kelas_arr),
                                    'begin_hrs' => $jam[0].':00',
                                    'end_hrs' => $jam[1].':00',
                                    'account_id' => $guru_arr[strtolower($kolom)]['account_id'],
                                    'matpel_id' => $guru_arr[strtolower($kolom)]['matpel_id'],
                                    'tahun_ajaran_id' => $this->get_ta_aktif()->id,
                                    'total_pertemuan' => 16,
                                    );

                            }
                        }
                    }

                    // DEBUG
                    echo "<pre style='margin-top: 50px'>";
                    print_r($guru_matpel_arr);
                    echo "</pre>";

                    $post = $this->prep_input_obj('type', 'account');

                }

                delete_files($upload_data['full_path']);
                $this->add_message('Update success');
            }
        }

        $this->set_title('Mass Add');
        $this->set_breadcrumbs('Data Account', 'Mass Add');
        $this->compile_page($this->get_site_root('account_bulk_add'));
        $this->publish();
    }

    public function add_guru($data)
    {
        foreach ($data['values'] as $col) {
            $tmp_uname = explode(' ', $col['C']);
            $input_account = array(
                'identity' => strtolower($tmp_uname[0].isset_val($tmp_uname, '1') ?: ''.rand(1, 9)),
                'password' => '123456789',
                'email' => 'dummy@sma-almuttaqin-tasikmalaya.sch.id',
                'group' => '3'
                );

            while ($this->ion_auth->username_check($input_account['identity'])) {
                $input_account['identity'] = strtolower($tmp_uname[0].rand(1, 9));
            }

            $insert_id = $this->acc_m->my_insert($input_account);

            $input_personal = array(
                'account_id' => $insert_id,
                'nip' => isset_val($col, 'A'),
                'niy' => isset_val($col, 'B'),
                'nama' => isset_val($col, 'C'),
                // 'tanggal_lahir' => array_search($col['D'], $kelas_arr) ?: null
                );

            $this->guru_m->insert($input_personal);
        }
    }

    public function add_siswa($data)
    {
        foreach ($data['values'] as $col) {
            if (empty($col['A']) === false) {
                $tmp_uname = explode(' ', $col['B']);
                $input_account = array(
                    'identity' => strtolower($tmp_uname[0].substr($col['A'], 3, 6)),
                    'password' => $col['A'],
                    'email' => 'dummy@sma-almuttaqin-tasikmalaya.sch.id',
                    'group' => '2'
                    );
                $insert_id = $this->acc_m->my_insert($input_account);

                $list_all_kelas = $this->kelas_m->get_all_kelas();
                $kelas_arr = array();
                foreach ($list_all_kelas as $kelas) {
                    $kelas_arr[$kelas->id] = $kelas->kelas;
                }

                $tmp_angkatan1 = '20'.substr($col['A'], 0, 2);
                $tmp_angkatan2 = '20'.substr($col['A'], 2, 2);

                $input_personal = array(
                    'account_id' => $insert_id,
                    'angkatan' => $tmp_angkatan1.'/'.$tmp_angkatan2,
                    'nis' => $col['A'],
                    'nama' => $col['B'],
                    'jenis_kelamin' => strtolower($col['C']) == 'l' ? 0 : 1,
                    'kelas_id' => array_search($col['D'], $kelas_arr) ?: null
                    );

                $this->siswa_m->insert($input_personal);
            }
        }
    }
}
