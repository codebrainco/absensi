<?php
class Account extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->library('filter_manager', 'table', 'excel');
        $this->loader->helper('form');
    }

    // -------------------------------------------------------------------------

    /**
     * Base for rendering *
     * @param  string $group group name that used for model and pair
     * @return void
     */
    public function base($group)
    {
        // dependencies variable
        $model = $this->to_model($group);
        $pair_func = 'get_'.$group.'_pair';

        // fetch only this item
        if ($group == 'siswa') {
            $headings = array('NIS', 'Nama', 'Kelas');
        } elseif ($group == 'guru') {
            $headings = array('NIP', 'Nama', 'Tanggal Lahir');
        } else {
            die();
        }

        // get list of fields
        $fields = array();
        foreach ($headings as $heading) {
            $field = $this->$pair_func($heading);
            if ($field !== null) {
                $fields[] = $field;
            }
        }

        $fields_str = implode(', ', $fields);

        // get data
        $filter = $this->init_filter($this->get_siswa_pair());
        $get = $this->input->get();
        if ($get === null || empty($get) !== false) {
            // siswa will have current avaliable angkatan (3 last tahun ajaran) as default
            if ($group == 'siswa') {
                $angkatan = $this->get_allowed_angkatan();
                $accounts = $this->siswa_m->get_by_angkatan($fields_str, $angkatan);
            } else {
                $accounts = $this->$model->get_all_acc($fields_str);
            }
        } else {
            $accounts = $this->$model->get_filtered($fields_str, $filter);
        }

        // prepare heading based on $pair_filter or all from *_pair
        $heading_arr = array('No');
        if (empty($headings) === false) {
            foreach ($headings as $heading) {
                $heading_arr[] = $heading;
            }
        } else {
            foreach ($this->$pair_func() as $heading => $field) {
                $heading_arr[] = $heading;
            }
        }
        $heading_arr[] = 'Aksi';

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-condensed table-bordered table-sort" width=\'100%\'>'));
        $this->table->set_heading($heading_arr);
        $this->counter_init();
        foreach ($accounts as $acc) {
            // check dropdown
            if ( ! empty($acc->kelas)) {
                $acc->kelas_id = $acc->kelas->kelas;
            }

            // build row, start with counter,
            $row = array($this->counter_inc());
            foreach ($headings as $heading) {
                $row[] = $acc->{$this->$pair_func($heading)};
            }
            $action = '';
            $action .= anchor(base_url('admin/account/'.$group.'/edit/'.$acc->account_id), 'Lihat', array('class' => $this->get_html_prop('button-default-xs')));
            $row[] = array('data' => $action, 'class' => 'text-center');
            $this->table->add_row($row);
        }
        $table_acc = $this->table->generate();

        // publish section
        $site_url = base_url('admin/account/'.$group);
        $this->session->referral_url = $site_url;
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_acc, 'table');
        $this->append_page('public/card-table');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    /**
     * View for guru, view only
     * @return void
     */
    public function guru()
    {
        $this->loader->model('guru');

        // publish section
        $interaction = anchor('admin/account/guru/add', 'Tambah Guru', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->add_prop($interaction, 'interaction');
        $this->set_title('Guru', 'Daftar semua guru yang pernah terdaftar dalam database.');
        $this->set_breadcrumbs('Data Account', 'Guru');
        $this->base('guru');
    }

    // -------------------------------------------------------------------------

    /**
     * View for siswa, view only
     * @return void
     */
    public function siswa()
    {
        $this->loader->model('siswa', 'kelas');

        // kelas filter, with get from all kelas
        $filter = $this->filter_manager->load('kelas', $this->kelas_m->get_all_kelas())->get();

        // publish section
        $interaction = anchor('admin/account/siswa/add', 'Tambah Siswa', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($filter, 'filter');
        $this->set_title('Siswa', 'Daftar semua siswa yang pernah terdaftar dalam database.');
        $this->set_breadcrumbs('Data Account', 'Siswa');
        $this->base('siswa');
    }

    // -------------------------------------------------------------------------

    /**
     * Base for rendering account editting
     * @param  string $group      in where group he/she is in
     * @param  int $acc_id
     * @return void
     */
    public function edit_base($group, $acc_id)
    {
        $this->loader->model('guru', 'siswa', 'kelas');
        $model = $this->to_model($group);
        $account = $this->$model->get_one($acc_id);

        if ( ! empty($account) || $acc_id == 0) {
            $groups = array();
            if ($acc_id != 0) {
                foreach ($this->ion_auth->get_users_groups($acc_id)->result() as $ion_group) {
                    $groups[] = $ion_group->description;
                }
            }

            // build form from siswa_pair || guru_pair
            $pair_func = 'get_'.$group.'_pair';
            $personal_pair = $this->$pair_func();
            $personal_field = array();
            foreach ($personal_pair as $heading_pair => $field) {
                $personal_field[$heading_pair] = $field.'|text';
            }

            // special for tanggal lahir (Both of them have)
            $personal_field['Tanggal Lahir'] = 'tanggal_lahir|date';

            // and some specific for siswa
            if ($group == 'siswa') {
                $personal_field['Tanggal Pendaftaran'] = 'tanggal_pendaftaran|date';
            }

            // build form first
            $input_personal = $this->form_generator($personal_field, $account, 'form-control');

            // then update some item that required to be dropdown/textarea for siswa
            if ($group == 'siswa') {
                // for dropdown
                $siswa_opt = $this->get_siswa_opt();
                foreach ($siswa_opt as $heading => $dropdown) {
                    if (isset($account->{$personal_pair[$heading]})) {
                        $selected = $account->{$personal_pair[$heading]};
                    } elseif ($selected = $this->get_siswa_opt_selected($personal_pair[$heading]) !== null) {
                    } else {
                        $selected = '';
                    }
                    $input_personal[$heading] = form_dropdown($personal_pair[$heading], $dropdown, $selected, array('class' => 'form-control custom-select'));
                }

                // for textarea
                $input_personal['Alamat'] = form_textarea('alamat', isset($account) ? $account->alamat : '', array('class' => 'form-control'));
            }

            $input_account = $this->form_input_account($account, true);

            if ($acc_id != 0) {
                $interaction = '<a href="'.base_url('admin/account/'.$group.'/remove/'.$acc_id).'" class="btn red white-text">Hapus</a>';
                $this->add_prop($interaction, 'interaction');
            }

            $this->add_prop($groups, 'groups');

            // populate two kind form
            $this->add_prop($input_account, 'input_account');
            $this->add_prop($input_personal, 'input_personal');
        } else {
            $this->add_message('Account ID Invalid', true, 3);
            redirect(base_url($this->get_site_root('account/'.$group)));
        }

        if (isset($_POST['submit'])) {
            if ($this->do_edit($group, $acc_id)) {
                $this->add_message('Perubahan data berhasil', true, 1);
                redirect('admin/account/'.$group, 'refresh');
            }
        }

        // publish section
        $this->compile_page($this->get_site_root('account_edit'));
        $this->publish();
    }

    public function do_edit($group, $acc_id)
    {
        $this->loader->library('form_validation');
        $this->loader->model('account');

        if ($this->form_validation->run('account_info')) {
            $field_account = array('identity', 'email', 'currpass');

            // get personal field
            $field_personal = array();
            $pair_func = 'get_'.$group.'_pair';
            foreach ($this->$pair_func() as $heading_pair => $field) {
                $field_personal[] = $field;
            }

            $this->form_validation->reset_validation();
            if ($this->form_validation->run($group.'_info')) {
            // is it adding or editing
                if ($acc_id == 0) {
                    if ($this->account_add($acc_id, $this->prep_input($field_account), $this->prep_input($field_personal), $group)) {
                        return true;
                    }
                } else {
                    if ($this->account_edit($acc_id, $this->prep_input($field_account), $this->prep_input($field_personal), $group)) {
                        return true;
                    }
                }
            } else {
                $this->add_message(validation_errors(), false, 2);
                return false;
            }
        } else {
            $this->add_message(validation_errors(), false, 2);
            return false;
        }
    }

    /**
     * Account add for guru and siswa
     *
     * @param  int $acc_id
     * @param  string[] $field_account
     * @param  string[] $field_personal
     * @param  string $table
     * @return boolean                 true if sucess, false if not
     */
    public function account_add($acc_id, $input_account, $input_personal, $table)
    {
        // unset currpass so it wont bother on add
        $input_account['password'] = $input_account['currpass'];
        unset($input_account['currpass']);

        // if siswa check nisn uniqueness
        if ($table == 'siswa') {
            $input_account['group'] = 2;
            if (empty($input_personal['lintas_minat1'])) {
                $input_personal['lintas_minat1'] = null;
            }
            if (empty($input_personal['lintas_minat2'])) {
                $input_personal['lintas_minat2'] = null;
            }
            if ( ! empty($input_personal['nis']) && ! $this->siswa_m->is_unique(array('nis' => $input_personal['nis']))) {
                $this->add_message('NIS tidak unique', false, 2);
                return false;
            }
        } elseif ($table == 'guru') {
            $input_account['group'] = 3;
        }

        if ( ! $this->ion_auth->username_check($input_account['identity'])) {
            $insert_id = $this->acc_m->my_insert($input_account);
        } else {
            $this->add_message('Username has been taken', false, 2);
            return false;
        }

        // update model per groups
        $input_personal['account_id'] = $insert_id;
        $model = $this->to_model($table);
        $this->$model->insert($input_personal);

        return true;
    }

    // -------------------------------------------------------------------------

    /**
     * Edit Account for siswa and guru
     *
     * @param  int $acc_id
     * @param  string[] $field_account
     * @param  string[] $field_personal
     * @param  string $table
     * @return boolean
     */
    public function account_edit($acc_id, $input_account, $input_personal, $table)
    {
        $input_account['username'] = $input_account['identity'];
        unset($input_account['identity']);

        // unset current password so it wont be bother on update
        $input_account['password'] = $input_account['currpass'];
        unset($input_account['currpass']);

        if (empty($input_account['password'])) {
            // if empty then dont udpate password
            unset($input_account['password']);
        } else {
            // hash new password
            $input_account['password'] = password_hash($input_account['password'], PASSWORD_DEFAULT);
        }

        if ($this->acc_m->is_unique(array('username' => $input_account['username']), $acc_id)) {
            // update account
            $this->acc_m->where('id', $acc_id)->update($input_account);
        } else {
            $this->add_message('Username tidak unique', false, 2);
            return false;
        }

        if ($table == 'siswa') {
            if (empty($input_personal['lintas_minat1'])) {
                $input_personal['lintas_minat1'] = null;
            }
            if (empty($input_personal['lintas_minat2'])) {
                $input_personal['lintas_minat2'] = null;
            }

            $nis = $this->siswa_m->where('account_id', $acc_id)->get()->nis;
            if ($nis != $input_personal['nis']) {
                // check for nis uniqueness
                if ( ! $this->siswa_m->is_unique(array('nis' => $input_personal['nis']))) {
                    $this->add_message('NIS tidak unique', false, 2);
                    return false;
                }
            }
        }

        // update per model
        $model = $this->to_model($table);
        $this->$model->where('account_id', $acc_id)->update($input_personal);

        return true;
    }

    // -------------------------------------------------------------------------

    /**
     * Siswa editing
     * @param  int $acc_id
     * @return void
     */
    public function siswa_edit($acc_id)
    {
        $this->set_title($this->title_add_or_edit($acc_id, 'Siswa'));
        $this->set_breadcrumbs('Data Account', 'Siswa', 'Edit');
        $this->edit_base('siswa', $acc_id);
    }

    // -------------------------------------------------------------------------

    /**
     * Guru editing
     * @param  int $acc_id
     * @return void
     */
    public function guru_edit($acc_id)
    {
        $this->set_title($this->title_add_or_edit($acc_id, 'Guru'));
        $this->set_breadcrumbs('Data Account', 'Guru', 'Edit');
        $this->edit_base('guru', $acc_id);
    }

    // -------------------------------------------------------------------------

    /**
     * Remove account
     *
     * @param  int $acc_id
     * @return void
     */
    public function remove($acc_id, $group)
    {
        $this->loader->model($group);
        $model = $this->to_model($group);
        $tmp_data = $this->$model->where('account_id', $acc_id)->get();
        $this->ion_auth->delete_user($acc_id);
        $this->add_message('User : '.$tmp_data->nama.' berhasil di hapus', true, 1);
        redirect('/admin/account/'.$group);
    }

    public function guru_remove($acc_id)
    {
        $this->remove($acc_id, 'guru');
    }

    public function siswa_remove($acc_id)
    {
        $this->remove($acc_id, 'siswa');
    }

    // -------------------------------------------------------------------------

    /**
     * to bulk add
     *
     * WARNING ! still buggy
     *
     * @return void
     */
}
