<?php
class Jadwal_pelajaran extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->loader->model('guru_matpel', 'matpel', 'guru', 'kelas', 'guru_matpel_jadwal', 'tahun_ajaran');
        $this->loader->library('table', 'filter_manager');
        $this->loader->helper('form');

        // fetch common items
        $all_hari = $this->get_hari();
        $ta_aktif = $this->get_ta_aktif();
        $filter = $this->init_filter();

        if (isset($filter['tahun_ajaran_id']) === false) {
            $filter['tahun_ajaran_id'] = $ta_aktif->id;
        }

        $all_guru_matpel = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas('order_by:kelas,asc')->with_guru('fields:nama|order_by:nama,asc')->where($filter)->get_all());

        $all_guru_matpel_id = array_column($all_guru_matpel, 'id');

        $jadwal = array();
        $all_jadwal = $this->guru_matpel_jadwal_m->get_all_jadwal($all_guru_matpel_id);
        foreach ($all_guru_matpel as $guru_matpel) {
            $tmp_jadwal = isset($all_jadwal[$guru_matpel->id]) ? $all_jadwal[$guru_matpel->id] : array();
            foreach ($tmp_jadwal as $tmp_jad) {
                $tmp_jad = (array)$tmp_jad;
                $tmp_jad['guru_matpel'] = $guru_matpel;
                $jadwal[$tmp_jad['hari']][] = $tmp_jad;
                $jadwal[$tmp_jad['hari']] = $this->array_sort($jadwal[$tmp_jad['hari']], 'begin_hrs');
            }
        }

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-striped">'));
        $this->table->set_heading(array('Hari', 'Jam', 'Nama Guru', 'Kelas','Mata Pelajaran'));
        foreach ($all_hari as $key_hari => $hari) {
            $this->table->add_row('<b>'.$hari.'</b>', '', '', '', '');
            if (empty($jadwal[$key_hari]) == false) {
                foreach ($jadwal[$key_hari] as $jdw) {
                    $row = array(
                        '',
                        substr($jdw['begin_hrs'], 0, 5).' - '.substr($jdw['end_hrs'], 0, 5),
                        $jdw['guru_matpel']->guru,
                        $jdw['guru_matpel']->kelas,
                        anchor('admin/kbm/guru_matpel/edit/'.$jdw['guru_matpel']->id, $jdw['guru_matpel']->matpel),
                        );
                    $this->table->add_row($row);
                }
            }
        }
        $table_jadwal = $this->table->generate();

        $filter = $this->filter_manager->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())->load('kelas', $this->kelas_m->order_by('kelas', 'ASC')->get_all())->get();

        // $interaction = anchor(base_url('admin/master/ta/add'), 'Tambah Tahun Akademik', array('class' => $this->get_html_prop('button-success-sm')));
        $site_url = base_url('admin/jadwal_pelajaran');
        $this->add_prop($site_url, 'site_url');
        // publish section
        $this->set_title('Jadwal Pelajaran', 'Daftar jadwal pelajaran');
        $this->set_breadcrumbs('Jadwal Pelajaran');
        $this->add_prop($filter, 'filter');
        $this->add_prop($table_jadwal, 'table');
        $this->append_var('<div class="alert blue white-text"><b>Info!</b> Klik pada mata pelajaran untuk melihat rincian </div>', 'prefix');
        $this->compile_page('public/card-table');
        $this->publish();
    }
}
