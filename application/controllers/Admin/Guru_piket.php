<?php
class Guru_piket extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('guru_piket_jadwal');
    }

    public function index()
    {
        $this->loader->library('table', 'filter_manager');
        $this->loader->model('guru', 'tahun_ajaran');
        $this->loader->helper('form');

        $filter = $this->init_filter();
        $ta_aktif = $this->get_ta_aktif();

        $template = array('table_open' => '<table class="table table-condensed table-bordered table-sort" data-leftcolumns="1" width=\'100%\'>');
        $this->table->set_template($template);

        $heading = array('No');
        $heading = array_merge($heading, $this->get_hari());
        $this->table->set_heading($heading);

        if (isset($filter['tahun_ajaran_id']) === false) {
            $filter['tahun_ajaran_id'] = $ta_aktif->id;
        }

        $all_guru_piket = $this->gpiket_jdw_m->get_this_periode($filter);

        $guru_piket_jadwal = array();
        foreach ($all_guru_piket as $guru_piket) {
            $tmp = $this->guru_m->where('account_id', $guru_piket->account_id)->get();
            $tmp->gpiket_id = $guru_piket->id;
            $guru_piket_jadwal[$guru_piket->hari][] = $tmp;
        }

        $length = 0;
        for ($hari = 1; $hari <= 7; $hari++) {
            if (isset($guru_piket_jadwal[$hari])) {
                if ($length < count($guru_piket_jadwal[$hari])) {
                    $length = count($guru_piket_jadwal[$hari]);
                }
            }
        }

        $this->counter_init();
        for ($row_no = 0; $row_no < $length; $row_no++) {
            $row = array($this->counter_inc());
            for ($hari=1; $hari <= 7; $hari++) {
                if (isset($guru_piket_jadwal[$hari][$row_no])) {
                    $row[] = $guru_piket_jadwal[$hari][$row_no]->nama.'<a href="'.base_url('admin/master/guru_piket/remove/'.$guru_piket_jadwal[$hari][$row_no]->gpiket_id).'"><span class="pull-right red-text"><i class="fa fa-remove"></span></a>';
                } else {
                    $row[] = ' ';
                }
            }
            $this->table->add_row($row);
        }

        $table_guru_piket = $this->table->generate();

        $this->set_title('Jadwal Guru Piket', 'Daftar guru piket untuk tahun ajaran tertentu');
        $this->set_breadcrumbs('Data Master', 'Guru Piket');

        $filter = $this->filter_manager->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())->get();

        $interaction = anchor('admin/master/guru_piket/add', 'Tambah Jadwal Guru Piket', array('class' => $this->get_html_prop('button-success-sm')));
        $site_url = base_url('admin/master/guru_piket');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($filter, 'filter');
        $this->add_prop($table_guru_piket, 'table');
        $this->add_prop($interaction, 'interaction');
        $this->append_page('public/card-table');
        $this->publish();
    }

    public function edit()
    {
        $this->loader->helper('form');
        $this->loader->model('guru');
        if (isset($_POST['submit'])) {
            $ta_aktif = $this->get_ta_aktif();
            $post = $this->prep_input('hari', 'account_id');
            $post['tahun_ajaran_id'] = $ta_aktif->id;
            if ($this->gpiket_jdw_m->is_unique($post)) {
                $this->gpiket_jdw_m->insert($post);
                // $this->abs_kbm_m->insert();
                $this->add_message('Data berhasil ditambahkan', true, 1);
                // redirect('admin/master/guru_piket', 'refresh');
            } else {
                $this->add_message('Data tidak unique', false, 2);
            }
        }

        $list_all_hari = $this->get_hari();
        $input['Hari'] = form_dropdown('hari', $list_all_hari, 'default', array('class' => 'form-control'));

        $list_all_guru = $this->guru_m->get_all();
        $guru_opt = array();
        foreach ($list_all_guru as $guru) {
            $guru_opt[$guru->account_id] = $guru->nama;
        }
        $input['Guru'] = form_dropdown('account_id', $guru_opt, 'default', array('class' => 'form-control'));
        $this->add_prop($input, 'input');
        $this->set_title('Tambah Guru Piket');
        $this->set_breadcrumbs('Data Master', 'Guru Piket', 'Tambah');
        $this->append_page('public/panel-edit');
        $this->publish();
    }

    public function remove($gpiket_id)
    {
        $this->gpiket_jdw_m->delete($gpiket_id);
        redirect('admin/master/guru_piket');
    }
}
