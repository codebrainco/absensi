<?php
class Berita_acara extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('berita_acara');
    }

    public function index($guru_matpel_id)
    {

        $this->loader->model('guru_matpel', 'disabled_date', 'tahun_ajaran', 'guru_matpel_jadwal', 'absensi_kbm');
        $this->loader->library('table');

        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        $tanggal_opt = array();
        $tmp_berita_acara = $this->ba_m->get_all_ba($guru_matpel_id);
        $berita_acara = $this->generate_ba($tmp_berita_acara);

        $tahun = $this->slice_tanggal($this->ta_m->get($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;

        $this->table->set_template(array('table_open' => '<table data-leftcolumns="1" class="table table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading('No', 'Tanggal', 'Materi', 'Tugas Siswa', 'Aksi');
        $this->counter_init();
        // echo urlencode(base64_encode('17.2016-07-21.1800-2200'));
        // echo base64_decode(urldecode('MTcuMjAxNi0wNy0yMS4xODowMC0yMjowMA%3D%3D'));
        $tmp_tanggal = $this->generate_tanggal($guru_matpel);
        foreach ($tmp_tanggal['date'] as $bulan => $list_tanggal) {
            foreach ($list_tanggal as $tanggal => $list_jadwal) {
                foreach ($list_jadwal as $key => $jadwal) {
                    $tmp_ba = isset($berita_acara[$bulan][$tanggal][$jadwal]) ? $berita_acara[$bulan][$tanggal][$jadwal] : '';
                    $add_or_edit = $tmp_ba ? $tmp_ba['id'] : 'add';
                    $encoded = $this->time_encode($tahun, $bulan, $tanggal, $jadwal);
                    $action = anchor(base_url('admin/kbm/ba/edit/'.$guru_matpel_id.'/'.$encoded), 'Edit', array('class' => $this->get_html_prop('button-default-sm')));
                    $row = array(
                        $this->counter_inc(),
                        $tanggal.' '.mdate('%M', strtotime($bulan)).' '.$tahun.' @ '.$jadwal,
                        $tmp_ba ? $tmp_ba['materi'] : '',
                        $tmp_ba ? $tmp_ba['tugas_siswa'] : '',
                        $action
                        );
                    $this->table->add_row($row);
                }
            }
        }
        $table = $this->table->generate();

        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->add_prop($this->session->referral_url, 'referral_url');
        $this->add_prop($table, 'table');
        $this->add_prop(array('section_column' => 6), 'sct_conf');
        $this->append_page('public/card-guru-badge');
        $this->set_title('Berita Acara', 'Daftar berita cara untuk guru_matpel tertentu');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel', 'Berita Acara');
        $this->add_prop('full', 'section_column');
        $this->add_prop(array('section_column' => 12), 'sct_conf');
        $this->append_page('public/card-table');
        $this->publish();
    }

    public function edit($guru_matpel_id, $time_encoded)
    {
        if ($this->is_valid_time($time_encoded)) {
            $time_data = $this->time_decode($time_encoded);
            $data = $this->ba_m->get_ba($guru_matpel_id, $time_data);

            if (isset($_POST['submit'])) {
                $post = $this->prep_input('materi', 'tugas_siswa');
                $post['guru_matpel_id'] = $guru_matpel_id;
                $post['tanggal'] = $time_data[0];
                $post['hours'] = $time_data[1];
                if ( ! is_null($data)) {
                    $this->ba_m->update_ba($post);
                } else {
                    $this->ba_m->insert($post);
                }
                $this->add_message('Data berhasil di update', true, 1);
                redirect('admin/kbm/ba/edit/'.$guru_matpel_id, 'refresh');
            }

            if (is_null($data)) {
                $data = new stdClass();
                $data->materi = '';
                $data->tugas_siswa = '';
            }
            $data->tanggal = $time_data[0];
            $data->hours = $time_data[1];
            $field = array(
                'Tanggal' => 'tanggal|date|disabled[]',
                'Jam' => 'hours|text|disabled[]',
                'Materi' => 'materi|text',
                'Tugas Siswa' => 'tugas_siswa|text',
                );

            $input = $this->form_generator($field, $data, 'form-control');

            // publish section
            $this->set_title('Edit Berita Acara', 'Melakukan perubahan pada data berita acara');
            $this->set_breadcrumbs('Data KBM', 'Guru-Mapel', 'Berita Acara', 'Edit');
            $this->add_prop($input, 'input');
            $this->compile_page('public/panel-edit');
            $this->publish();
        } else {
            $this->add_message('Invalid Token !', true, 2);
            redirect('admin/kbm/ba/edit/'.$guru_matpel_id);
        }
    }

    public function time_decode($time_encoded)
    {
        $tmp = base64_decode(urldecode($time_encoded));
        $return_data = explode('.', $tmp);
        $return_data[1] = $this->nice_hours($return_data[1]);
        return $return_data;
    }

    public function time_encode($tahun, $bulan, $tanggal, $jadwal)
    {
        return urlencode(base64_encode($tahun.'-'.mdate('%m', strtotime($bulan)).'-'.$tanggal.'.'.str_replace(':', '', $jadwal)));
    }

    public function is_valid_time($time_encoded)
    {
        $tmp_data = base64_decode(urldecode($time_encoded));

        $tmp = explode('.', $tmp_data);
        if ( ! isset($tmp[0]) || ! isset($tmp[1])) {
            return false;
        }

        $tmp_tanggal = explode('-', $tmp[0]);

        if ( ! isset($tmp_tanggal[0]) || ! isset($tmp_tanggal[1]) || ! isset($tmp_tanggal[1])) {
            return false;
        }

        $tahun = $tmp_tanggal[0];
        $bulan = $tmp_tanggal[1];
        $tanggal = $tmp_tanggal[2];

        if ( ! is_numeric($tahun) || ! is_numeric($bulan) || ! is_numeric($tanggal)) {
            return false;
        }

        if ( ! checkdate($bulan, $tanggal, $tahun)) {
            return false;
        }

        $tmp_hours = explode('-', $tmp[1]);
        if ( ! isset($tmp_hours[0]) || ! isset($tmp_hours[1])) {
            return false;
        }

        $begin_hrs = $tmp_hours[0];
        $end_hrs = $tmp_hours[1];

        if ( ! is_numeric($begin_hrs) || ! is_numeric($end_hrs)) {
            return false;
        }

        return true;
    }
}
