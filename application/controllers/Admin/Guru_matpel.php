<?php
class Guru_matpel extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('guru_matpel', 'guru_matpel_jadwal', 'kelas');
    }

    /**
     * View untuk menu KBM => Guru-Matpel
     * @return void
     */
    public function index()
    {
        // basic load
        $this->loader->model('tahun_ajaran');
        $this->loader->library('table', 'filter_manager');
        $this->loader->helper('form');

        // filter get
        $ta_aktif = $this->get_ta_aktif();
        $filter = $this->init_filter();
        if (isset($filter['tahun_ajaran_id']) === false) {
            $filter['tahun_ajaran_id'] = $ta_aktif->id;
        }

        // populating
        $all_guru_matpel = $this->get_guru_matpel(null, $filter);

        // table
        $heading_pair = array(
            'ID'            => 'id',
            'Nama Guru'     => 'guru',
            'Mapel di Ampu' => 'matpel',
            'Kelas'         => 'kelas',
            'Jadwal'        => 'jadwal',
            'TA'            => 'ta',
            'Semester'      => 'semester',
            );
        $action = '';
        $action .= anchor(base_url("admin/kbm/guru_matpel/edit/(:guru_matpel_id)"), "Lihat", array('class' => 'btn btn-space btn-sm btn-default'));
        $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action);

        // filter
        $filter = $this->filter_manager
            ->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())
            ->load('kelas', $this->kelas_m->get_all())
            ->load('guru', $this->guru_m->get_all())
            ->get();

        $interaction = anchor(base_url('admin/kbm/guru_matpel/add'), 'Tambah Data', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $site_url = base_url('admin/kbm/guru_matpel');

        // publish
        $this->set_title('Guru Matpel', 'Daftar hubungan antara guru dan mata pelajaran yang di ampu');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel');
        $this->add_prop($filter, 'filter');
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_matpel, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function lm()
    {
        $this->loader->model('tahun_ajaran');
        $this->loader->library('table', 'filter_manager');
        $this->loader->helper('form');

        // build table
        $heading_pair = array(
            'ID'            => 'id',
            'Nama Guru'     => 'guru',
            'Mapel di Ampu' => 'matpel',
            'Kelas'         => 'kelas',
            'Jadwal'        => 'jadwal',
            'TA'            => 'ta',
            'Semester'      => 'semester',
            );
        $action = '';
        $action .= anchor(base_url("admin/kbm/guru_matpel_lm/edit/(:guru_matpel_id)"), "Lihat", array('class' => 'btn btn-space btn-sm btn-default'));

        $ta_aktif = $this->get_ta_aktif();
        $filter = $this->init_filter();

        if (isset($filter['tahun_ajaran_id']) === false) {
            $filter['tahun_ajaran_id'] = $ta_aktif->id;
        }

        $all_guru_matpel = $this->get_guru_matpel_lm(null, $filter);
        $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action, $filter, null, $lm = true);

        $filter = $this->filter_manager->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())->get();

        $interaction = anchor(base_url('admin/kbm/guru_matpel_lm/add'), 'Tambah Data', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $site_url = base_url('admin/kbm/guru_matpel_lm');

        // publish section
        $this->set_title('Guru Matpel', 'Daftar hubungan antara guru dan mata pelajaran yang di ampu');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel');
        $this->add_prop($filter, 'filter');
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_matpel, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function do_edit($guru_matpel_id)
    {
        // fetch input and common item
        $input = $this->prep_input('account_id', 'kelas_id', 'matpel_id', 'tahun_ajaran_id', 'total_pertemuan');
        $input['jadwal_type'] = 3;

        // check uniqueness
        $checked_pair = array(
            'account_id' => $input['account_id'],
            'kelas_id' => $input['kelas_id'],
            'matpel_id' => $input['matpel_id'],
            'tahun_ajaran_id' => $input['tahun_ajaran_id'],
            );

        if ($this->guru_matpel_m->is_unique($checked_pair, $guru_matpel_id)) {
            if ($guru_matpel_id == 0) {
                $guru_matpel_id = $this->guru_matpel_m->my_insert($input);
            } else {
                $this->guru_matpel_m->where('id', $guru_matpel_id)->update($input);
            }
            $this->add_message('Data berhasil diubah <b><a href="'.base_url('admin/kbm/jadwal/edit/'.$guru_matpel_id).'">Klik untuk menambahkan jadwal</a></b>', true, 1);
            return true;
        } else {
            $this->add_message('Duplicate Found', false, 3);
            return false;
        }
    }

    public function edit_base($guru_matpel_id, $all_kelas, $lm = false)
    {
        $this->loader->model('guru', 'matpel', 'tahun_ajaran');
        $this->loader->helper('form');

        $ta_aktif = $this->get_ta_aktif();

        if (isset($_POST['submit'])) {
            if ($this->do_edit($guru_matpel_id)) {
                redirect('admin/kbm/guru_matpel'.($lm ? '_lm' : ''));
            }
        }

        // populate guru
        $all_guru = $this->guru_m->order_by('nama')->get_all();
        foreach ($all_guru as $key => $guru) {
            $guru_options[$guru->account_id] = $guru->nama;
        }

        // populate kelas
        $kelas_options = array();
        foreach ($all_kelas as $kelas) {
            $kelas_options[$kelas->id] = $kelas->kelas;
        }

        // populate matpel
        $matpel_options = $this->matpel_m->order_by('matpel')->as_dropdown('matpel')->get_all();

        // populate tahun ajaran
        $all_ta = $this->ta_m->get_all_ta();
        $ta_options = array();
        foreach ($all_ta as $ta) {
            $ta_options[$ta->id] = $ta->tahun_ajaran.' - '.$this->get_semester($ta->semester);
        }

        if ($guru_matpel_id != 0) {
            $interaction = '<a href="'.base_url('admin/kbm/guru_matpel/remove/'.$guru_matpel_id).'" class="btn red white-text">Hapus</a>';

            $derived_edit = '';
            $derived_edit .= anchor(base_url("admin/kbm/jadwal/edit/".$guru_matpel_id.""), "Jadwal", array('class' => $this->get_html_prop('button-primary')));
            $derived_edit .= anchor(base_url("admin/kbm/ba/edit/".$guru_matpel_id."#table"), "Berita Acara", array('class' => $this->get_html_prop('button-primary')));
            $derived_edit .= anchor(base_url("admin/kbm/absensi".($lm ? "_lm" : '')."/edit/".$guru_matpel_id."#table"), "Absensi", array('class' => $this->get_html_prop('button-primary')));

            $this->add_prop($interaction, 'interaction');
            $this->add_prop($derived_edit, 'derived_edit');
        }

        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        if ( ! empty($guru_matpel)) {
            $guru = $guru_matpel->account_id;
            $kelas = $guru_matpel->kelas_id;
            $matpel = $guru_matpel->matpel_id;
            $tahun_ajaran = $guru_matpel->tahun_ajaran_id;
            $total_pertemuan = $guru_matpel->total_pertemuan;
        }

        $input = array();
        $input['Guru'] = form_dropdown('account_id', $guru_options, isset($guru) ? $guru : '', array('class' => 'form-control'));
        $input['Kelas'] = form_dropdown('kelas_id', $kelas_options, isset($kelas) ? $kelas : '', array('class' => 'form-control'));
        $input['Mata Pelajaran'] = form_dropdown('matpel_id', $matpel_options, isset($matpel) ? $matpel : '', array('class' => 'form-control'));
        $input['Tahun Akademik'] = form_dropdown('tahun_ajaran_id', $ta_options, isset($tahun_ajaran) ? $tahun_ajaran : $ta_aktif->id, array('class' => 'form-control'));
        $input['Total Pertemuan'] = form_input('total_pertemuan', isset($total_pertemuan) ? $total_pertemuan : '', array('class' => 'form-control'));

        // publish section
        $this->add_prop($input, 'input');
        $this->compile_page($this->get_site_root('guru_matpel_edit'));
        $this->publish();
    }

    /**
     * View for adding guru_matpel
     *
     * @param  int $guru_matpel_id
     * @return void
     */
    public function edit($guru_matpel_id)
    {
        $this->set_title($this->title_add_or_edit($guru_matpel_id, ' Guru Matpel'), 'Edit hubungan antara guru dan mata pelajaran yang di ampu');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel', 'Edit');
        $all_kelas = $this->kelas_m->get_all_kelas();
        $this->edit_base($guru_matpel_id, $all_kelas, $lm = false);
        $this->session->referral_url = base_url('admin/kbm/guru_matpel/edit/'.$guru_matpel_id);
        $this->session->mark_as_flash('referral_url');
    }

    /**
     * View for adding guru_matpel
     *
     * @param  int $guru_matpel_id
     * @return void
     */
    public function lm_edit($guru_matpel_id)
    {
        $this->set_title($this->title_add_or_edit($guru_matpel_id, ' Guru Matpel Lintas Minat'), 'Tambah hubungan antara guru dan mata pelajaran yang di ampu');
        $this->set_breadcrumbs('Data KBM', 'Guru Matpel', 'Edit');
        $all_kelas_lm = $this->kelas_m->get_all_kelas_lm();
        $this->edit_base($guru_matpel_id, $all_kelas_lm, $lm = true);
        $this->session->referral_url = base_url('admin/kbm/guru_matpel_lm/edit/'.$guru_matpel_id);
        $this->session->mark_as_flash('referral_url');
    }

    /**
     * Remove guru_matpel and their jadwal
     *
     * @param  int $guru_matpel_id
     * @return void
     */
    public function remove($guru_matpel_id, $lm = false)
    {
        $this->add_message('Data berhasil dihapus', true, 1);
        $this->guru_matpel_m->delete($guru_matpel_id);
        $this->guru_matpel_jadwal_m->where('guru_matpel_id', $guru_matpel_id)->delete();
        redirect('admin/kbm/guru_matpel'.($lm ? '_lm' : ''));
    }
}
