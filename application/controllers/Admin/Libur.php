<?php
class Libur extends Admin_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->loader->model('disabled_date');
    }

    public function index()
    {
        $this->loader->model('tahun_ajaran');
        $this->loader->library('table', 'filter_manager');
        $this->loader->helper('form');

        // list all disabled date
        $ta = $this->get_ta_aktif();
        $filter = $this->init_filter();

        $get = $this->input->get();
        if (empty($get) !== false) {
            $filter = array('tahun_ajaran_id' => $ta->id);
        }

        $all_libur = $this->ddate_m->where($filter)->order_by('tanggal')->get_all() ?: array();

        // build table
        $this->table->set_template(array('table_open' => '<table class="table table-bordered table-striped table-sort" width=\'100%\'>'));
        $this->table->set_heading(array('No', 'Tanggal', 'Keterangan', 'Aksi'));
        $this->counter_init();
        foreach ($all_libur as $hari) {
            $action = '';
            // $action .= anchor(base_url('admin/master/libur/edit/'.$hari->id), "Edit", array('class' => "btn btn-default btn-space btn-sm"));
            $action .= anchor(base_url('admin/master/libur/remove/'.$hari->id), "Hapus", array('class' => "btn btn-default red white-text btn-space btn-sm"));
            $date = new DateTime($hari->tanggal);
            $row = array(
                $this->counter_inc(),
                $date->format('d F Y'),
                $hari->keterangan,
                array('data' => $action, 'class' => 'text-center')
                );
            $this->table->add_row($row);
        }
        $table_libur = $this->table->generate();

        $filter = $this->filter_manager->load('tahun_ajaran', $this->ta_m->get_all_ta_smt())->get();

        // publish section
        $site_url = base_url('admin/master/libur');
        $interaction = anchor(base_url('admin/master/libur/add'), 'Tambah Hari Libur', array('class' => 'pull-right '.$this->get_html_prop('button-success-sm')));
        $this->set_title('Hari Libur', 'Daftar hari libur untuk tahun ajaran tertentu');
        $this->set_breadcrumbs('Data Master', 'Hari Libur');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($interaction, 'interaction');
        $this->add_prop($table_libur, 'table');
        $this->add_prop($filter, 'filter');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // -------------------------------------------------------------------------

    /**
     * Remove matpel from matpel_table
     *
     * @param  int $matpel_id
     * @return void
     */
    public function remove($ddate_id)
    {
        $tanggal = $this->ddate_m->get($ddate_id)->tanggal;
        $this->add_message('Tanggal : '.$tanggal.' berhasil di hapus', true, 1);
        $this->ddate_m->delete($ddate_id);
        redirect('admin/master/libur');
    }

    /**
     * Something went wrong here
     * Jadi ketika nambah hari libur kan entah bagus apa kagak kalo nge hapus absensi tanggal seitu
     */
    public function do_edit($ddate_id)
    {
        $this->loader->library('form_validation');
        $this->loader->model('absensi_kbm');

        $post = $this->prep_input('tanggal', 'keterangan', 'tahun_ajaran_id');

        if ($this->form_validation->run('hari_libur') == true) {
            if ($ddate_id != 0) {
                $this->ddate_m->update($post, $ddate_id);
                $this->abs_kbm_m->where('tanggal', $post['tanggal'])->delete();
            } else {
                $this->ddate_m->insert($post);
                $this->abs_kbm_m->where('tanggal', $post['tanggal'])->delete();
            }
            $this->add_message('Data berhasil di update', true, 1);
            return true;
        } else {
            $this->add_message(validation_errors(), false, 2);
            return false;
        }
    }

    /**
     * View for editting hari libur
     *
     * @param  int $ddate_id disabled date id
     * @return void
     */
    public function edit($ddate_id)
    {
        $this->loader->helper('form');
        $this->loader->model('tahun_ajaran');

        if (isset($_POST['submit'])) {
            if ($this->do_edit($ddate_id)) {
                redirect('admin/master/libur');
            }
        }

        // build form
        $tahun = $this->ddate_m->get($ddate_id);
        $field = array(
            'Tanggal' => 'tanggal|date',
            'Keterangan' => 'keterangan|text',
            );

        $input = $this->form_generator($field, $tahun, 'form-control');

        $all_ta = $this->ta_m->get_all_ta();
        $ta_aktif = $this->get_ta_aktif();

        $ta_options = array();
        foreach ($all_ta as $ta) {
            $ta_options[$ta->id] = $ta->tahun_ajaran.' - '.$this->get_semester($ta->semester);
        }

        $input['Tahun Akademik'] = form_dropdown('tahun_ajaran_id', $ta_options, $ta_aktif->id, array('class' => 'form-control',));

        // publish section
        $this->set_title($this->title_add_or_edit($ddate_id, 'Hari Libur'));
        $this->set_breadcrumbs('Data Master', 'Hari Libur', 'Edit');
        $this->add_prop($input, 'input');
        $this->compile_page('public/panel-edit');
        $this->publish();
    }
}
