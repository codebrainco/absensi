<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Public_Controller extends Manage_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function bulk_update_absen()
    {
        $this->load->model('absensi_kbm_model', 'abs_kbm_m');
        $regex = '/\[([^\]]*)\]/';
        $bulan = $this->get_bulan_eng();
        $input = $this->input->post('data');
        foreach ($input as $value)
        {
            if (strpos($value['guru_matpel_id'], '#') !== false) {
                $guru_matpel_id = substr($value['guru_matpel_id'], 0, strpos($value['guru_matpel_id'], '#'));
            } else {
                $guru_matpel_id = $value['guru_matpel_id'];
            }
            preg_match_all($regex, $value['name'], $matches);
            $update_data = array();
            $update_data[] = array(
                'tanggal' => $value['tahun'].'-'.str_pad(array_search($matches[1][0], $bulan), 2, '0', STR_PAD_LEFT).'-'.$matches[1][1],
                'account_id' => $matches[1][3],
                'hours' => $matches[1][2],
                'guru_matpel_id' => $guru_matpel_id,
                'hadir' => $value['status'],
                );
            $checked_pair = $update_data[0];
            unset($checked_pair['hadir']);

            if ($this->abs_kbm_m->is_unique($checked_pair)) {
                $this->abs_kbm_m->insert_single($update_data);
            } else {
                $this->abs_kbm_m->update_single($update_data);
            }
        }
    }
}
