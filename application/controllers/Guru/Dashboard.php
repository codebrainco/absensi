<?php
class Dashboard extends Guru_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->set_title('Dashboard');
        $this->set_breadcrumbs('Dashboard');
        $this->compile_page($this->get_site_root('dashboard'));
        $this->publish();
    }
}
