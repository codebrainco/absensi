<?php
class Guru_piket extends Guru_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * View for Guru Piket
     * @return void
     */
    public function index()
    {
        // basic load
        $this->loader->model('guru_matpel', 'guru_matpel_jadwal', 'absensi_kbm');
        $this->loader->library('table');

        // populating static data
        $today = new DateTime();
        $today_hari = $today->format('N');
        $list_hari = $this->get_hari();
        $ta_aktif = $this->get_ta_aktif();
        $site_url = base_url('guru/manage/piket');

        // populating guru matpel
        $all_guru_matpel = $this->guru_matpel_filter((array)$this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->with_jadwal("where:`hari`='$today_hari'")->order_by('tahun_ajaran_id')->where('tahun_ajaran_id', $this->get_ta_aktif()->id)->get_all());
        $all_guru_matpel_id = array_column($all_guru_matpel, 'id');
        $all_account_id = array_column($all_guru_matpel, 'account_id');

        // build array jadwal per hari
        foreach ($all_guru_matpel as $guru_matpel) {
            foreach ($guru_matpel->jadwal as $tmp_jad) {
                unset($guru_matpel->jadwal);
                $tmp_jad->guru_matpel = $guru_matpel;
                $jadwal[$tmp_jad->hari][] = $tmp_jad;
                $jadwal[$tmp_jad->hari] = $this->array_sort($jadwal[$tmp_jad->hari], 'begin_hrs');
            }
        }

        // populating today's absen
        $tmp_status_absen = $this->abs_kbm_m
            ->where('tanggal', $today->format('Y-m-d'))
            ->where('account_id', $all_account_id)
            ->get_all();
        foreach ($tmp_status_absen as $status_absen) {
            $all_status_absen[$status_absen->guru_matpel_id] = $status_absen;
        }

        // table
        $this->table->set_template(array('table_open' => '<table class="table table-striped table-sort" width="100%" data-pagelength="200">'));
        $this->table->set_heading(array('Jam', 'Nama Guru', 'Kelas', 'Mata Pelajaran', 'Status', 'Aksi'));
        foreach ($jadwal[$today->format('N')] as $jdw) {
            $action = '';
            $action .= anchor(base_url("guru/manage/piket/".$jdw->guru_matpel->id), "Berita Acara", array('class' => $this->get_html_prop('button-default-sm')));
            $action .= anchor(base_url("guru/manage/piket/".$jdw->guru_matpel->id."#table"), "Absensi", array('class' => $this->get_html_prop('button-default-sm')));
            $begin_hrs = new DateTime($jdw->begin_hrs);
            $end_hrs = new DateTime($jdw->end_hrs);

            // if had filled guru matpel
            if (isset($all_status_absen[$jdw->guru_matpel->id])) {
                $status = '<span class="green-text"><i class="fa fa-check-circle-o fa-2x"></span>';
            } else {
                $status = '<span class="red-text"><i class="fa fa-times fa-2x"></span>';
            }

            // if jadwal is right now
            if ($today >= $begin_hrs && $today <= $end_hrs) {
                $row = array(
                    array('data' => substr($jdw->begin_hrs, 0, 5).' - '.substr($jdw->end_hrs, 0, 5), 'class' => 'info'),
                    array('data' => $jdw->guru_matpel->guru, 'class' => 'info'),
                    array('data' => $jdw->guru_matpel->kelas, 'class' => 'info'),
                    array('data' => $jdw->guru_matpel->matpel, 'class' => 'info'),
                    array('data' => $status, 'class' => 'text-center info'),
                    array('data' => $action, 'class' => 'text-center info'),
                    );
            } else {
                $row = array(
                    substr($jdw->begin_hrs, 0, 5).' - '.substr($jdw->end_hrs, 0, 5),
                    $jdw->guru_matpel->guru,
                    $jdw->guru_matpel->kelas,
                    $jdw->guru_matpel->matpel,
                    array('data' => $status, 'class' => 'text-center'),
                    array('data' => $action, 'class' => 'text-center'),
                    );
            }
            $this->table->add_row($row);
        }
        $table_jadwal = $this->table->generate();

        // $interaction = anchor(base_url('admin/master/ta/add'), 'Tambah Tahun Akademik', array('class' => $this->get_html_prop('button-success-sm')));

        // publish
        $this->set_title('Jadwal Pelajaran Hari '.$this->get_hari($today->format('N')), 'Daftar jadwal pelajaran hari '.$this->get_hari($today->format('N')));
        $this->set_breadcrumbs('Guru Piket');
        $this->add_prop($site_url, 'site_url');
        $this->add_prop($table_jadwal, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    public function edit($guru_matpel_id, $lm = false)
    {
        // basic load
        $this->loader->model('kelas', 'guru_matpel', 'siswa', 'absensi_kbm', 'disabled_date', 'berita_acara');
        $today = new DateTime();

        // populating
        $guru_matpel = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas('order_by:kelas,asc')->with_guru('fields:nama|order_by:nama,asc')->order_by('tahun_ajaran_id')->get($guru_matpel_id));
        $gm_jadwal = $this->get_jadwal($guru_matpel_id, $today->format('N'));
        $ta_aktif = $this->get_ta_aktif();
        $available_angkatan = $this->get_allowed_angkatan();

        // validating
        if ($gm_jadwal === null) {
            show_error('Invalid Guru Matpel ID');
        }

        // all siswa from available angkatan, and kelas and lintas minat
        if ($this->kelas_m->is_lm($guru_matpel->kelas_id)) {
            $all_siswa = $this->siswa_m->get_siswa_lm($guru_matpel->kelas_id, $available_angkatan);
        } else {
            $all_siswa = $this->siswa_m->with_account()->where('angkatan', $available_angkatan)->where('kelas_id', $guru_matpel->kelas_id)->get_all() ?: array();
        }

        // prepare today, hari, bulan, bulan no and tanggal
        $hari = mdate("%D", now('Asia/Jakarta'));
        $bulan = mdate("%F", now('Asia/Jakarta'));
        $bulan_no = mdate("%m", now('Asia/Jakarta'));
        $tanggal = mdate("%d", now('Asia/Jakarta'));

        $siswa_id_arr = array();
        foreach ($all_siswa as $siswa) {
            $siswa_id_arr[] = $siswa->account_id;
        }

        // build table absensi
        $tmp_abs = array();
        $tmp = $this->abs_kbm_m->get_absensi_all($guru_matpel->tahun_ajaran, $bulan_no.'-'.$tanggal, $guru_matpel_id, $siswa_id_arr);

        $status_dropdown = $this->generate_absensi($tmp, true);
        $jadwal = substr($gm_jadwal['begin_hrs'], 0, 5).'-'.substr($gm_jadwal['end_hrs'], 0, 5);

        // set all dates, all day to be just one array, today
        $all_dates['dates'] = array(
            $bulan => array(
                $tanggal => array(
                    substr($gm_jadwal['begin_hrs'], 0, 5).'-'.substr($gm_jadwal['end_hrs'], 0, 5),
                )
            )
        );

        $all_dates['day'] = array(
            $bulan => array(
                $tanggal => $hari
            )
        );

        // build an option none
        $status_hadir = $this->get_status_hadir();
        $option_none = '';
        foreach ($status_hadir as $key => $hadir) {
            $option_none .= '<option value="'.($key).'">'.$hadir.'</option>';
        }

        // another dependencies for output
        $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;
        $form_tahun = form_hidden('tahun', $tahun);

        $time_data = array(
            $tahun.'-'.mdate('%m', strtotime($bulan)).'-'.$tanggal,
            $jadwal
            );


        $data = $this->ba_m->get_ba($guru_matpel_id, $time_data);
        if (isset($_POST['submit'])) {
            $tmp_gmap = $this->guru_matpel_m->get($guru_matpel_id);
            $post = $this->prep_input('materi', 'tugas_siswa');
            $post['guru_matpel_id'] = $guru_matpel_id;
            $post['tanggal'] = $time_data[0];
            $post['hours'] = $time_data[1];
            if ( ! is_null($data)) {
                $this->ba_m->update_ba($post);
            } else {
                $this->ba_m->insert($post);
            }
            $update_data = array();
            $update_data[] = array(
                'account_id' => $tmp_gmap->account_id,
                'guru_matpel_id' => $guru_matpel_id,
                'tanggal' => $time_data[0],
                'hours' => $time_data[1],
                'hadir' => 1,
                );
            $checked_pair = $update_data[0];
            unset($checked_pair['hadir']);

            if ($this->abs_kbm_m->is_unique($checked_pair)) {
                $this->abs_kbm_m->insert_single($update_data);
            } else {
                $this->abs_kbm_m->update_single($update_data);
            }
            $this->add_message('Berita acara berhasil di update', false, 1);
            $data = $this->ba_m->get_ba($guru_matpel_id, $time_data);
        }

        if (is_null($data)) {
            $data = new stdClass();
            $data->materi = '';
            $data->tugas_siswa = '';
        }
        $data->tanggal = $time_data[0];
        $data->hours = $time_data[1];

        $field = array(
            'Tanggal' => 'tanggal|date|disabled[]',
            'Jam' => 'hours|text|disabled[]',
            'Materi' => 'materi|text',
            'Tugas Siswa' => 'tugas_siswa|text'
            );

        $input = $this->form_generator($field, $data, 'form-control');

        // publish section
        $this->set_title('Absensi');
        $this->add_prop($form_tahun, 'form_tahun');
        $this->add_prop($guru_matpel, 'guru_matpel');

        $this->add_prop($input, 'input');
        $this->add_prop($status_dropdown, 'status_dropdown');
        $this->add_prop($option_none, 'option_none');
        $this->add_prop($all_siswa, 'all_siswa');
        $this->add_prop($all_dates['dates'], 'all_dates');
        $this->add_prop($all_dates['day'], 'all_day');
        $this->append_page($this->get_site_root('manage_absen'));
        $this->publish();
    }
}
