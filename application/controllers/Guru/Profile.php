<?php
class Profile extends Guru_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $guru = $this->guru_m->get_one();
        $guru_pair = $this->get_guru_pair();

        if (isset($_POST['submit']) === true) {
            if ($this->do_edit($this->acc_id)) {
                redirect('guru/profile','refresh');
            }
        }

        $guru_field = array();
        foreach ($guru_pair as $heading_pair => $field) {
            $guru_field[$heading_pair] = $field.'|text';
        }
        $guru_field['Tanggal Lahir'] = $field.'|date';
        $input_personal = $this->form_generator($guru_field, $guru, 'form-control');

        $input_account = $this->form_input_account($guru);

        if ($this->acc_id != 0) {
            foreach ($this->ion_auth->get_users_groups($this->acc_id)->result() as $ion_group) {
                $groups[] = $ion_group->description;
            }
        }

        // Publish Section
        $this->set_title('Edit Profile');
        $this->add_prop($groups, 'groups');
        $this->add_prop($input_personal, 'input_personal');
        $this->add_prop($input_account, 'input_account');
        $this->compile_page('public/profile');
        $this->publish();
    }

    public function do_edit($acc_id)
    {
        $this->loader->model('account');
        if ($_POST['submit'] === null) {
            return false;
        }

        $guru = $this->guru_m->get_one();
        $guru_pair = $this->get_guru_pair();

        $field_personal = array();

        // get all field
        foreach ($guru_pair as $field) {
            $field_personal[] = $field;
        }

        if ($this->acc_m->edit_account($guru)) {
            $input_personal = $this->prep_input($field_personal);
            $this->guru_m->edit_data($this->acc_id, $input_personal);
            $this->add_message('Profil data berhasil di update', true, 1);
            return true;
        } else {
            return false;
        }
    }
}
