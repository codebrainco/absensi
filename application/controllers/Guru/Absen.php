<?php
class Absen extends Guru_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index($lm = false)
    {
        // build table
        $heading_pair = array(
            'ID' => 'id',
            'Mata Pelajaran' => 'matpel',
            'Kelas'          => 'kelas',
            'Jadwal'         => 'jadwal',
        );

        $action = anchor(base_url("guru/manage/absen".($lm ? "_lm" : '')."/(:guru_matpel_id)#table"), "<span class='glyphicon glyphicon-ok'></span> Absen", array('class' => 'btn btn-sm btn-primary btn-space'));
        if ($lm === false) {
            $all_guru_matpel = $this->get_guru_matpel();
            $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action, array('account_id' => $this->session->user_id), null, $lm = false);
        } else {
            $all_guru_matpel = $this->get_guru_matpel_lm();
            $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action, array('account_id' => $this->session->user_id), null, $lm = true);
        }

        // publish section
        $this->set_title('Manage Absensi', 'Melakukan absensi pada tahun akademik yang sedang aktif');
        $this->add_prop($this->get_ta_aktif(), 'ta_aktif');
        $this->add_prop($table_matpel, 'table');
        $this->append_page('public/card-table');
        $this->publish();
    }

    public function absensi($guru_matpel_id, $lm = false)
    {
        $this->loader->model('siswa', 'absensi_kbm', 'disabled_date', 'berita_acara');
        // validation check if it is siswa or any other guru than logged-in guru
        if ( ! $this->is_allowed_to_edit($guru_matpel_id, $lm)) {
            /**
            * @todo ganti error
            */
            show_error('You dont have permission', 403, 'Permission');
        }

        // fetch validation items
        $guru_matpel_jadwal = $this->get_jadwal($guru_matpel_id);

        if ($lm) {
            $guru_matpel = $this->get_guru_matpel_lm($guru_matpel_id);
        } else {
            $guru_matpel = $this->get_guru_matpel($guru_matpel_id);
        }

        $ta_aktif = $this->get_ta_aktif();


        // set time, days and years for right now
        $now_time = strtotime(mdate("%H:%i:00", now('Asia/Jakarta')));
        $now_days = mdate("%l", now('Asia/Jakarta'));
        $now_years = mdate("%Y", now('Asia/Jakarta'));

        $token = false;

        // for each jadwal exist in guru_matpel_jadwal of that guru_matpel
        foreach ($guru_matpel_jadwal as $jadwal) {
            // and in allowed hours as in jadwal
            // ====== year is disabled, somehow i dunno
            // $year = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;
            // $in_year = $year == $now_years;
            $in_days = $this->get_hari_eng($jadwal['hari']) == $now_days;
            $in_time = ($now_time <= strtotime($jadwal['end_hrs']) && $now_time >= strtotime($jadwal['begin_hrs']));

            if ($in_time && $in_days) {
                // if found one that true, break with true
                $token = $jadwal;
                break;
            }
        }

        $berita_acara_opt = array(
            'Tanggal' => 'tanggal|text',
            'Materi' => 'materi|text',
            'Tugas Siswa' => 'tugas_siswa|text',
        );

        if ($token !== false) {
            // fetch common items
            $profile = $this->get_profile();
            $ta_aktif = $this->get_ta_aktif();
            $available_angkatan = $this->get_allowed_angkatan();

            // all siswa from avaliable angkatan, and kelas and lintas minat
            if ($lm) {
                $all_siswa = $this->siswa_m->get_siswa_lm($guru_matpel->kelas_id, $available_angkatan);
            } else {
                $all_siswa = $this->siswa_m->with_account()->where('angkatan', $available_angkatan)->where('kelas_id', $guru_matpel->kelas_id)->get_all() ?: array();
            }

            // prepare today, hari, bulan, bulan no and tanggal
            $hari = mdate("%D", now('Asia/Jakarta'));
            $bulan = mdate("%F", now('Asia/Jakarta'));
            $bulan_no = mdate("%m", now('Asia/Jakarta'));
            $tanggal = mdate("%d", now('Asia/Jakarta'));

            $siswa_id_arr = array();
            foreach ($all_siswa as $siswa) {
                $siswa_id_arr[] = $siswa->account_id;
            }

            // build table absensi
            $tmp_abs = array();
            $tmp = $this->abs_kbm_m->get_absensi_all($guru_matpel->tahun_ajaran, $bulan_no.'-'.$tanggal, $guru_matpel_id, $siswa_id_arr);

            $status_dropdown = $this->generate_absensi($tmp, true);
            $jadwal = substr($token['begin_hrs'], 0, 5).'-'.substr($token['end_hrs'], 0, 5);

            // set all dates, all day to be just one array, today
            $all_dates['dates'] = array(
                $bulan => array(
                    $tanggal => array(
                        substr($token['begin_hrs'], 0, 5).'-'.substr($token['end_hrs'], 0, 5),
                    )
                )
            );

            $all_dates['day'] = array(
                $bulan => array(
                    $tanggal => $hari
                )
            );

            // build an option none
            $status_hadir = $this->get_status_hadir();
            $option_none = '';
            foreach ($status_hadir as $key => $hadir) {
                $option_none .= '<option value="'.($key).'">'.$hadir.'</option>';
            }

            // another dependencies for output
            $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;
            $form_tahun = form_hidden('tahun', $tahun);

            $time_data = array(
                $tahun.'-'.mdate('%m', strtotime($bulan)).'-'.$tanggal,
                $jadwal
                );


            $data = $this->ba_m->get_ba($guru_matpel_id, $time_data);
            if (isset($_POST['submit'])) {
                $post = $this->prep_input('materi', 'tugas_siswa');
                $post['guru_matpel_id'] = $guru_matpel_id;
                $post['tanggal'] = $time_data[0];
                $post['hours'] = $time_data[1];
                if ( ! is_null($data)) {
                    $this->ba_m->update_ba($post);
                } else {
                    $this->ba_m->insert($post);
                }
                $update_data = array();
                $update_data[] = array(
                    'account_id' => $this->session->user_id,
                    'guru_matpel_id' => $guru_matpel_id,
                    'tanggal' => $time_data[0],
                    'hours' => $time_data[1],
                    'hadir' => 1,
                    );
                $checked_pair = $update_data[0];
                unset($checked_pair['hadir']);

                if ($this->abs_kbm_m->is_unique($checked_pair)) {
                    $this->abs_kbm_m->insert_single($update_data);
                } else {
                    $this->abs_kbm_m->update_single($update_data);
                }
                $this->add_message('Berita acara berhasil di update', false, 1);
                $data = $this->ba_m->get_ba($guru_matpel_id, $time_data);
            }

            if (is_null($data)) {
                $data = new stdClass();
                $data->materi = '';
                $data->tugas_siswa = '';
            }
            $data->tanggal = $time_data[0];
            $data->hours = $time_data[1];

            $field = array(
                'Tanggal' => 'tanggal|date|disabled[]',
                'Jam' => 'hours|text|disabled[]',
                'Materi' => 'materi|text',
                'Tugas Siswa' => 'tugas_siswa|text'
                );

            $input = $this->form_generator($field, $data, 'form-control');

            // publish section
            $this->set_title('Absensi');
            $this->add_prop($form_tahun, 'form_tahun');
            $this->add_prop($guru_matpel, 'guru_matpel');

            $this->add_prop($input, 'input');
            $this->add_prop($status_dropdown, 'status_dropdown');
            $this->add_prop($option_none, 'option_none');
            $this->add_prop($all_siswa, 'all_siswa');
            $this->add_prop($all_dates['dates'], 'all_dates');
            $this->add_prop($all_dates['day'], 'all_day');
            $this->append_page($this->get_site_root('manage_absen'));

            $this->publish();
        } else {
            /**
            * @todo ganti error
            */
            show_error('Anda tidak bisa mengabsen diluar jam mengajar, tekan back', 403, 'Out of allowed time');
        }
    }
}
