<?php
class Report extends Guru_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function attendance()
    {
        $this->loader->model('absensi_kbm');
        $this->loader->library('table');
        $this->loader->helper('form');
        // fetch common items
        $list_absensi_kbm = $this->abs_kbm_m->count_absensi($this->acc_id);

        // make absensi array
        $absensi = array();
        foreach ($list_absensi_kbm as $one_absensi) {
            $absensi[$one_absensi->guru_matpel_id] = $one_absensi;
        }

        // generate table
        $heading = array(
            'Nama Matpel' => 'matpel',
            'Kelas' => 'kelas',
            'Kehadiran' => array('field' => 'kehadiran', 'data' => $absensi),
        );
        $action = anchor(base_url('guru/report/attendance/detail/(:guru_matpel_id)'), '<span class="glyphicon glyphicon-eye-open"></span> Detail Absensi', array('class' => 'btn btn-default btn-xs'));
        $all_guru_matpel = $this->get_guru_matpel();
        $table_matpel = $this->guru_matpel_table_generate($all_guru_matpel, $heading, $action);

        // Publish Section
        $this->add_prop($this->get_account_group(), 'group');
        $this->add_prop($this->get_ta_aktif(), 'ta_aktif');
        $this->set_title('Laporan Guru Mengajar');
        $this->set_breadcrumbs('Laporan', 'Laporan Guru Mengajar');
        $this->add_prop($table_matpel, 'table');
        $this->compile_page('public/card-table');
        $this->publish();
    }

    // ---------------------------------------------------------------------------

    /**
    * More info @see Manage_Controller::detail_absensi()
    *
    * @param  int $guru_matpel_id
    * @return void
    */
    public function detail_attendance($guru_matpel_id)
    {
        if ( ! $this->is_allowed_to_edit($guru_matpel_id)) {
            /**
            * @todo ganti error
            */
            show_error('You dont have permission', 403, 'Permission');
        }
        $this->detail_absensi($guru_matpel_id);
    }

}
