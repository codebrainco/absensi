<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Home extends Base_Controller
{

    /*
    |
    | Class var
    | ----------------------------------------------------------------------------
    | $site_root          = Contain root this controller root directory
    | $view_data          = Data that will be passed to view
    | $cfg                = Config Object
    |
    | Function
    | ----------------------------------------------------------------------------
    | init               = Initialize script
    | set_data           = Assign another key and value to view_data
    | add_message        = Assign message to view_data
    | add_prop           = Assign another key beside view_data and message
    | prep_input         = Prepare input to be passed to model
    | compile_page       = Compile the page and pass view_data
    |
    */

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Siswa_model', 'siswa_m');
        $this->load->model('Account_model', 'acc_m');
        $this->load->library('ion_auth');
        $this->load->library('form_validation');
        $this->template->set_template($this->site_root.'template');
        $this->template->stylesheet->add(asset_url('css/template.min.css'));
        $this->template->stylesheet->add(asset_url('css/public.css'));
        $this->template->javascript->add(asset_url('js/jquery-2.1.4.min.js'));
        $this->template->javascript->add(asset_url('js/template.min.js'));
        $this->template->javascript->add(asset_url('js/public.js'));
        $this->template->meta->add('viewport', 'width=device-width, initial-scale=1.0');
    }

    public function index()
    {
        redirect(base_url('login'));
    }

    public function view_login()
    {
        $this->template->title = 'Login';

        if ($this->form_validation->run('login') === true) {
            $post_login = array(
                'identity' => $this->input->post('identity'),
                'password' => $this->input->post('password'),
                );

            $login_token = false;
            if ($this->acc_m->username_login($post_login)) {
                $login_token = true;
            } elseif ($this->acc_m->email_login($post_login)) {
                $login_token = true;
            } elseif ($this->acc_m->nis_login($post_login)) {
                $login_token = true;
            } else {
                $this->add_message('Wrong credential', false, 2);
            }

            if ($login_token) {
                $this->add_message($this->ion_auth->messages(), true);
                if ($this->ion_auth->in_group(1)) {
                    redirect('/interface', 'refresh');
                } elseif ($this->ion_auth->in_group(2)) {
                    redirect('/siswa', 'refresh');
                } elseif ($this->ion_auth->in_group(3)) {
                    redirect('/guru', 'refresh');
                }
            }
        } else {
            $this->add_message(validation_errors(), false);
        }


        // Input Properties
        $input_prop['Username'] = array(
            'name'  => 'identity',
            'id'    => 'identity',
            'type'  => 'text',
            'placeholder' => 'Username/Email',
            'value' => $this->form_validation->set_value('identity'),
            'class' => 'form-control',
            );
        $input_prop['Password'] = array(
            'name'  => 'password',
            'id'    => 'password',
            'placeholder' => 'Password',
            'type'  => 'password',
            'class' => 'form-control',
            );

        $this->add_prop($input_prop, 'input');
        $this->compile_page('view_login');
        $this->template->publish();
    }

    // public function view_register()
    // {
    //   $this->template->title = 'Daftar';
    //   $this->form_validation->set_rules('identity', 'Identity', 'required|is_unique[account.username]');
    //   $this->form_validation->set_rules('password', 'Password', 'required|min_length[6]');
    //   $this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|matches[password]');
    //   $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[account.email]');
    //   $this->add_prop('false', 'registStatus');

    //   if ($this->form_validation->run() === true)
    // / {
    //     $post_register = $this->prep_input('identity', 'password', 'email');
    //     if ( $this->ion_auth->register(
    //       $post_register['identity'],
    //       $post_register['password'],
    //       $post_register['email']
    //     ))
    // / {
    //       $this->add_prop('true', 'registStatus');
    //     }
    //   }
     // else
    // / {
    //     $this->add_message((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));

    //     $input_prop['Username'] = array(
    //       'name'  => 'identity',
    //       'id'    => 'identity',
    //       'type'  => 'text',
    //       'value' => $this->form_validation->set_value('identity'),
    //       'class' => 'form-control',
    //       );
    //     $input_prop['E-Mail'] = array(
    //       'name'  => 'email',
    //       'id'    => 'email',
    //       'type'  => 'email',
    //       'value' => $this->form_validation->set_value('email'),
    //       'class' => 'form-control',
    //       );
    //     $input_prop['Password'] = array(
    //       'name'  => 'password',
    //       'id'    => 'password',
    //       'type'  => 'password',
    //       'class' => 'form-control',
    //       );
    //     $input_prop['Password Confirmation'] = array(
    //       'name'  => 'passconf',
    //       'id'    => 'passconf',
    //       'type'  => 'password',
    //       'class' => 'form-control',
    //       );

    //     $this->add_prop($input_prop, 'input');
    //   }

    //   $this->compile_page('view_register');
    //   $this->template->publish();
    // }

    public function view_interface() {
        $this->check_auth(1);
        $this->add_prop(base_url('/admin'), 'admin');
        for ($i=2; $i <= 3; $i++) {
            if ($this->ion_auth->in_group($i)) {
                $group_name = $this->ion_auth->group($i)->row()->name;
                $this->add_prop(base_url('/'.$group_name), 'group');
                $this->session->in_group = new stdClass();
                $this->session->in_group->id = $i;
                $this->session->in_group->name = $group_name;
                break;
            }
        }
        $this->compile_page('view_interface');
        $this->template->publish();
    }
}
