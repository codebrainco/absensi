<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_C extends Admin_Controller
{
    public function __construct()
    {
        parent::__construct();

        // @fixme
        $this->load->library('Excel');
    }

    // -------------------------------------------------------------------------

    // -------------------------------------------------------------------------

    public function admin_remove($acc_id)
    {
        $this->ion_auth->remove_from_group(1, $acc_id);
        $this->add_message('Account ID : '.$acc_id.' berhasil diturunkan dari admin.', true, 1);
        redirect($this->session->referral_url);
    }

    // -------------------------------------------------------------------------

    public function admin_add($acc_id)
    {
        $this->ion_auth->add_to_group(1, $acc_id);
        $this->add_message('Account ID : '.$acc_id.' berhasil dinaikan menjadi admin.', true, 1);
        redirect($this->session->referral_url);
    }



    // -------------------------------------------------------------------------
}
