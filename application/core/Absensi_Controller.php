<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * -----------------------------------------------------------------------------
 * Absensi_Controller Class
 * -----------------------------------------------------------------------------
 *
 * This controller will take care of Absensi Controller as 1st Branch of 3
 * Level Auth system
 *
 * TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
 * ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
 *
 * TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
 *
 * Properties
 * -----------------------------------------------------------------------------
 * $account_pair  = pair for account
 * $bulan         = 12 month in indonesian
 * $bulan_eng     = 12 month in english
 * $guru_pair     = pair for guru
 * $hari          = 7 days in indonesian
 * $hari_eng      = 7 days in english
 * $jenis_kelamin = jenis kelamin
 * $root          = determine root
 * $semester      = semester
 * $siswa_options = dropdown option for siswa
 * $siswa_pair    = pair for siswa
 * $status_hadir  = 4 status hadir
 *
 * Function
 * -----------------------------------------------------------------------------
 * fetch_profile              = Fetch profile from database
 * fetch_config               = Fetch config from database
 * fetch_tahun_ajaran         = Fetch tahun ajaran from database
 * main_nav                   = Compile the main navigation
 * edit_account               = Edit account evaluate
 * man_nav                    = 'Manage' navigation
 * form_input_account         = Form of input account
 * form_generator             = Generate form from input array structured string
 * generate_tanggal           = Generate tanggal in 1 year
 * generate_absensi           = Generate absensi based on get data
 * guru_matpel_table_generate = Generate table of guru_matpel
 * is_allowed_to_edit         = Verification of permission
 * count_kehadiran            = Count kehadiran from specific account_id
 * account_filter             = Filter for account before passed to view
 * guru_matpel_filter         = Filter for guru_matpel before passed to view
 * slice_tanggal              = Slice to 3 part of date
 * nice_kelas                 = Get nice kelas
 * get_profile                = Get profile
 * get_config                 = Get config
 * get_absensi_kbm            = Get absensi_kbm
 * matpel_m->get_matpel                 = Get matpel
 * get_absensi_kegiatan       = Get absensi_kegiatan
 * get_status_hadir           = Get 4 status hadir
 * get_hari                   = Get 7 Days
 * get_hari_eng               = Get 7 Days Eng
 * get_bulan                  = Get 12 month
 * get_bulan_eng              = Get 12 month Eng
 * get_siswa_pair             = Get pair of siswa
 * get_guru_pair              = Get pair of guru
 * get_account_pair           = Get pair of account
 * get_siswa_opt          = Get options for siswa dropdown
 * get_all_kelas              = Get all available kelas
 * get_kelas                  = Get singke kelas
 * get_jurusan                = Get jurusan
 * get_imploded_jadwal        = Get imploded jadwal
 * get_kehadiran_color        = Get kehadiran color
 * get_status_hadir_color     = Get status_hadir color
 * get_jadwal                 = Get jadwal
 * get_root                   = Get root
 */
class Absensi_Controller extends Base_Controller
{
    /**
    * 4 type of status hadir, hadir sakit izin alfa
    *
    * @var string[]
    */
    public $status_hadir;

    /**
     * 7 name of days in Indonesian
     *
     * @var string[]
     */
    public $hari;

    /**
     * 7 name of days in English
     *
     * @var string[]
     */
    public $hari_eng;

    /**
     * 12 name of month in Indonesian, with offset 1
     *
     * @var string[]
     */
    public $bulan;

    /**
     * 12 name of month in English, with offset 1
     * @var strnig[]
     */
    public $bulan_eng;

    /**
     * Heading-field pair for account
     * @var string[]
     */
    public $account_pair;

    /**
     * Heading-field pair for siswa
     * @var string[]
     */
    public $siswa_pair;

    /**
     * Heading-field pair for guru
     * @var string[]
     */
    public $guru_pair;

    /**
     * Dropdown option for siswa
     * @var string[]
     */
    public $siswa_options;

    /**
     * Semester
     * @var string[]
     */
    public $semester;

    /**
     * Semester
     * @var string[]
     */
    public $jenis_kelamin;

    /**
     * Root
     *
     * @var string
     */
    public $root;

    public function __construct()
    {
        parent::__construct();

        // $this->load->helper('html');
        // $this->load->library('table');
        // $this->load->library('filter_manager');
        // $this->load->library('pdf');

        if ( ! $this->ion_auth->logged_in()) {
            redirect('/', 'refresh');
        }

        $this->load->library('loader');

        $this->fetch_profile();

        // template engine
        $this->template->javascript->add(asset_url('js/template.min.js'));
        $this->template->javascript->add(asset_url('js/toastr.min.js'));
        $this->template->javascript->add(asset_url('js/public.js'));
        $this->template->stylesheet->add(asset_url('css/template.min.css'));
        $this->template->stylesheet->add(asset_url('css/public.css'));
        $this->template->meta->add('viewport', 'width=device-width, initial-scale=1.0');

        $this->template->set_template('public/template');

        date_default_timezone_set('Asia/Jakarta');

        // for populating message
        foreach ($this->session->flashdata('message') ?: array() as $flashdata) {
            $this->add_message($flashdata['message_body'], false, $flashdata['severity'], $flashdata['severity_message']);
        }

        // optional set of profiler for performance report only
        $this->output->enable_profiler(true);
    }

    // ---------------------------------------------------------------------------

    /**
     * Fetch from account and guru|siswa database and assign to session profile
     *
     * @return object|exit Will return object profile or exit if profile null
     */
    public function fetch_profile()
    {
        // change group based who is currenty logged in
        $group_name = $this->get_account_group_name();
        $model = $group_name.'_m';

        $this->loader->model($group_name);

        $acc_id = $this->session->user_id;

        // fetch profile data
        $profile = $this->$model->get_profile($acc_id);

        if ($profile === null) {
            show_error('Something went wrong, please contact admin');
        }

        // add additional data
        $profile->group = $this->ion_auth->is_admin() ? 'admin' : $this->ion_auth->group($this->get_account_group())->row()->name;
        $profile->group_not_admin = $this->uri->segment(1, 0) == 'admin' ? $this->ion_auth->group($this->get_account_group())->row()->name : 'admin';

        $this->session->profile = $profile;

        return $profile;
    }

    // ---------------------------------------------------------------------------

    /**
     * Load Main navigation based on group
     *
     * @return html sidebar of main navigation with bootstrap pills
     */
    public function main_nav()
    {
        $group_name = $this->get_account_group_name();

        $nav_item = array(
            array('fa_icon' => 'tachometer', 'link' => base_url($group_name.'/dashboard/'), 'html' => 'Dashboard'),
            array('fa_icon' => 'user', 'link' => base_url($group_name.'/profile'), 'html' => 'Profile'),
            array('fa_icon' => 'file-text-o', 'id' => 'report', 'html' => 'Laporan', 'dropdown' => array(
                array('link' => base_url($group_name.'/report/attendance'), 'html' => 'Kehadiran'),
                )
            ),
        );

        $this->main_nav_extends($nav_item);

        // anchor($group_name.'/manage', 'Manage'),
        return $nav_item;
    }

    public function main_nav_extends(&$nav_item)
    {
        return true;
    }

    // ---------------------------------------------------------------------------

    /**
     * Validation for edit account on profile
     *
     *
     * @return bool TRUE if updated succesfully, FALSE otherwise
     */


    // ---------------------------------------------------------------------------

    /**
     * Form for edit account
     *
     * @param object  $profile  profile data used for account form
     * @param bool    $admin    is admin or not
     * @return array[] of form data
     */
    public function form_input_account($profile, $admin = false)
    {
        $input_account = array();

        // check if its and add or edit
        $acc_id = ($profile ? $profile->account_id : '');

        // add form hidden to handle account id

        // check if it is an admin or not
        $disabled = $profile ? 'disabled' : '';

        $input_account['Username'] = form_input(array(
            'name'  => 'identity',
            'id'    => 'username',
            'type'  => 'text',
            'value' => $profile ? $profile->account->username : set_value('identity'),
            'class' => 'form-control',
            // $disabled => ''
            ));
        $input_account['E-Mail'] = form_input(array(
            'name'  => 'email',
            'id'    => 'email',
            'type'  => 'email',
            'value' => $profile ? $profile->account->email : set_value('email'),
            'class' => 'form-control',
            ));
        $input_account['Password'] = form_input(array(
            'name'        => 'currpass',
            'id'          => 'currpass',
            'type'        => 'password',
            'placeholder' => 'Current Password',
            'class'       => 'form-control',
            ));
        if ($admin === false) {
            $input_account['--'] = form_input(array(
                'name'        => 'password',
                'id'          => 'password',
                'type'        => 'password',
                'placeholder' => 'New Password',
                'class'       => 'form-control',
                ));
            $input_account['-'] = form_input(array(
                'name'        => 'passconf',
                'id'          => 'passconf',
                'type'        => 'password',
                'placeholder' => 'New Password Confirmation',
                'class'       => 'form-control',
                ));
        }
        $input_account[''] = form_hidden('account_id', $acc_id);

        return $input_account;
    }

    // ---------------------------------------------------------------------------

    /**
     * Generate form from specifically typed array
     *
     * $heading_field is meant to have 3 structure in 1 string separeted
     * by | and passed to $rule, it 3 structure is as follow :
     * {name}|{type}|{attributes[value]}
     * {name} will fill name, id, value and set_value
     * {type} will fill type
     * {attribues[value]} will fill 1 or more attributes
     *
     * @todo make the 4th structure for optional attributes
     * @param  string[]  $heading_field array of heading and their field
     * @param  object    $data          optional, if it need data to be filled
     * @param  string    $class         optional, if it need general class
     * @return string[]                 of baked form_input
     */
    public function form_generator($heading_field, $data = null, $class = null)
    {
        $this->loader->helper('form');
        $input = array();
        foreach ($heading_field as $heading => $field) {
            // explode to have 2 main structure, and 1 or more optional
            $rule = explode('|', $field);
            $options = array(
                'name'        => $rule[0],
                'id'          => $rule[0],
                'type'        => $rule[1],
                'value'       => $data ? $data->{$rule[0]} : set_value($rule[0]),
                'placeholder' => $heading,
                'class'       => $class,
                );
            // if another attributes is present
            if (isset($rule[2])) {
                // iterate through all optional attributes
                for ($i = 2; $i < count($rule); $i++) {
                    $attribute = '';
                    $value = '';

                    // regex to get attribute (before [)
                    $regex_attribute = '/([^\]]*)\[/';

                    // regex to get value (inside [])
                    $regex_value = '/\[([^\]]*)\]/';

                    preg_match($regex_attribute, $rule[$i], $attribute);
                    preg_match($regex_value, $rule[$i], $value);

                    // if class, add it
                    if ($attribute[1] == 'class') {
                        $options[$attribute[1]] = $class.' '.$value[1];
                    } else {
                        $options[$attribute[1]] = $value[1];
                    }
                }
            }
            $input[$heading] = form_input($options);
        }
        return $input;
    }

    // ---------------------------------------------------------------------------

    /**
     * Generate tanggal in 1 term based on jadwal
     *
     * @param  object $guru_matpel guru_matpel
     * @return array[]             of all date with their respective day
     */
    public function generate_tanggal($guru_matpel, $periode = null, &$total_pertemuan = 0)
    {
        $this->loader->model('guru_matpel_jadwal');

        // get ta date based on tahun ajaran of guru matpel join with tahun_ajaran
        $ta = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id);

        // set begin and end(array) date same as tahun ajaran setting
        $begin = new DateTime($ta->begin_date, new DateTimeZone('Asia/Jakarta'));
        $end = new DateTime($ta->end_date, new DateTimeZone('Asia/Jakarta'));

        $all_jadwal = $this->get_jadwal($guru_matpel->guru_matpel_id);
        $all_jadwal_tanggal = array();
        foreach ($all_jadwal as $jadwal) {
            $tmp_hours = substr($jadwal['begin_hrs'], 0, 5).'-'.substr($jadwal['end_hrs'], 0, 5);
            $all_jadwal_tanggal[] = $jadwal['hari'];
        }

        $all_absen = $this->abs_kbm_m->get_absensi($guru_matpel->id) ?: array();

        $all_dates = array();
        $all_dates['date'] = array();
        $all_dates['day'] = array();

        // loop day per day in 1 periode

        $total_pertemuan = 0;
        $this->counter_init();
        $today = new DateTime();
        while ($begin <= $end) {
            // if not the month, go to next month
            if ($periode !== null) {
                if ($periode != $begin->format("n")) {
                    $begin->modify('first day of next month');
                    continue;
                }
            }

            // check from absen, if there's already absensi
            $tmp_tanggal = clone $begin;
            $token = true;
            for ($i = 0; $i < 7; $i++) {
                foreach ($all_absen as $absen) {
                    if ($tmp_tanggal->format("Y-m-d") == $absen->tanggal) {
                        $all_dates['day'][$tmp_tanggal->format("F")][$tmp_tanggal->format("d")][] = $tmp_tanggal->format("D");
                        $all_dates['date'][$tmp_tanggal->format("F")][$tmp_tanggal->format("d")][] = $absen->hours;
                        $token = false;
                        if ($tmp_tanggal <= $today) {
                            $this->counter_inc();
                        }
                    }
                }

                $tmp_tanggal->modify('+1 day');
            }

            // if that week didnt have absensi, continue counting
            if ($token) {
                $tmp_tanggal = clone $begin;
                $i = 0;
                while ($i < 7 && ($periode === null || $periode == $tmp_tanggal->format("n"))) {
                    foreach ($all_jadwal as $jadwal) {
                        if ($tmp_tanggal->format("N") == $jadwal['hari']) {
                            $tmp_hours = substr($jadwal['begin_hrs'], 0, 5).'-'.substr($jadwal['end_hrs'], 0, 5);
                            $all_dates['day'][$tmp_tanggal->format("F")][$tmp_tanggal->format("d")][] = $tmp_tanggal->format("D");
                            $all_dates['date'][$tmp_tanggal->format("F")][$tmp_tanggal->format("d")][] = $tmp_hours;
                            if ($tmp_tanggal <= $today) {
                                $this->counter_inc();
                            }
                        }
                    }

                    $tmp_tanggal->modify('+1 day');
                    $i++;
                }
            }

            $begin->modify('+1 week');
        }

        $total_pertemuan = $this->get_counter();
        return $all_dates;
    }

    // ---------------------------------------------------------------------------

    /**
     * Generate absensi that available
     *
     * @param  object  $tmp_abs array of absensi from get_absensi
     * @param  boolean $edit    optional if edit (admin)
     * @return string[]            of baked select|button
     */
    public function generate_absensi($tmp_abs, $edit = false)
    {
        $this->loader->helper('form');

        $absensi = array();
        $status_dropdown = array();
        $ddate = $this->ddate_m->get_all_ddate();

        foreach ($tmp_abs as $abs) {
            $tmp_kehadiran = array(
                'id' => $abs->id,
                'kehadiran' => $this->get_status_hadir($abs->hadir),
                'color' => $this->get_status_hadir_color($abs->hadir)
                );
            $tmp_tanggal = $this->slice_tanggal($abs->tanggal);
            // make an array of absensi or dropdown absensi with format :
            // absensi[{month}][{date}][{account_id}] = $tmp_kehadiran
            // status_dropdown[{month}][{date}][{account_id}] = dropdown

            $absensi[$this->get_bulan_eng($tmp_tanggal->bulan)][$tmp_tanggal->tanggal][$abs->hours][$abs->account_id] = $tmp_kehadiran;
            $status_dropdown[$this->get_bulan_eng($tmp_tanggal->bulan)][$tmp_tanggal->tanggal][$abs->hours][$abs->account_id] = form_dropdown('status['.$this->get_bulan_eng($tmp_tanggal->bulan).']['.$tmp_tanggal->tanggal.']['.$abs->hours.']['.$abs->account_id.']', $this->get_status_hadir(), $abs->hadir, array('class' => 'btn btn-sm original-select'));

            /**
             * @todo  Tambahin buat yang aktif aja, gk usah semua
             */
            foreach ($ddate as $value) {
                $tmp_kehadiran = array(
                    'id' => 0,
                    'kehadiran' => 'Libur',
                    'color' => $this->get_status_hadir_color(5)
                    );
                $tmp_ddate = $this->slice_tanggal($value->tanggal);

                $absensi[$this->get_bulan_eng($tmp_ddate->bulan)][$tmp_ddate->tanggal][$abs->hours] = $tmp_kehadiran;
                $status_dropdown[$this->get_bulan_eng($tmp_ddate->bulan)][$tmp_ddate->tanggal][$abs->hours] = form_dropdown('status['.$this->get_bulan_eng($tmp_ddate->bulan).']['.$tmp_ddate->tanggal.']['.$abs->account_id.']', array('Libur'), '', array('class' => 'btn btn-sm original-select'));
            }
        }

        // if edit true return the dropdown instead of just button
        if ($edit === true) {
            return $status_dropdown;
        } else {
            return $absensi;
        }
    }


    /**
     * Generate absensi that available
     *
     * @param  object  $tmp_abs array of absensi from get_absensi
     * @param  boolean $edit    optional if edit (admin)
     * @return string[]            of baked select|button
     */
    public function generate_ba($tmp_berita_acara)
    {
        $berita_acara = array();
        $ddate = $this->ddate_m->get_all();

        foreach ($tmp_berita_acara as $ba) {
            $tmp_ba = array(
                'id' => $ba->id,
                'materi' => $ba->materi,
                'tugas_siswa' => $ba->tugas_siswa,
                );
            $tmp_tanggal = $this->slice_tanggal($ba->tanggal);

            $berita_acara[$this->get_bulan_eng($tmp_tanggal->bulan)][$tmp_tanggal->tanggal][$ba->hours] = $tmp_ba;

            /**
             * @todo  Change logic to make it libur
             */
            // foreach ($ddate as $value) {
            //     $tmp_kehadiran = array(
            //         'id' => 0,
            //         'kehadiran' => 'Libur',
            //         'color' => $this->get_status_hadir_color(5)
            //         );
            //     $tmp_ddate = $this->slice_tanggal($value->tanggal);

            //     $absensi[$this->get_bulan_eng($tmp_ddate->bulan)][$tmp_ddate->tanggal][$abs->hours] = $tmp_kehadiran;
            //     $status_dropdown[$this->get_bulan_eng($tmp_ddate->bulan)][$tmp_ddate->tanggal][$abs->hours] = form_dropdown('status['.$this->get_bulan_eng($tmp_ddate->bulan).']['.$tmp_ddate->tanggal.']['.$abs->account_id.']', array('Libur'), '', array('class' => 'btn btn-sm original-select'));
            // }
        }

        return $berita_acara;
    }

    // ---------------------------------------------------------------------------

    /**
     * Generate table consist of table matpel and based on guru_matpel per
     * end-point controller
     *
     * @param  [type] $heading_pair [description]
     * @param  [type] $action       [description]
     * @return [type]               [description]
     */
    public function guru_matpel_table_generate($all_guru_matpel, $heading_pair, $action = null)
    {
        $this->loader->model('guru_matpel_jadwal');
        $this->loader->library('table');
        $this->loader->helper('form');
        // if ($filter === null) {
        //     $list_guru_matpel = $lm ? $this->guru_matpel_m->get_lm() : $this->get_guru_matpel();
        // } else {
        //     $list_guru_matpel = $lm ? $this->guru_matpel_m->get_lm(null, $filter) : $this->get_guru_matpel(null, $filter);
        // }

        // populate heading_pair, basically heading pair is consist of array with
        // heading => field, so we only need the heading.
        $heading_array = array('No');
        foreach ($heading_pair as $heading => $field) {
            $heading_array[] = $heading;
        }

        // if it need action or not
        if ($action !== null) {
            $heading_array[] = 'Action';
        }

        $this->table->set_template(array('table_open' => '<table class="table table-sort" width=\'100%\'>', 'heading_cell_start'    => '<th style="vertical-align:middle; text-align:center">',));
        $this->table->set_heading($heading_array);

        $all_guru_matpel_id = array_column($all_guru_matpel, 'id');

        // check dependencies
        if (in_array('jadwal', $heading_pair)) {
            // get jadwal
            $all_jadwal = $this->guru_matpel_jadwal_m->get_all_jadwal($all_guru_matpel_id);
            $all_jadwal = $this->get_imploded_jadwal($all_jadwal);
        }
        if (in_array('ta', $heading_pair)) {
            // get tahun ajaran
            $all_ta = $this->ta_m->get_all_ta();
        }

        $this->counter_init();
        foreach ($all_guru_matpel as $guru_matpel) {
            if (in_array('jadwal', $heading_pair)) {
                $guru_matpel->jadwal = isset($all_jadwal[$guru_matpel->id]) ? $all_jadwal[$guru_matpel->id]['jadwal_hari'] : '';
            }
            if (in_array('ta', $heading_pair)) {
                $guru_matpel->ta = $all_ta[$guru_matpel->tahun_ajaran_id]->tahun_ajaran;
            }
            if (in_array('semester', $heading_pair)) {
                $guru_matpel->semester = $this->get_semester($all_ta[$guru_matpel->tahun_ajaran_id]->semester);
            }
            if (isset($heading_pair['Total Kehadiran'])) {
                $guru_matpel->total_kehadiran = isset($heading_pair['Total Kehadiran']['data'][$guru_matpel->id]) ? $heading_pair['Total Kehadiran']['data'][$guru_matpel->id]->kehadiran : 0;
            }
            if (array_search('kehadiran', array_column($heading_pair, 'field')) !== null) {
                // $kehadiran = $this->count_kehadiran($guru_matpel, $heading_pair['Kehadiran']['data']->kehadiran);
                // $color = $this->get_kehadiran_color($kehadiran);
                $array = array_column($heading_pair, 'data', 'field');
                if (isset($array['kehadiran'][$guru_matpel->id])) {
                    $kehadiran = round($array['kehadiran'][$guru_matpel->id]->kehadiran / $guru_matpel->total_pertemuan, 2)*100;
                } else {
                    $kehadiran = 0;
                }
                $guru_matpel->kehadiran = form_button('', $kehadiran.'%', array('class' => 'btn btn-space btn-xs btn-default'));
                $guru_matpel->kehadiran_num = $kehadiran;
            }
            if (isset($heading_pair['Grade'])) {
                $guru_matpel->grade = '';
                $guru_matpel->grade .= '<button class="btn btn-default btn-sm btn-space btn-xs">'.$this->get_grade($guru_matpel->kehadiran_num).'</button>';
            }

            // iterate through heading pair and assign to row based in field pair
            $row = array($this->counter_inc());
            foreach ($heading_pair as $heading => $field) {
                // if array get the 'field'
                if (is_array($field)) {
                    $tmp_field = $field['field'];
                    $row[] = $guru_matpel->$tmp_field;
                } else {
                    $row[] = $guru_matpel->$field;
                }
            }
            $row[] = array('data' => $action, 'class' => 'text-center');

            // check for any inside variable
            $final_row = array();
            foreach ($row as $value) {
                $tmp_row = $value;
                $tmp_row = str_replace('(:guru_matpel_id)', $guru_matpel->id, $tmp_row);
                $tmp_row = str_replace('(:account_id)', $guru_matpel->account_id, $tmp_row);
                if (is_numeric($value)) {
                    $tmp_row = array('data' => $tmp_row, 'class' => 'text-center');
                }
                $final_row[] = $tmp_row;
            }

            // add per row
            $this->table->add_row($final_row);
        }

        // generate table
        $table_matpel = $this->table->generate();
        return $table_matpel;
    }

    // ---------------------------------------------------------------------------

    /**
     * Is this user allowed to edit, if admin yes to all, if guru, just to their
     * guru matpel
     *
     * @param  int  $guru_matpel_id
     * @return boolean
     */
    public function is_allowed_to_edit($guru_matpel_id = null, $lm = false)
    {
        $this->loader->model('guru_matpel');
        // 3 for guru
        if ($this->get_account_group() == 3) {
            // iterate through all guru_matpel per controller (Admin will take
            // all guru_matpel but Guru will only take their guru_matpel)
            $guru_matpel = $lm ? $this->guru_matpel_m->get_lm(null, array('account_id' => $this->session->user_id)) : $this->get_guru_matpel();
            foreach ($guru_matpel as $guru_matpel) {
                // check if guru_matpel is exist on that list
                if ($guru_matpel->id == $guru_matpel_id) {
                    return true;
                }
            }
        }
        if ($this->is_admin() === true) {
            return true;
        }
        return false;
    }

    // ---------------------------------------------------------------------------

    /**
     * Count kehadiran for an account id
     *
     * @param  object $guru_matpel
     * @param  object $absensi     array of absensi
     * @return int                 percentage of kehadiran
     */
    public function count_kehadiran($guru_matpel, $absensi, $single = false)
    {
        // fetch common items
        if ($single) {
            $jumlah_hadir = isset($absensi) ? $absensi->kehadiran : null;
        } else {
            $jumlah_hadir = isset($absensi[$guru_matpel->id]) ? $absensi[$guru_matpel->id]->kehadiran : 0;
        }
        $ta = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id);

        /* Dari total Pertemua, (V1)*/
        // $total_pertemuan = $guru_matpel->total_pertemuan;

        /* Dari couting tanggal, (V2)*/
        // set begin and end date same as tahun ajaran setting
        $begin = new DateTime($ta->begin_date);
        $end = new DateTime($ta->end_date);
        $today = new DateTime(mdate('%Y-%m-%d', now('Asia/Jakarta')));

        $jadwal = $this->get_jadwal($guru_matpel->id);

        $ddate = $this->ddate_m->get_all();

        $total_pertemuan = 0;
        // loop day per day in 1 year
        while ($begin <= $end && $begin <= $today) {
            // loop through jadwal in matpel
            foreach ($jadwal as $hari) {
                // check if jadwal day is same as 'this day'
                if ($begin->format("l") == $this->get_hari_eng($hari['hari']) && ( ! in_array($begin->format("Y-m-d"), array_column($ddate, 'tanggal')))) {
                    $total_pertemuan++;
                }
            }
            $begin->modify('+1 day');
        }

        if ($total_pertemuan == 0) {
            return 0;
        } else {
            $total_pertemuan = 1;
            $tmp_tanggal = array();
            $today = new DateTime();
            $tahun = $this->slice_tanggal($ta->begin_date)->tahun;
            $this->counter_init();
            $total_pertemuan = 0;
            for ($i=1; $i < 13; $i++) {
                $tmp_tanggal = $this->generate_tanggal($guru_matpel, $i, $tmp);
                $total_pertemuan += $tmp;
            }

            $kehadiran = round($jumlah_hadir / $total_pertemuan * 100);
        }
        return $kehadiran;
    }

    public function count_pertemuan($guru_matpel)
    {
        $this->loader->model('disabled_date');
        $ta = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id);

        /* Dari total Pertemua, (V1)*/
        // $total_pertemuan = $guru_matpel->total_pertemuan;

        /* Dari couting tanggal, (V2)*/
        // set begin and end date same as tahun ajaran setting
        $begin = new DateTime($ta->begin_date);
        $end = new DateTime($ta->end_date);
        $today = new DateTime(mdate('%Y-%m-%d', now('Asia/Jakarta')));

        $jadwal = $this->get_jadwal($guru_matpel->id);

        $ddate = $this->ddate_m->get_all_ddate();

        $total_pertemuan = 0;
        // loop day per day in 1 year
        while ($begin <= $end && $begin <= $today) {
            // loop through jadwal in matpel
            foreach ($jadwal as $hari) {
                // check if jadwal day is same as 'this day'
                if ($begin->format("l") == $this->get_hari_eng($hari['hari']) && ( ! in_array($begin->format("Y-m-d"), array_column($ddate, 'tanggal')))) {
                    $total_pertemuan++;
                }
            }
            $begin->modify('+1 day');
        }

        if ($total_pertemuan == 0) {
            return 0;
        } else {
            $total_pertemuan = 1;
            $tmp_tanggal = array();
            $today = new DateTime();
            $tahun = $this->slice_tanggal($ta->begin_date)->tahun;
            $this->counter_init();
            $total_pertemuan = 0;
            for ($i=1; $i < 13; $i++) {
                $tmp_tanggal = $this->generate_tanggal($guru_matpel, $i, $tmp);
                $total_pertemuan += $tmp;
            }
        }
        return $total_pertemuan;
    }

    public function count_all_pertemuan($guru_matpel)
    {
        $this->loader->model('disabled_date');
        $ta = $this->get_tahun_ajaran($guru_matpel[0]->tahun_ajaran_id);

        /* Dari total Pertemua, (V1)*/
        // $total_pertemuan = $guru_matpel->total_pertemuan;

        /* Dari couting tanggal, (V2)*/
        // set begin and end date same as tahun ajaran setting
        $begin = new DateTime($ta->begin_date);
        $end = new DateTime($ta->end_date);
        $today = new DateTime(mdate('%Y-%m-%d', now('Asia/Jakarta')));

        $jadwal = $this->get_jadwal($guru_matpel->id);

        $ddate = $this->ddate_m->get_all_ddate();

        $total_pertemuan = 0;
        // loop day per day in 1 year
        while ($begin <= $end && $begin <= $today) {
            // loop through jadwal in matpel
            foreach ($jadwal as $hari) {
                // check if jadwal day is same as 'this day'
                if ($begin->format("l") == $this->get_hari_eng($hari['hari']) && ( ! in_array($begin->format("Y-m-d"), array_column($ddate, 'tanggal')))) {
                    $total_pertemuan++;
                }
            }
            $begin->modify('+1 day');
        }

        if ($total_pertemuan == 0) {
            return 0;
        } else {
            $total_pertemuan = 1;
            $tmp_tanggal = array();
            $today = new DateTime();
            $tahun = $this->slice_tanggal($ta->begin_date)->tahun;
            $this->counter_init();
            $total_pertemuan = 0;
            for ($i=1; $i < 13; $i++) {
                $tmp_tanggal = $this->generate_tanggal($guru_matpel, $i, $tmp);
                $total_pertemuan += $tmp;
            }
        }
        return $total_pertemuan;
    }


    // ---------------------------------------------------------------------------

    /**
     * Filtering from account that only some data will be passed as final data
     *
     * @param  object $absensi_kbm raw object data after select from account table
     * @return object              filtered object data
     */
    public function assign_dropdown($input)
    {
        $tmp_account = new stdClass();
        $tmp_account = $input;

        // change dropdown code to something human readable
        if (isset($tmp_account->angkatan)) {
            $siswa_options = $this->get_siswa_opt();
            foreach ($siswa_options as $heading => $value) {
                $field = $this->get_siswa_pair($heading);
                if (isset($tmp_account->$field)) {
                    $tmp_account->$field = $value[$tmp_account->$field];
                }
            }
        }

        return $tmp_account;
    }

    // ---------------------------------------------------------------------------

    /**
     * Basically this method make an object 1 level downward to be promoted
     *
     * @param  array|object $list_guru_matpel
     * @return array|obect                    filtered guru_matpel
     */
    public function guru_matpel_filter($list_guru_matpel)
    {
        // if array iterate through all of them
        $this->loader->model('matpel', 'guru');
        if (is_array($list_guru_matpel)) {
            foreach ($list_guru_matpel as $key => $value) {
                $tmp = $value;
                $tmp_res = $this->gm_filtering($tmp);
                $result[] = $tmp_res;
            }
        } else {
            $result = $list_guru_matpel;
            if ( ! empty($list_guru_matpel)) {
                // promote matpel and guru
                $result = $this->gm_filtering($result);
            } else {
                $result = array();
            }
        }
        return isset($result) ? $result : array();
    }

    public function gm_filtering($guru_matpel)
    {
        $matpel = $guru_matpel->matpel->matpel;
        $guru = $guru_matpel->guru->nama;
        $kelas = $guru_matpel->kelas->kelas;
        $tahun_ajaran = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id);

        $guru_matpel->matpel = $matpel;
        $guru_matpel->guru = $guru;
        $guru_matpel->guru_matpel_id = $guru_matpel->id;
        $guru_matpel->kelas = $kelas;
        $guru_matpel->tahun_ajaran = $tahun_ajaran->tahun_ajaran;
        $guru_matpel->tahun_ajaran = $tahun_ajaran->tahun_ajaran;
        $guru_matpel->semester = $this->get_semester($tahun_ajaran->semester);
        $guru_matpel->ta_begin_date = $tahun_ajaran->begin_date;
        $guru_matpel->ta_end_date = $tahun_ajaran->end_date;
        return $guru_matpel;
    }

    // ---------------------------------------------------------------------------

    /**
     * Slice date string to become 3 part of date
     *
     * @param  string $date
     * @return object       tahun, bulan and tanggal
     */
    public function slice_tanggal($date)
    {
        $tmp = explode('-', $date);
        $final = new stdClass();
        $final->tahun = $tmp[0];
        $final->bulan = $tmp[1];
        $final->tanggal = $tmp[2];
        return $final;
    }
    /**
     * Slice date string to become 3 part of date
     *
     * @param  string $date
     * @return object       tahun, bulan and tanggal
     */
    public function slice_kelas($kelas)
    {
        $tmp = explode('-', $kelas);
        $final = new stdClass();
        $final->tingkat = $tmp[0];
        $final->jurusan = $tmp[1];
        $final->kelas = $tmp[2];
        return $final;
    }

    public function slice_kelas_lm($kelas_lm)
    {
        $tmp = explode('-', $kelas_lm);
        $final = new stdClass();
        $final->tingkat = $tmp[0];
        $final->matpel = $tmp[1];
        return $final;
    }

    public function nice_hours($hours)
    {
        $tmp_hours = explode('-', $hours);
        $tmp_hours[0] = substr_replace($tmp_hours[0], ':', 2, 0);
        $tmp_hours[1] = substr_replace($tmp_hours[1], ':', 2, 0);
        $hours = implode('-', $tmp_hours);
        return $hours;
    }

    public function to_model($to_be_model)
    {
        return $to_be_model.'_m';
    }

    public function init_filter($resource = null)
    {
        $filter = $this->input->get();

        foreach ($filter as $key => $value) {
            if ($value == 'all') {
                unset($filter[$key]);
            }
        }

        if ($resource !== null) {
            foreach ($filter as $key => $value) {
                if ( ! in_array($key, $resource)) {
                    unset($filter[$key]);
                }
            }
        }

        return $filter;
    }

    public function title_add_or_edit($id_to_check, $title = '')
    {
        return $id_to_check == 0 ? 'Tambah '.$title : 'Edit '.$title;
    }

    /* ----------- START GET FUNCTION ----------- */


    public function get_all_grade()
    {
        $grades = explode(',', $this->get_config('grade'));

        $return_data = array();
        foreach ($grades as $grade) {
            $tmp_grade = explode('|', $grade);
            $return_data[$tmp_grade[0]] = $tmp_grade[1];
        }
        return $return_data;
    }

    public function get_grade($percentage)
    {
        $grades = explode(',', $this->get_config('grade'));

        foreach ($grades as $grade) {
            $tmp_grade = explode('|', $grade);
            if ($percentage >= $tmp_grade[1]) {
                return $tmp_grade[0];
            }
        }
        return '-';
    }

    public function get_profile()
    {
        return $this->session->profile;
    }

    public function get_config($config_item = null)
    {
        $this->loader->model('config');

        if ($config_item === null) {
            $temp_cfg_data = $this->config_m->get_all();

            $cfg = new stdClass();
            foreach ($temp_cfg_data as $key => $value) {
                $x = $value->prop_name;
                $cfg->$x = $value->prop_value;
            }

            return $cfg;
        } else {

            $temp_cfg_data = $this->config_m->where('prop_name', $config_item)->get();
            return $temp_cfg_data->prop_value;
        }
    }

    public function get_absensi_kbm($acc_id = null, $guru_matpel_id = null)
    {
        if ($acc_id === null) {
            return $this->abs_kbm_m->count_absensi(null, $guru_matpel_id);
        } else {
            return $this->abs_kbm_m->count_absensi($acc_id, $guru_matpel_id);
        }
    }

    public function get_guru($acc_id)
    {
        return $this->guru_m->where('account_id', $acc_id)->get();
    }

    public function get_absensi_kegiatan()
    {
        // $data = $this->abs_kbm_m->count_absensi($this->session->profile->account_id);
        return array();
    }

    public function get_status_hadir($status_no = null)
    {
        $status_hadir = array(0=> 'None', 1 => 'Hadir', 2 => 'Sakit', 3 => 'Izin', 4 => 'Alfa', 5 => 'Terlambat', 6 => 'Absen'); return $status_no === null ? $status_hadir : $status_hadir[$status_no];
    }

    public function get_hari($hari_no = null)
    {
        $hari = array(1 => 'Senin', 2 => 'Selasa', 3 => 'Rabu', 4 => 'Kamis', 5 => 'Jumat', 6 => 'Sabtu', 7 => 'Minggu', );
        return $hari_no === null ? $hari : $hari[$hari_no];
    }

    public function get_hari_eng($hari_no = null)
    {
        $hari_eng = array(1 => 'Monday', 2 => 'Tuesday', 3 => 'Wednesday', 4 => 'Thursday', 5 => 'Friday', 6 => 'Saturday', 7 => 'Sunday');
        return $hari_no === null ? $hari_eng : $hari_eng[$hari_no];
    }

    public function get_bulan($bulan_no = null)
    {
        $bulan = array(' ', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
        return $bulan_no === null ? $bulan : $bulan[ltrim($bulan_no, '0')];
    }

    public function get_bulan_eng($bulan_no = null)
    {
        $bulan_eng = array(' ', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        return $bulan_no === null ? $bulan_eng : $bulan_eng[ltrim($bulan_no, '0')];
    }

    public function get_account_pair($heading = null)
    {
        $this->account_pair = array(
            'Username' => 'username',
            'eMail'   => 'email',
            'Password' => 'password',
            );
    }

    public function get_siswa_pair($heading = null)
    {
        $siswa_pair = array(
            'NIS' => 'nis',
            'Nama' => 'nama',
            'Tanggal Pendaftaran' => 'tanggal_pendaftaran',
            'Kelas' => 'kelas_id',
            'Angkatan' => 'angkatan',
            'Jenis Kelamin' => 'jenis_kelamin',
            'NISN' => 'nisn',
            'No Ijazah SMP' => 'no_ijazah_smp',
            'No Seri SKHUN' => 'no_seri_skhun',
            'No UN SMP' => 'no_un_smp',
            'NIK' => 'nik',
            'Tempat Lahir' => 'tempat_lahir',
            'Tanggal Lahir' => 'tanggal_lahir',
            'Agama' => 'agama',
            'Berkebutuhan Khusus' => 'kebutuhan_khusus',
            'Alamat' => 'alamat',
            'Dusun' => 'alamat_dusun',
            'RT/RW' => 'alamat_rt_rw',
            'Kelurahan' => 'alamat_kelurahan',
            'Kode Pos' => 'alamat_kode_pos',
            'Kecamatan' => 'alamat_kecamatan',
            'Kabupaten' => 'alamat_kabupaten',
            'Provinsi' => 'alamat_provinsi',
            'Alat Transportasi' => 'alat_transportasi',
            'Jenis Tinggal' => 'jenis_tinggal',
            'No Telp Rumah' => 'no_telp_rumah',
            'No Hp' => 'no_hp',
            'No Hp Orang Tua' => 'no_hp_ortu',
            // 'Email' => 'email',
            'Penerima KPS' => 'kps',
            'No Kps' => 'no_kps',
            'Nama Ayah' => 'ayah_nama',
            'Tahun Lahir' => 'ayah_tahun_lahir',
            'Berkebutuhan Khusus Ayah' => 'ayah_kebutuhan_khusus',
            'Pekerjaan Ayah' => 'ayah_pekerjaan',
            'Pendidikan Ayah' => 'ayah_pendidikan',
            'Penghasilan Ayah' => 'ayah_penghasilan',
            'Nama Ibu' => 'ibu_nama',
            'Berkebutuhan Khusus Ibu' => 'ibu_kebutuhan_khusus',
            'Tahun Lahir Ibu' => 'ibu_tahun_lahir',
            'Pekerjaan Ibu' => 'ibu_pekerjaan',
            'Pendidikan Ibu' => 'ibu_pendidikan',
            'Penghasilan Ibu' => 'ibu_penghasilan',
            'Nama Wali' => 'wali_nama',
            'Berkebutuhan Khusus Wali' => 'wali_kebutuhan_khusus',
            'Tahun Lahir Wali' => 'wali_tahun_lahir',
            'Pekerjaan Wali' => 'wali_pekerjaan',
            'Pendidikan Wali' => 'wali_pendidikan',
            'Penghasilan Wali' => 'wali_penghasilan',
            'Tinggi' => 'tinggi',
            'Berat' => 'berat',
            'Jarak ke Sekolah' => 'jarak_sekolah',
            'Waktu Tempuh ke Sekolah' => 'waktu_sekolah',
            'Jumlah Saudara Kandung' => 'saudara_kandung',
            'Lintas Minat 1' => 'lintas_minat1',
            'Lintas Minat 2' => 'lintas_minat2',
            );

        if ($heading === null) {
            return $siswa_pair;
        } else {
            return $siswa_pair[$heading] ?: null;
        }
    }

    public function get_guru_pair($heading = null)
    {
        $guru_pair = array(
            'Nama'          => 'nama',
            'NIP'           => 'nip',
            'NIY'           => 'niy',
            'Tanggal Lahir' => 'tanggal_lahir',
            );
        if ($heading === null) {
            return $guru_pair;
        } else {
            return $guru_pair[$heading];
        }
    }

    public function get_siswa_opt($field = null, $key = null)
    {
        $this->loader->model('tahun_ajaran');

        $pekerjaan = array('Tidak bekerja', 'Nelayan', 'Petani', 'Peternak', 'PNS/TNI/Polri', 'Karyawan Swasta', 'Pedagang Kecil', 'Pedagang Besar', 'Wiraswasta', 'Wirausaha', 'Buruh Pensiunan', 'Sudah Meninggal', 'Lainnya');
        $pendidikan = array('Tidak sekolah', 'PAUD', 'TK/sederajat', 'Putus SD', 'SD/sederajat', 'SMP/sederajat', 'SMA/sederajat', 'Paket A', 'Paket B', 'Paket C', 'D1', 'D2', 'D3', 'D4', 'S1', 'S2', 'S3', 'Non formal', 'Informal', 'Lainnya');
        $penghasilan = array('Kurang dari Rp. 500.000', 'Rp. 500.000 - Rp. 999.999', 'Rp. 1.000.000 - Rp. 1.999.999', 'Rp. 2.000.000 - Rp. 4.999.999', 'Rp. 5.000.000 - Rp. 20.000.000', 'Lebih dari Rp. 20.000.000');
        $kelas = array();
        $list_all_kelas = $this->kelas_m->get_all_kelas() ?: array();
        foreach ($list_all_kelas as $value) {
            $kelas[$value->id] = $value->kelas;
        }
        $angkatan = array();
        foreach ($this->ta_m->get_all() as $value) {
            $angkatan[$value->tahun_ajaran] = $value->tahun_ajaran;
        }
        $all_kelas_lm = $this->kelas_m->get_all_kelas_lm();
        $kelas_lm_opt = array(null => '-');
        foreach ($all_kelas_lm as $kelas_lm) {
            $kelas_lm_opt[$kelas_lm->id] = $kelas_lm->kelas;
        }

        $siswa_options = array(
            'Angkatan'                 => $angkatan,
            'Kelas'                    => $kelas,
            'Jenis Kelamin'            => array('Laki-Laki', 'Perempuan'),
            'Agama'                    => array('Islam', 'Kristen', 'Katholik', 'Hindu', 'Budha', 'Konghucu'),
            'Berkebutuhan Khusus'      => array('Tidak', 'Netra (A)', 'Rungu (B)', 'Grahita ringan (C)', 'Grahita sedang (C1)', 'Daksa ringan (D)', 'Daksa sedang (D1)', 'Laras (E)', 'Wicara (F)', 'Hiper Aktif (H)', 'Cerdas Istimewa (I)', 'Bakat Istimewa (J)', 'Kesulitan Belajar (K)', 'Narkoba (N)', 'Indigo (O)', 'Down syndrome (P)', 'Autis (Q)'),
            'Alat Transportasi'        => array('Jalan kaki', 'Angkutan umum/bus/pete-pete', 'Mobil/bus antar jemput', 'Kereta api', 'Ojek', 'Andong/bendi/sado/dokra/delman/becak', 'Perau penyebrangan/rakit/getek', 'Kuda', 'Sepeda', 'Sepeda motor', 'Mobil pribadi', 'Lainnya'),
            'Jenis Tinggal'            => array('Bersama Orang tua', 'Wali', 'Kost', 'Asrama', 'Panti Asuhan', 'Lainnya'),
            'Pekerjaan Ayah'           => $pekerjaan,
            'Berkebutuhan Khusus Ayah' => array('Tidak', 'Netra (A)', 'Rungu (B)', 'Grahita ringan (C)', 'Grahita sedang (C1)', 'Daksa ringan (D)', 'Daksa sedang (D1)', 'Laras (E)', 'Wicara (F)', 'Hiper Aktif (H)', 'Cerdas Istimewa (I)', 'Bakat Istimewa (J)', 'Kesulitan Belajar (K)', 'Narkoba (N)', 'Indigo (O)', 'Down syndrome (P)', 'Autis (Q)'),
            'Pendidikan Ayah'          => $pendidikan,
            'Penghasilan Ayah'         => $penghasilan,
            'Pekerjaan Ibu'            => $pekerjaan,
            'Berkebutuhan Khusus Ibu'  => array('Tidak', 'Netra (A)', 'Rungu (B)', 'Grahita ringan (C)', 'Grahita sedang (C1)', 'Daksa ringan (D)', 'Daksa sedang (D1)', 'Laras (E)', 'Wicara (F)', 'Hiper Aktif (H)', 'Cerdas Istimewa (I)', 'Bakat Istimewa (J)', 'Kesulitan Belajar (K)', 'Narkoba (N)', 'Indigo (O)', 'Down syndrome (P)', 'Autis (Q)'),
            'Pendidikan Ibu'           => $pendidikan,
            'Penghasilan Ibu'          => $penghasilan,
            'Pekerjaan Wali'           => $pekerjaan,
            'Berkebutuhan Khusus Wali' => array('Tidak', 'Netra (A)', 'Rungu (B)', 'Grahita ringan (C)', 'Grahita sedang (C1)', 'Daksa ringan (D)', 'Daksa sedang (D1)', 'Laras (E)', 'Wicara (F)', 'Hiper Aktif (H)', 'Cerdas Istimewa (I)', 'Bakat Istimewa (J)', 'Kesulitan Belajar (K)', 'Narkoba (N)', 'Indigo (O)', 'Down syndrome (P)', 'Autis (Q)'),
            'Pendidikan Wali'          => $pendidikan,
            'Penghasilan Wali'         => $penghasilan,
            'Jarak ke Sekolah'         => array('Kurang dari 1 km', 'Lebih dari 1 km'),
            'Waktu Tempuh ke Sekolah'  => array('Kurang dari 60 menit', 'Lebih dari 60 menit'),
            'Penerima KPS'             => array('Tidak', 'Ya'),
            'Lintas Minat 1'             => $kelas_lm_opt,
            'Lintas Minat 2'             => $kelas_lm_opt,
            );

        if ($field !== null) {
            if ($key === null) {
                return $siswa_options[$field];
            } else {
                return $siswa_options[$field][$key];
            }
        } else {
            return $siswa_options;
        }
    }

    public function get_siswa_opt_selected($field = null, $key = null)
    {
        $siswa_opt_selected = array(
            'Angkatan' => $this->get_ta_aktif()->tahun_ajaran
            );

        if ($field !== null) {
            if ($key === null) {
                return isset($siswa_opt_selected[$field]) ? $siswa_opt_selected[$field] : null;
            } else {
                return isset($siswa_opt_selected[$field][$key]) ? $siswa_opt_selected[$field][$key] : null;
            }
        } else {
            return $siswa_opt_selected ?: array();
        }
    }

    public function get_imploded_jadwal($all_jadwal)
    {
        foreach ($all_jadwal as $key_jadwal => $guru_matpel_jadwal) {
            $tmp_jadwal = array();
            foreach ($guru_matpel_jadwal as $key => $jadwal) {
                $jadwal = (array)$jadwal;
                $tmp_jadwal[$jadwal['hari']] = $this->get_hari($jadwal['hari']);
            }
            rsort($tmp_jadwal);
            $all_jadwal[$key_jadwal]['jadwal_hari'] = implode(', ', $tmp_jadwal);
        }

        return $all_jadwal;
    }

    public function get_kehadiran_color($kehadiran)
    {
        // check range kehadiran from config
        $range_kehadiran = explode(',', $this->get_config('range_kehadiran'));

        // add button color correspond with bootstrap btn design
        if ($kehadiran >= $range_kehadiran[0]) {
            $color = 'success';
        } elseif ($kehadiran >= $range_kehadiran[1]) {
            $color = 'warning';
        } elseif ($kehadiran >= 0) {
            $color = 'danger';
        } else {
            $color = 'default';
        }
        return $color;
    }

    public function get_status_hadir_color($status_hadir)
    {
        if ($status_hadir == 0) {
            $color = 'default';
        } elseif ($status_hadir == 1) {
            $color = 'success';
        } elseif ($status_hadir == 2 || $status_hadir == 3) {
            $color = 'warning';
        } elseif ($status_hadir == 4) {
            $color = 'danger';
        } elseif ($status_hadir == 5) {
            $color = 'info';
        } else {
            $color = 'default';
        }
        return $color;
    }

    public function get_jadwal($guru_matpel_id, $hari = null)
    {
        if ( ! isset($this->session->{$guru_matpel_id.'_jadwal'})) {
            $this->loader->model('guru_matpel_jadwal');
            if ($hari !== null) {
                $return_data = $this->guru_matpel_jadwal_m->as_array()->where('guru_matpel_id', $guru_matpel_id)->where('hari', $hari)->order_by('hari', 'ASC')->order_by('begin_hrs', 'ASC')->get() ?: null;
            } else {
                $return_data = $this->guru_matpel_jadwal_m->as_array()->where('guru_matpel_id', $guru_matpel_id)->order_by('hari', 'ASC')->order_by('begin_hrs', 'ASC')->get_all() ?: array();
            }
            $this->session->{$guru_matpel_id.'_jadwal'} = $return_data;
            $this->session->mark_as_flash($guru_matpel_id.'_jadwal');
        } else {
            $return_data = $this->session->{$guru_matpel_id.'_jadwal'};
        }
        return $return_data;
    }

    public function get_root($url = '')
    {
        return $this->root.$url;
    }

    public function get_semester($ganjil_genap = null)
    {
        $semester = array('Genap', "Ganjil");

        if ($ganjil_genap !== null) {
            return $semester[$ganjil_genap];
        } else {
            return $semester;
        }
    }

    public function get_jenis_kelamin($jk_id)
    {
        $jenis_kelamin = array('Laki-Laki', 'Perempuan');

        return $jenis_kelamin[$jk_id];
    }

    public function get_tahun_ajaran($ta_id)
    {
        if ( ! isset($this->session->tmp_ta)) {
            $this->loader->model('tahun_ajaran');
            $tmp_ta = $this->ta_m->get($ta_id);
            $this->session->tmp_ta = $tmp_ta;
            $this->session->mark_as_flash('tmp_ta');
        } else {
            $tmp_ta = $this->session->tmp_ta;
        }
        return $tmp_ta;

        return ;
    }

    public function get_ta_aktif()
    {
        $CI =& get_instance();
        if ( ! isset($this->session->ta_aktif)) {
            $this->loader->model('tahun_ajaran');
            $ta_id = $this->get_config('ta_aktif');
            $ta_aktif = $this->ta_m->get($ta_id);

            if ($ta_aktif) {
                $this->session->ta_aktif = $ta_aktif;
                $this->session->mark_as_flash('ta_aktif');
            } else {
                if ($this->ion_auth->is_admin()) {
                    if (uri_string() !== 'admin/master/ta') {
                        $CI->add_message('Tahun ajaran kosong', true, 2);
                        redirect('admin/master/ta','refresh');
                    }
                } else {
                    show_error('Tahun Ajaran aktif belum di set, silahkan hubungi administrator');
                }
            }
        } else {
            $ta_aktif = $this->session->ta_aktif;
        }
        return $ta_aktif;
    }

    public function get_allowed_angkatan()
    {
        $tmp = explode('/', $this->get_ta_aktif()->tahun_ajaran);
        $available_angkatan = array();
        for ($i=0; $i < 3; $i++) {
            $available_angkatan[] = ($tmp[0].'/'.($tmp[0]+1));
            $tmp[0]--;
        }
        return $available_angkatan;
    }

    public function get_html_prop($key)
    {
        $html_properties = array(
            'button-link-xs' => 'btn btn-xs btn-space btn-link',
            'button-default-xs' => 'btn btn-xs btn-space btn-default',
            'button-primary-xs' => 'btn btn-xs btn-space btn-primary',
            'button-success-xs' => 'btn btn-xs btn-space btn-success',
            'button-danger-xs' => 'btn btn-xs btn-space btn-danger',
            'button-warning-xs' => 'btn btn-xs btn-space btn-warning',
            'button-link-sm' => 'btn btn-sm btn-space btn-link',
            'button-default-sm' => 'btn btn-sm btn-space btn-default',
            'button-primary-sm' => 'btn btn-sm btn-space btn-primary',
            'button-success-sm' => 'btn btn-sm btn-space btn-success',
            'button-danger-sm' => 'btn btn-sm btn-space btn-danger',
            'button-warning-sm' => 'btn btn-sm btn-space btn-warning',
            'button-link' => 'btn btn-space btn-link',
            'button-default' => 'btn btn-space btn-default',
            'button-primary' => 'btn btn-space btn-primary',
            'button-success' => 'btn btn-space btn-success',
            'button-danger' => 'btn btn-space btn-danger',
            'button-warning' => 'btn btn-space btn-warning',
            );
        return $html_properties[$key];
    }

    public function array_sort($array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                break;
                case SORT_DESC:
                    arsort($sortable_array);
                break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }
}
