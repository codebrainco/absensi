<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * ------------------------------------------------------------------------------
 * Manage_Controller Class
 * ------------------------------------------------------------------------------
 *
 * This controller will take care of 2nd branch of 3 level Auth System
 *
 * TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
 * ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
 *
 * TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
 *
 * Function
 * ------------------------------------------------------------------------------
 * detail_absensi = for viewing with many siswa absensi
 *
 */

class Siswa_Controller extends Absensi_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->check_auth(2);


        // load navigation
        $nav = $this->main_nav();
        $profile = $this->session->profile;

        $this->site_root = 'siswa/';
        $this->acc_id = $this->session->user_id;

        $this->add_prop($nav, 'main_nav');
        $this->add_prop($profile, 'profile');

        $this->loader->model('siswa', 'kelas');
    }

    public function get_guru_matpel($guru_matpel_id = null)
    {
        $profile = $this->siswa_m->get($this->session->profile->id);
        $where = array(
            'kelas_id' => $profile->kelas_id,
            'tahun_ajaran_id' => $this->get_ta_aktif()->id,
            );
        if ($guru_matpel_id === null) {
            $result = $this->guru_matpel_filter($this->guru_matpel_m->with_kelas()->with_matpel()->with_guru('fields:nama')->where($where)->get_all());
        } else {
            $result = $this->guru_matpel_filter($this->guru_matpel_m->with_kelas()->with_matpel()->with_guru('fields:nama')->where($where)->get($guru_matpel_id));
        }
        return $result;
    }
}
