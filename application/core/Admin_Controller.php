<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_Controller extends Manage_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->check_auth(1);

        $this->site_root = 'admin/';

        $profile = $this->session->profile;
        $nav = $this->main_nav();

        $this->add_prop($profile, 'profile');
        $this->add_prop($nav, 'main_nav');
    }

    /**
     * Navigation array, navigation structure :
     * Single Item :
     *  array(
     *  'fa_icon' => [fontawesome icon_name without fa-],
     *  'link'    => [url for href]
     *  'html'    => [text to show]
     *  )
     * Dropdown Item :
     *  array(
     *  'fa_icon'  => [fontawesome icon_name without fa-],
     *  'html'     => [text to show]
     *  'dropdown' => [array of single item]
     *  )
     * @return array[] of navigational item
     */
    public function main_nav()
    {
        $nav_items = array(
            array('fa_icon' => 'tachometer', 'link' => base_url('admin/dashboard/'), 'html' => 'Dashboard'),
            array('fa_icon' => 'users', 'id' => 'account', 'html' => 'Data Akun', 'dropdown' => array(
                array('link' => base_url('admin/account/siswa'), 'html' => 'Siswa'),
                array('link' => base_url('admin/account/guru'), 'html' => 'Guru'),
                array('link' => base_url('admin/account/bulk_add'), 'html' => 'Bulk Add'),
                )
            ),
            array('fa_icon' => 'book', 'id' => 'kbm', 'html' => 'Data KBM', 'dropdown' => array(
                array('link' => base_url('admin/kbm/guru_matpel/'), 'html' => 'Guru-Mapel'),
                array('link' => base_url('admin/kbm/guru_matpel_lm/'), 'html' => 'Guru-Mapel Lintas Minat'),
                )
            ),
            array('fa_icon' => 'list', 'id' => 'daftar', 'html' => 'Data Master', 'dropdown' => array(
                array('link' => base_url('admin/master/guru_piket/'), 'html' => 'Guru Piket'),
                array('link' => base_url('admin/master/matpel/'), 'html' => 'Mata Pelajaran'),
                array('link' => base_url('admin/master/jurusan/'), 'html' => 'Jurusan'),
                array('link' => base_url('admin/master/kelas/'), 'html' => 'Kelas'),
                array('link' => base_url('admin/master/kelas_lm/'), 'html' => 'Kelas Lintas Minat'),
                array('link' => base_url('admin/master/ta/'), 'html' => 'Tahun Akademik'),
                array('link' => base_url('admin/master/libur/'), 'html' => 'Hari Libur'),
                )
            ),
            array('fa_icon' => 'file-text-o', 'id' => 'report', 'html' => 'Laporan', 'dropdown' => array(
                array('link' => base_url('admin/report/guru'), 'html' => 'Kehadiran Guru Mengajar'),
                array('link' => base_url('admin/report/siswa'), 'html' => 'Kehadiran Siswa'),
                )
            ),
            array('fa_icon' => 'calendar', 'link' => base_url('admin/jadwal_pelajaran/'), 'html' => 'Jadwal Pelajaran'),
            array('fa_icon' => 'gear', 'link' => base_url('admin/config'), 'html' => 'Config'),
            array('fa_icon' => 'trash', 'link' => base_url('admin/purge'), 'html' => 'Purge'),
        );

        return $nav_items;
    }

    public function get_guru_matpel_all($guru_matpel_id = null, $filter = null)
    {
        $this->loader->model('kelas');
        if ($guru_matpel_id === null) {
            $tmp = $this->guru_matpel_m->get_guru_matpel($filter);
            $result = $this->guru_matpel_filter($tmp);
        } else {
            $result = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas('order_by:kelas,asc')->with_guru('fields:nama|order_by:nama,asc')->order_by('tahun_ajaran_id')->get($guru_matpel_id));
        }

        return $result;
    }

    /**
     * Get Matpel based on guru_matpel_id
     *
     * @return object[]|array[] of Matpel|or empty array
     */
    public function get_guru_matpel($guru_matpel_id = null, $filter = null)
    {
        $this->loader->model('kelas');
        $list_all_kelas_non_lm = $this->kelas_m->kelas_non_lm();
        $kelas_non_lm_arr = array();
        foreach ($list_all_kelas_non_lm as $kelas) {
            $kelas_non_lm_arr[] = $kelas->id;
        }
        if (empty($kelas_non_lm_arr)) {
            $kelas_non_lm_arr = null;
        }
        if ($guru_matpel_id === null) {
            if ($filter === null) {
                $tmp = $this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_non_lm_arr)->get_all();
            } else {
                $tmp = $this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_non_lm_arr)->where($filter)->get_all();
            }
            $result = $this->guru_matpel_filter($tmp);
        } else {
            $result = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas('order_by:kelas,asc')->with_guru('fields:nama|order_by:nama,asc')->order_by('tahun_ajaran_id')->get($guru_matpel_id));
        }

        return $result;
    }

    public function get_guru_matpel_lm($guru_matpel_id = null, $filter = null)
    {
        $this->loader->model('kelas');
        $all_kelas_lm = $this->kelas_m->get_all_kelas_lm();
        $kelas_lm = array_column($all_kelas_lm, 'id');
        if (empty($kelas_lm)) {
            $kelas_lm = null;
        }
        if ($guru_matpel_id === null) {
            if ($filter === null) {
                $tmp = $this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_lm)->get_all();
            } else {
                $tmp = $this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_lm)->where($filter)->get_all();
            }
            $result = $this->guru_matpel_filter($tmp);
        } else {
            $result = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas('order_by:kelas,asc')->with_guru('fields:nama|order_by:nama,asc')->order_by('tahun_ajaran_id')->get($guru_matpel_id));
        }

        return $result;
    }
}
