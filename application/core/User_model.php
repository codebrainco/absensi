<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends Base_model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function edit_data($acc_id, $post_data)
    {
        $this->where('account_id', $acc_id)->update($post_data);
    }

    public function get_all_acc($fields)
    {
        return $this->fields($fields)->with_kelas()->with_account("fields:id, username, email")->get_all() ?: array();
    }

    public function get_filtered($fields, $filter)
    {
        if (is_array($filter)) {
            $tmp = $this->with_account();
            foreach ($filter as $field => $filter_data) {
                $tmp->where($field, $filter_data);
            }
            return $tmp->fields($fields)->with_kelas()->with_account("fields:id, username, email")->get_all() ?: array();
        } else {
            return $this->fields($fields)->with_kelas()->with_account("fields:id, username, email")->where($filter)->get_all() ?: array();
        }
    }

    public function get_one($acc_id = null)
    {
        if ($acc_id === null) {
            $acc_id = $this->session->user_id;
        }
        return $this->with_account("fields:id, username, email")->where('account_id', $acc_id)->get() ?: null;
    }

    public function get_profile($acc_id = null)
    {
        return $this->fields('id, nama')->where('account_id', $acc_id)->get() ?: null;
    }
}
