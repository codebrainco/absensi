<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * ------------------------------------------------------------------------------
 * Manage_Controller Class
 * ------------------------------------------------------------------------------
 *
 * This controller will take care of 2nd branch of 3 level Auth System
 *
 * TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
 * ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
 *
 * TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
 *
 * Function
 * ------------------------------------------------------------------------------
 * detail_absensi = for viewing with many siswa absensi
 *
 */

class Manage_Controller extends Absensi_Controller
{
    /**
     * @todo Change all to handle form input here;
     * @todo change to pendik and tendik
     * @todo Tambah tendik di siswa
     * @todo Based on account aj@a jangan siswa
     * @todo toggle status kehadiran pake noscript
     * @todo Analisa public dan protected
     */

    public function __construct()
    {
        parent::__construct();
    }

    // ---------------------------------------------------------------------------

    /**
     * Detail Absensi
     *
     * @see Guru::get_guru_matpel()
     * @see Admin::get_guru_matpel()
     * @see Absensi_Controller::generate_tanggal()
     * @see Absensi_Controller::generate_absensi()
     * @param  int $guru_matpel_id
     * @return void
     */
    public function detail_absensi($guru_matpel_id, $lm = false)
    {
        $this->loader->model('disabled_date', 'tahun_ajaran', 'absensi_kbm', 'siswa');

        // fetch common itmes
        $guru_matpel = $this->get_guru_matpel($guru_matpel_id);

        if (empty($guru_matpel)) {
            show_error('Invalid Guru Matpel', 403, 'Invalid');
        }

        $lm = $this->kelas_m->is_lm($guru_matpel->kelas_id);

        // get periode from GET but set default with begining of semester (January/July)
        $periode = $this->input->get('periode');
        if (isset($periode)) {
            // check if periode is outside range
            if ($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0) {
                ($periode >= 1 && $periode <= 6) or show_error('Invalid Periode', 403, 'Invalid');
            }
            else {
                ($periode >= 7 && $periode <= 12) or show_error('Invalid Periode', 403, 'Invalid');
            }
        }
        else {
            $periode = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        }

        $tmp_abs = $this->abs_kbm_m->get_absensi_all($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->tahun_ajaran, null, $guru_matpel_id);

        $available_angkatan = $this->get_allowed_angkatan();

        $lm = $this->kelas_m->is_lm($guru_matpel->kelas_id);

        if ($lm) {
            $all_siswa = $this->siswa_m->get_siswa_lm($guru_matpel->kelas_id, $available_angkatan);
        }
        else {
            $all_siswa = $this->siswa_m->with_account()->where('angkatan', $available_angkatan)->where('kelas_id', $guru_matpel->kelas_id)->get_all() ?: array();
        }

        $all_properties = array();
        $tmp_siswa_absensi = $this->get_absensi_kbm(null, $guru_matpel->id);
        $all_siswa_absensi = array();
        foreach ($tmp_siswa_absensi as $value) {
            $all_siswa_absensi[$value->account_id] = $value;
        }
        $total_pertemuan = $this->count_pertemuan($guru_matpel);
        foreach ($all_siswa as $key => $siswa) {
            // $siswa_absensi = $this->get_absensi_kbm($siswa->account_id, $guru_matpel->id);
            $all_properties['persentase'][$siswa->account_id] = isset($all_siswa_absensi[$siswa->account_id]) ? round($all_siswa_absensi[$siswa->account_id]->kehadiran / $total_pertemuan * 100) : 0;

            foreach ($this->get_status_hadir() as $key => $value) {
                if (isset($all_siswa_absensi[$siswa->account_id])) {
                    $all_properties[$value][$siswa->account_id] = $all_siswa_absensi[$siswa->account_id]->{strtolower($value)};
                }
                else {
                    $all_properties[$value][$siswa->account_id] = 0;
                }
            }
        }

        // build tanggal and absensi
        $all_dates = $this->generate_tanggal($guru_matpel, $periode);
        $all_absensi = $this->generate_absensi($tmp_abs);

        $guru_matpel->jadwal = $this->get_jadwal($guru_matpel_id);
        foreach ($guru_matpel->jadwal as $key => $value) {
            $guru_matpel->jadwal[$key]['hari'] = $this->get_hari($guru_matpel->jadwal[$key]['hari']);
            $guru_matpel->jadwal[$key]['begin_hrs'] = substr($guru_matpel->jadwal[$key]['begin_hrs'], 0, 5);
            $guru_matpel->jadwal[$key]['end_hrs'] = substr($guru_matpel->jadwal[$key]['end_hrs'], 0, 5);
        }

        // fetch bulan, if semester is Ganjil start from July to end, January otherwise
        $begin = $this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->semester == 0 ? 1 : 7;
        $bulan_opt = array();
        for ($i=$begin; $i < $begin+6; $i++) {
            $bulan_opt[$i] = $this->get_bulan_eng($i);
        }
        $available_bulan = form_dropdown('periode', $bulan_opt, $periode, array('class' => 'form-control'));

        $tahun = $this->slice_tanggal($this->get_tahun_ajaran($guru_matpel->tahun_ajaran_id)->begin_date)->tahun;

        // publish section
        $this->set_title('Detail Absensi');
        $this->add_prop($all_absensi, 'absensi');
        $this->add_prop($all_properties, 'all_properties');
        $this->add_prop($all_siswa, 'all_siswa');
        $this->add_prop($available_bulan, 'available_bulan');
        $this->add_prop($all_dates['date'], 'all_dates');
        $this->add_prop($tahun, 'tahun');
        $this->add_prop($all_dates['day'], 'all_day');
        $this->add_prop($guru_matpel, 'guru_matpel');
        $this->compile_page('public/guru-badge', 'guru_badge');
        $this->compile_page('public/detail_absensi');
        $this->publish();
    }
}
