<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * -------------------------------------------------------------------------
 * Base_Controller Class
 * -------------------------------------------------------------------------
 *1f Basic Controller
 *
 * TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
 * ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
 *
 * Class var
 * -------------------------------------------------------------------------
 * $site_root          = Contain root this controller root directory
 * $view_data          = Data that will be passed to view
 * $counter            = Counter
 *
 * Function
 * -------------------------------------------------------------------------
 * set_title              = Set site title
 * add_message            = Assign message to view_data
 * clear_message          = Clear message array
 * add_prop               = Assign another key beside view_data and message
 * prep_input             = Prepare input to be passed to model
 * prep_input_obj         = Prepare input to be passed to model
 * compile_page           = Compile the page and pass view_data
 * publish                = Publish using template engine
 * get_counter            = Return current state of counter
 * counter_init           = Initialize the counter
 * counter_inc            = Increment then return value of counter
 * counter_dec            = Decrement then return value of counter
 * logout                 = Logout user using ion_auth
 * check_auth        = Check if user is logged in
 * is_admin               = Check if user is admin
 * get_account_group      = Get account group
 * get_account_group_name = Get account group name
 * get_site_root          = Get site root
 */

class Base_Controller extends CI_Controller
{
    /**
     * Contain site root based on controller architecture folder with base_url()
     * @var string
     */
    public $site_root;

    /**
     * Contain all data to be passed without using original template engine
     * @var object
     */
    public $view_data;

    /**
     * For counter
     * @var int
     */
    public $counter;

    public function __construct()
    {
        parent::__construct();
         // Basic Load
        $this->load->database();
        $this->load->library('ion_auth');
        $this->load->library('template');
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->helper('utility');

        if ( ! class_exists('template')) {
            die('Please Load Template Libraries');
        }

         // Variable Initialization
        $this->site_root = '/';
        $this->view_data = new StdClass();
        $this->view_data->messages = array();
    }

    // ---------------------------------------------------------------------------

    public function set_title($title, $subtitle = null)
    {
        $this->template->title = $title;
        $this->template->subtitle = $subtitle;
    }

    public function set_breadcrumbs()
    {
        if (func_num_args() >= 1) {
            $breadcrumbs = func_get_args();

            if (is_array($breadcrumbs[0])) {
                $breadcrumbs = $breadcrumbs[0];
            }

            $this->view_data->breadcrumbs = $breadcrumbs;
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * For adding message to array of message that will be
     * passed to the template engine
     *
     * @param String $message       Message to be shown
     * @param String $as_flashdata  If need to be passed as flashdata
     * @param String $title         Message title optional to be shown
     * @return void
     */
    public function add_message($message, $as_flashdata = false, $severity = 0, $severity_message = null)
    {
        if ( ! empty($message)) {
            if ($as_flashdata === true) {
                $message_arr = array(
                    "severity_message" => $severity_message,
                    "severity" => $severity,
                    "message_body" => $message,
                    );
                $id = (string)md5(rand(100, 999).now());
                $_SESSION['message']['msg_'.$id] = $message_arr;
                $this->session->mark_as_flash('message');
            } else {
                $severity_color = array('info', 'success', 'warning', 'danger');
                $severity_message_array = array('Heads Up!', 'Well Done!', 'Warning!', 'Oh snap!');
                $severity_message = ($severity_message === null) ? $severity_message_array[$severity] : $severity_message;
                $message_arr = array(
                    "severity_message" => $severity_message,
                    "severity_color" => $severity_color[$severity],
                    "message_body" => $message,
                    );
                $this->view_data->messages[] = $message_arr;
            }
        }
    }


    // ---------------------------------------------------------------------------

    /**
     * For clearing message array
     *
     * @return void
     */

    public function clear_message()
    {
        $this->view_data->message = array();
    }

    // ---------------------------------------------------------------------------

    /**
     * Adding key properties to view data
     *
     * @param mixed $prop_value     properties value
     * @param String $prop_name     properties name
     * @return void
     */
    public function add_prop($prop_value, $prop_name)
    {
        if (is_array($prop_value) === true) {
            $this->view_data->$prop_name = array();
        }

        if (is_object($prop_value) === true) {
            $this->view_data->$prop_name = new stdClass();
        }

        $this->view_data->$prop_name = $prop_value;
    }

    // ---------------------------------------------------------------------------

    /**
     * Prepare input to be passed to Model, the form 'name' must be same as param
     *
     * @param string dynamic Catch dynamic parameter of form 'name', if array, set
     * it as the 'dynamic parameter'
     * @return array populated input data
     */
    public function prep_input()
    {
        if (func_num_args() >= 1) {
            $input_set = func_get_args();

            if (is_array($input_set[0])) {
                $input_set = $input_set[0];
            }

            foreach ($input_set as $input) {
                if ($this->input->post($input) !== null) {
                    $passed_data[$input] = $this->input->post($input);
                } else {
                    $passed_data[$input] = null;
                }
            }

            return $passed_data;
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * Same as prep_input, return object
     *
     * @param string dynamic Catch dynamic parameter
     * @return object populated input data
     */
    public function prep_input_obj()
    {
        if (func_num_args() >= 1) {
            $input_set = func_get_args();

            if (is_array($input_set[0])) {
                $input_set = $input_set[0];
            }

            $passed_data = new stdClass();
            foreach ($input_set as $input) {
                if ($this->input->post($input) !== null) {
                    $passed_data->$input = $this->input->post($input);
                } else {
                    $passed_data->$input = null;
                }
            }

            return $passed_data;
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * Render page and pass view_data
     *
     * @param  String $page     filename to render
     * @param  String $to       optional filename to be rendered as
     * @return void
     */
    public function compile_page($page, $to = null)
    {
        $to = ($to === null ? 'content' : $to);
        $this->template->$to->view($page, $this->view_data);
    }

    // ---------------------------------------------------------------------------

    /**
     * Append page and pass view_data
     *
     * @param  String $page     filename to render
     * @param  String $to       optional filename to be rendered as
     * @return void
     */
    public function append_page($page, $to = null)
    {
        $to = ($to === null ? 'content' : $to);
        $this->template->$to->view($page, $this->view_data, $overwrite = false);
    }

    /**
     * Append var and pass view_data
     *
     * @param  String $page     filename to render
     * @param  String $to       optional filename to be rendered as
     * @return void
     */
    public function append_var($var, $to = null)
    {
        $to = ($to === null ? 'content' : $to);
        $this->template->$to = $var;
    }

    // ---------------------------------------------------------------------------

    /**
     * Publish template
     *
     * @return void
     */
    public function publish()
    {
        $this->template->publish();
    }

    // ---------------------------------------------------------------------------

    /**
     * Return counter state
     *
     * @return int
     */
    public function get_counter()
    {
        return $this->counter;
    }

    // ---------------------------------------------------------------------------

    /**
     * Initialize counter
     *
     * @param  int $initial_val Initial value, default 0
     * @return void
     */
    public function counter_init($initial_val = null)
    {
        if ($initial_val !== null) {
            $this->counter = $initial_val;
        } else {
            $this->counter = 0;
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * Increment counter variable
     *
     * @param  int $inc_by Optional, increment by int, default 1
     * @return int         counter state after incremented
     */
    public function counter_inc($inc_by = null)
    {
        if ($inc_by !== null) {
            $this->counter += $inc_by;
        } else {
            $this->counter += 1;
        }
        return $this->counter;
    }

    // ---------------------------------------------------------------------------

    /**
     * Decrement counter properties
     *
     * @param  int $dec_by Optinal, decrement by int, default 1
     * @return int         counter state after decremented
     */
    public function counter_dec($dec_by = null)
    {
        if ($inc_by !== null) {
            $this->counter += $inc_by;
        } else {
            $this->counter -= 1;
        }
        return $this->counter;
    }

    // ---------------------------------------------------------------------------

    /**
     * Logout the user
     *
     * @return void
     */
    public function logout()
    {
        $this->ion_auth->logout();
        $this->session->sess_destroy();
        $this->cache->clean();
        redirect('/');
    }

    // ---------------------------------------------------------------------------

    /**
     * Check if user logged in or not and if user in some group
     *
     * @param  int $group in what group this user exist
     */
    public function check_auth($group, $return = '/')
    {
        if (( ! $this->ion_auth->logged_in()) || ( ! $this->ion_auth->in_group($group))) {
            redirect($return);
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * Shorthand for ion_auth->is_admin
     *
     * @return boolean true if admin, false if not
     */
    public function is_admin()
    {
        if ($this->ion_auth->is_admin() && $this->uri->segment(2) == 'admin') {
            return true;
        } else {
            return false;
        }
    }

    // ---------------------------------------------------------------------------

    /**
     * Get the account group of someone, beside of admin
     *
     * @return int|bool group number or false
     */
    public function get_account_group()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

        if ( ! $group_id = $this->cache->get('group_id'))
        {
            $i = 2;
            while ($this->ion_auth->group($i) !== null && $i <= 3) {
                if ($this->ion_auth->in_group($i)) {
                    $this->cache->save('group_id', $i, 300);
                    return $i;
                }
                $i++;
            }
        } else {
            return $group_id;
        }
        return false;
    }

    // ---------------------------------------------------------------------------

    /**
     * Get the account group name of someone, beside of admin
     *
     * @return string group number or null
     */
    public function get_account_group_name()
    {
        $this->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file'));

        if ( ! $group_name = $this->cache->get('group_name')) {
            $group_name = $this->ion_auth->group($this->get_account_group())->row()->name;
            $this->cache->save('group_name', $group_name, 300);
        }
        return $group_name;
    }

    // ---------------------------------------------------------------------------

    /**
     * Get the site root
     *
     * @param string $url url if exist
     * @return string of site root
     */
    public function get_site_root($url = '')
    {
        return $this->site_root.$url;
    }
}
