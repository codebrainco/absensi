<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * ------------------------------------------------------------------------------
 * Manage_Controller Class
 * ------------------------------------------------------------------------------
 *
 * This controller will take care of 2nd branch of 3 level Auth System
 *
 * TO BE NOTED, DOCUMENTATION ALL FUNCTION BELOW ITS NOT FULLY DOCUMENTED
 * ALL FUNCTION HERE IS A SIMPLE BRIEF ABOUT THAT FUNCTION.
 *
 * TO BE NOTED, ALL MODEL SHOULD BE FOLLOWED BY _m FOR CONVENIENT PURPOSE.
 *
 * Function
 * ------------------------------------------------------------------------------
 * detail_absensi = for viewing with many siswa absensi
 *
 */

class Guru_Controller extends Manage_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->check_auth(3);


        // load navigation
        $nav = $this->main_nav();
        $profile = $this->session->profile;

        $this->site_root = 'guru/';
        $this->acc_id = $this->session->user_id;

        $this->add_prop($nav, 'main_nav');
        $this->add_prop($profile, 'profile');
    }

    public function main_nav_extends(&$nav_item)
    {
        $this->loader->model('guru_piket_jadwal');
        $group_name = $this->get_account_group_name();

        if ($this->gpiket_jdw_m->is_guru_piket($this->session->user_id)) {
            $man_kbm = array('fa_icon' => 'slack', 'link' => base_url($group_name.'/manage/piket'), 'html' => 'Guru Piket');
            array_push($nav_item, $man_kbm);
        }

        // special for guru it already have mata pelajaran for absensi
        $man_kbm = array('fa_icon' => 'slack', 'link' => base_url($group_name.'/manage/absen'), 'html' => 'Manage Absensi');
        array_push($nav_item, $man_kbm);

        $man_kbm = array('fa_icon' => 'slack', 'link' => base_url($group_name.'/manage/absen_lm'), 'html' => 'Manage Absensi Lintas Minat');
        array_push($nav_item, $man_kbm);
    }

    public function get_guru_matpel($guru_matpel_id = null, $filter = null)
    {
        $this->loader->model('guru_matpel', 'kelas');
        $all_kelas_non_lm = $this->kelas_m->kelas_non_lm();
        $kelas_non_lm = array_column($all_kelas_non_lm, 'id');
        if (empty($kelas_non_lm)) {
            $kelas_non_lm = null;
        }
        if ($guru_matpel_id === null) {
            // filter for better data
            $tmp = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_non_lm)->where('account_id', $this->session->user_id)->get_all() ?: array());
            $result = array();
            foreach ($tmp as $value) {
                if ($value->tahun_ajaran_id == $this->get_ta_aktif()->id) {
                    $result[] = $value;
                }
            }
        } else {
            $result = new stdClass();
            $tmp = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_non_lm)->where('account_id', $this->session->user_id)->where('id', $guru_matpel_id)->get() ?: null);
            if ( ! empty($tmp) and $tmp->tahun_ajaran_id == $this->get_ta_aktif()->id) {
                $result = $tmp;
            }
        }
        return $result ?: null;
    }

    public function get_guru_matpel_lm($guru_matpel_id = null, $filter = null)
    {
        $this->loader->model('guru_matpel', 'kelas');
        $all_kelas_lm = $this->kelas_m->get_all_kelas_lm();
        $kelas_lm = array_column($all_kelas_lm, 'id');
        if (empty($kelas_lm)) {
            $kelas_lm = null;
        }
        if ($guru_matpel_id === null) {
            // filter for better data
            $tmp = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_lm)->where('account_id', $this->session->user_id)->get_all() ?: array());
            $result = array();
            foreach ($tmp as $value) {
                if ($value->tahun_ajaran_id == $this->get_ta_aktif()->id) {
                    $result[] = $value;
                }
            }
        } else {
            $result = new stdClass();
            $tmp = $this->guru_matpel_filter($this->guru_matpel_m->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where('kelas_id', $kelas_lm)->where('account_id', $this->session->user_id)->where('id', $guru_matpel_id)->get() ?: null);
            if ( ! empty($tmp) and $tmp->tahun_ajaran_id == $this->get_ta_aktif()->id) {
                $result = $tmp;
            }
        }
        return $result ?: null;
    }
}
