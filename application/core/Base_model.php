<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Base_model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function is_unique($checked_pair, $self_id = null)
    {
        $query = $this->db
            ->from($this->table)
            ->where($checked_pair)
            ->get();
        if ($self_id !== null) {
            $counter = 0;
            foreach ($query->result() as $value) {
                if ($value->id != $self_id) {
                    $counter++;
                }
            }
            if ($counter > 0) {
                return false;
            } else {
                return true;
            }
        } else {
            if ($query->num_rows() >= 1) {
                return false;
            } else {
                return true;
            }
        }
    }
}
