<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Group_model extends MY_Model
{
  public $table = 'account';
  public $primary_key = 'id';

  public $protected_attributes = array('id');

  public function __construct()
  {
    parent::__construct();
    $this->timestamps = false;
  }
}
