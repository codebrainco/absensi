<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Siswa_model extends User_model
{
    public $table = 'siswa';
    public $primary_key = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['account'] = array('Account_model', 'id', 'account_id');
        $this->has_one['kelas'] = array('Kelas_model', 'id', 'kelas_id');
    }

    public function get_except_kelas($kelas_id, $available_angkatan)
    {
        $query = $this->db
             ->select()
             ->from('siswa')
             ->where_not_in('kelas_id', $kelas_id)
             ->where_in('angkatan', $available_angkatan)
             ->get();

        return $query->result();
    }

    public function get_except_kelas_lm($kelas_id, $available_angkatan)
    {
        $query = $this->db
             ->select()
             ->from('siswa')
             ->where_in('angkatan', $available_angkatan)
             ->get();
        return $query->result();
    }

    public function get_siswa_lm($kelas_id, $available_angkatan)
    {
        $query = $this->db
             ->select()
             ->from('siswa')
             ->where_in('angkatan', $available_angkatan)
             ->group_start()
                    ->where('lintas_minat1', $kelas_id)
                    ->or_where('lintas_minat2', $kelas_id)
             ->group_end()
             ->get();

        foreach ($query->result() as $value) {
            $result[] = $value->account_id;
        }
        return $this->with_account()->where('account_id', $result)->get_all() ?: array();
    }

    public function get_by_angkatan($fields, $available_angkatan)
    {
        return $this->with_account("fields:id, username, email")->with_kelas()->fields($fields)->where('angkatan', $available_angkatan)->get_all() ?: array();
    }
}
