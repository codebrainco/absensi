<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Config_model extends MY_Model
{
    public $table = 'config';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function update_config($data)
    {
        $insert_data = array();
        foreach ($data as $prop_name => $prop_value) {
            $insert_data[] = array(
                'prop_name' => $prop_name,
                'prop_value' => $prop_value,
                );
        }
        $this->db->update_batch('config', $insert_data, 'prop_name');
    }
}
