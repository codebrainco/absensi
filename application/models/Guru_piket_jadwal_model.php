<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru_piket_jadwal_model extends Base_model
{
    public $table = 'guru_piket_jadwal';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['guru_matpel'] = array('Guru_matpel_model', 'id', 'guru_matpel_id');
    }

    public function get_this_periode($filter)
    {
        $CI =& get_instance();
        if (isset($filter)) {
            return $this->where($filter)->get_all() ?: array();
        } else {
            return $this->where('tahun_ajaran_id', $CI->get_ta_aktif()->id)->get_all() ?: array();
        }
    }

    public function is_guru_piket($acc_id)
    {
        $CI =& get_instance();
        $data = $this->where('tahun_ajaran_id', $CI->get_ta_aktif()->id)->where('account_id', $acc_id)->get_all() ?: array();
        $today = new DateTime();
        // DEBUG
        foreach ($data as $guru_piket) {
            if ( ! empty($guru_piket)) {
                if ($guru_piket->hari == $today->format('N')) {
                    return true;
                }
            }
        }
        return false;
    }
}
