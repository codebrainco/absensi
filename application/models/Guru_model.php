<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru_model extends User_model
{
    public $table = 'guru';
    public $primary_key = 'id';
    public $protected_attributes = array('id', 'account_id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['account'] = array('Account_model', 'id', 'account_id');
    }

    public function get_all_guru()
    {
        $all_guru = $this->get_all();
        $return_data = array();
        foreach ($all_guru as $guru) {
            $return_data[$guru->account_id] = $guru;
        }
        return $return_data;
    }
}
