<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Berita_acara_model extends Base_model
{
    public $table = 'berita_acara';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['guru_matpel'] = array('Guru_matpel_model', 'id', 'guru_matpel_id');
    }

    public function get_all_ba($guru_matpel_id)
    {
        return $this->where('guru_matpel_id', $guru_matpel_id)->get_all() ?: array();
    }

    public function get_ba($guru_matpel_id, $time)
    {
        $where = array(
            'guru_matpel_id' => $guru_matpel_id,
            'tanggal' => $time[0],
            'hours' => $time[1]
            );
        return $this->where($where)->get() ?: null;
    }

    public function update_ba($post)
    {
        $where = array(
            'guru_matpel_id' => $post['guru_matpel_id'],
            'tanggal' => $post['tanggal'],
            'hours' => $post['hours']
            );
        $this->where($where)->update($post);
    }
}
