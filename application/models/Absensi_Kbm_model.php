<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Absensi_kbm_model extends Base_model
{
    public $table = 'absensi_kbm';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['guru_matpel'] = array('Guru_matpel_model', 'id', 'guru_matpel_id');
    }

    /**
     * Bulk insert absensi
     *
     * @param  object $insert_data contain log per day per account
     * @return true|error              true or error
     */
    public function insert_absensi($insert_data)
    {
        $this->db->set_insert_batch($insert_data);
        if ($this->db->insert_batch('absensi_kbm')) {
            return true;
        } else {
            return $this->db->error();
        }
    }


    /**
     * Bulk update absensi
     *
     * @param  object $update_data contain log per day per account to be udpated
     * @return true|error              true or error
     */
    public function update_absensi($update_data)
    {
        if ($this->db->update_batch('absensi_kbm', $update_data, 'id')) {
            return true;
        } else {
            return $this->db->error();
        }
    }

    /**
     * Bulk insert absensi
     *
     * @param  object $update_data contain log per day per account to be udpated
     * @return true|error              true or error
     */
    public function insert_single($insert_data)
    {
        foreach ($insert_data as $value) {
            $where = array(
                'account_id' => $value['account_id'],
                'guru_matpel_id' => $value['guru_matpel_id'],
                'tanggal' => $value['tanggal'],
                );
            $this->db->set($value);
            $this->db->insert('absensi_kbm');
        }
    }

    /**
     * Bulk update absensi
     *
     * @param  object $update_data contain log per day per account to be udpated
     * @return true|error              true or error
     */
    public function update_single($update_data)
    {
        foreach ($update_data as $value) {
            $where = array(
                'account_id' => $value['account_id'],
                'guru_matpel_id' => $value['guru_matpel_id'],
                'tanggal' => $value['tanggal'],
                );
            $this->db->set($value);
            $this->db->where($where)->update('absensi_kbm');
        }
    }

    /**
     * Count absensi based on account id
     *
     * @param  int $acc_id
     * @return object
     */
    public function count_absensi($acc_id = null, $guru_matpel_id = null)
    {
        $query = $this->db
            ->select('
                absensi_kbm.account_id,
                absensi_kbm.guru_matpel_id,
                absensi_kbm.hadir,
                guru_matpel.kelas_id,
                sum(case when hadir = 1 then 1 else 0 end) as kehadiran,
                sum(case when hadir = 1 then 1 else 0 end) as hadir,
                sum(case when hadir = 2 then 1 else 0 end) as sakit,
                sum(case when hadir = 3 then 1 else 0 end) as izin,
                sum(case when hadir = 4 then 1 else 0 end) as alfa,
                sum(case when hadir = 5 then 1 else 0 end) as terlambat,
                sum(case when hadir = 6 then 1 else 0 end) as absen,
                sum(case when hadir = 0 then 1 else 0 end) as none
                ')
            ->from('absensi_kbm')
            ->join('guru_matpel', 'absensi_kbm.guru_matpel_id = guru_matpel.id', "INNER");

        if ($acc_id !== null) {
            if (is_array($acc_id)) {
                $query->where_in('absensi_kbm.account_id', $acc_id);
            } else {
                $query->where('absensi_kbm.account_id', $acc_id);
            }
        } else {
            $query->group_by('account_id');
        }
        if ($guru_matpel_id !== null && ! empty($guru_matpel_id)) {
            if (is_array($guru_matpel_id)) {
                $query->where_in('absensi_kbm.guru_matpel_id', $guru_matpel_id);
            } else {
                $query->where('absensi_kbm.guru_matpel_id', $guru_matpel_id);
            }
        }

        $result = $query->group_by('guru_matpel_id')
            ->order_by('kelas_id')
            ->get();
        return $result->result();
    }

    public function get_absensi($guru_matpel_id)
    {
        if ( ! isset($this->session->{$guru_matpel_id.'_absen'})) {
            $return_data = $this->where(array('guru_matpel_id' => $guru_matpel_id, 'hadir !=' => '0'))->group_by(array('tanggal', 'hours'))->get_all() ?: array();
            $this->session->{$guru_matpel_id.'_absen'} = $return_data;
            $this->session->mark_as_flash($guru_matpel_id.'_absen');
        } else {
            $return_data = $this->session->{$guru_matpel_id.'_absen'};
        }
        return $return_data;
    }

    /**
     * Get absensi per-bulan based on guru_matpel_id
     *
     * @param  int $tahun          tahun to get
     * @param  int $bulan          bulan to get
     * @param  int $guru_matpel_id guru matpel_id to get
     * @return object                 result
     */
    public function get_absensi_all($tahun, $bulan, $guru_matpel_id, $filter = null)
    {
        $tmp_tahun = explode('/', $tahun);
        $like = $tmp_tahun[0].($bulan !== null ? ('-'.$bulan) : '');

        $query = $this->db
            ->select()
            ->from('absensi_kbm')
            ->where('guru_matpel_id', $guru_matpel_id);

        if ( ! empty($filter)) {
            $query = $query->where_in('account_id', $filter);
        }

        $query = $query
            ->group_start()
                ->like('tanggal', $like)
            ->group_end()
            ->get();

        return $query->result();
    }

    /**
     * Check if a tanggal is present, if yes then all 1 month would be present
     *
     * @param  string $tanggal string of tanggal to check
     * @return bool          true if unique, else otherwise
     */
    public function is_unique_absen($acc_id, $guru_matpel_id, $tanggal)
    {
        $query = $this->db
            ->select('absensi_kbm.tanggal')
            ->from('absensi_kbm')
            ->where('tanggal', $tanggal)
            ->where('guru_matpel_id', $guru_matpel_id)
            ->where('account_id', $acc_id)
            ->get();

        return ($query->num_rows() >= 1 ? false : true);
    }
}
