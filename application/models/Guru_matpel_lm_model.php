<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru_matpel_lm_model extends Base_model
{
  public $table = 'guru_matpel_lm';
  public $primary_key = 'id';
  public $protected_attributes = array('id');

  public function __construct()
  {
    parent::__construct();
    $this->timestamps = false;
    $this->has_one['guru'] = array('Guru_model', 'account_id', 'account_id');
    $this->has_one['kelas_lm'] = array('Kelas_lm_model', 'id', 'kelas_lm_id');
  }

  /**
   * Insert as usual but return inserted id
   *
   * @param  object $data data to be inserted
   * @return int       inserted id
   */
  public function my_insert($data)
  {
    $this->insert($data);

    return $this->db->insert_id();
  }
}
