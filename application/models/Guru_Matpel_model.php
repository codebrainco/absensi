<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru_matpel_model extends Base_model
{
    public $table = 'guru_matpel';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['matpel'] = array('Matpel_model', 'id', 'matpel_id');
        $this->has_one['guru'] = array('Guru_model', 'account_id', 'account_id');
        $this->has_one['kelas'] = array('Kelas_model', 'id', 'kelas_id');
        $this->has_many['jadwal'] = array('Guru_matpel_jadwal_model', 'guru_matpel_id', 'id');
    }

    public function get_lm($acc_id = null, $filter = null)
    {
        $query = $this->db
            ->select('*, guru_matpel.id')
            ->from($this->table)
            ->join('kelas', 'kelas_id = kelas.id')
            ->join('guru', 'guru_matpel.account_id = guru.account_id')
            ->join('matpel', 'matpel_id = matpel.id')
            ->where("kelas.lm = 1");
        if ($acc_id !== null) {
            $query = $query->where('guru_matpel.account_id', $acc_id);
        }
        if ($filter !== null) {
            foreach ($filter as $filter_field => $filter_val) {
                $filter_arr['guru_matpel.'.$filter_field] = $filter_val;
            }
            $query = $query->where($filter_arr);
        }
        return $query->get()->result();
    }

    public function get_normal($guru_matpel_id = null, $filter = null)
    {
        $query = $this->db
            ->select('*, guru_matpel.id')
            ->from($this->table)
            ->join('kelas', 'kelas_id = kelas.id')
            ->join('guru', 'guru_matpel.account_id = guru.account_id')
            ->join('matpel', 'matpel_id = matpel.id')
            ->where("kelas.lm = 0");
        if ($guru_matpel_id !== null) {
            $query = $query->where('guru_matpel.id', $guru_matpel_id);
        }

        if ($filter !== null) {
            $query = $query->where($filter);
        }
        return $query->get()->result();
    }

    public function get_by($field, $filter_data)
    {
        $CI =& get_instance();
        return $this->with_guru()->with_kelas()->with_matpel()->where($field, $filter_data)->where('tahun_ajaran_id', $CI->get_ta_aktif()->id)->get_all() ?: array();
    }

    /**
     * Insert as usual but return inserted id
     *
     * @param  object $data data to be inserted
     * @return int       inserted id
     */
    public function my_insert($data)
    {
        $this->insert($data);

        return $this->db->insert_id();
    }

    public function get_guru_matpel($filter = null)
    {
        if ($filter === null) {
            return $this->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->get_all();
        } else {
            return $this->with_matpel()->with_kelas()->with_guru()->order_by('tahun_ajaran_id')->where($filter)->get_all();
        }
    }
}
