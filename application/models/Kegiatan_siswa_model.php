<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kegiatan_siswa_model extends MY_Model
{
  public $table = 'kegiatan_siswa';
  public $primary_key = 'id';
  public $protected_attributes = array('id');

  public function __construct()
  {
    parent::__construct();
    $this->timestamps = false;
    $this->has_one['kegiatan'] = array('Kegiatan_model', 'id', 'kegiatan_id');
  }
}
