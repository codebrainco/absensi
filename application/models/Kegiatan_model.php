<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kegiatan_model extends MY_Model
{
  public $table = 'kegiatan';
  public $primary_key = 'id';
  public $protected_attributes = array('id');

  public function __construct()
  {
    parent::__construct();
    $this->timestamps = false;
  }
}
