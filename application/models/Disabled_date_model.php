<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Disabled_date_model extends MY_Model
{
    public $table = 'disabled_date';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function get_all_ddate()
    {
        if ( ! isset($this->session->ddate)) {
            $return_data = $this->get_all();
            $this->session->ddate = $return_data;
            $this->session->mark_as_flash('ddate');
        } else {
            $return_data = $this->session->ddate;
        }
        return $return_data ?: array();
    }
}
