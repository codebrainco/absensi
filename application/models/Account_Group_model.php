<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account_Group_model extends MY_Model
{
  public $table = 'account_group';
  public $primary_key = 'id';
  public $protected_attributes = array('id');

  public function __construct()
  {
    parent::__construct();
    $this->timestamps = false;
  }
}
