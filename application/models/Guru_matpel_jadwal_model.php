<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Guru_matpel_jadwal_model extends MY_Model
{
    public $table = 'guru_matpel_jadwal';
    public $primary_key = 'id';

    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    /**
     * Bulk insert jadwal after inserting guru_matpel
     *
     * @param  object $insert_data data to be inseeted
     * @return true|error()              true or error
     */
    public function insert_jadwal($insert_data)
    {
        $this->db->set_insert_batch($insert_data);
        if ($this->db->insert_batch('guru_matpel_jadwal'))
        {
            return true;
        }
        else
        {
            return $this->db->error();
        }
    }

    public function get_all_jadwal($all_guru_matpel_id)
    {
        if ( ! isset($this->session->all_jadwal)) {
            if ( ! empty($all_guru_matpel_id)) {
                $all_jadwal = $this->where('guru_matpel_id', $all_guru_matpel_id)->get_all() ?: array();
            } else {
                $all_jadwal = $this->get_all() ?: array();
            }
            $return_data = array();
            foreach ($all_jadwal as $jadwal) {
                $return_data[$jadwal->guru_matpel_id][] = $jadwal;
            }
            $this->session->all_jadwal = $return_data;
            $this->session->mark_as_flash('all_jadwal');
        } else {
            $return_data = $this->session->all_jadwal;
        }
        return $return_data;
    }
}
