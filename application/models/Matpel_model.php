<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matpel_model extends Base_model
{
    public $table = 'matpel';
    public $primary_key = 'id';

    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }
}
