<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tahun_ajaran_model extends MY_Model
{
    public $table = 'tahun_ajaran';
    public $primary_key = 'id';
    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function get_ta_date($tahun)
    {
        $query = $this->db
            ->select()
            ->like('begin_date', $tahun)
            ->get('tahun_ajaran');

        return $query->row();
    }

    public function get_all_ta()
    {
        $all_ta = $this->order_by('tahun_ajaran')->get_all() ?: array();
        $return_data = array();
        foreach ($all_ta as $ta) {
            $return_data[$ta->id] = $ta;
        }
        return  $return_data;
    }

    public function get_semester($ganjil_genap = null)
    {
        $semester = array('Genap', "Ganjil");

        if ($ganjil_genap !== null) {
            return $semester[$ganjil_genap];
        } else {
            return $semester;
        }
    }

    public function get_all_ta_smt()
    {
        $all_ta = array();
        foreach ($this->get_all_ta() as $ta) {
            $tmp = new stdClass();
            $tmp = $ta;
            $tmp->tahun_ajaran .= ' - '.$this->get_semester($tmp->semester);
            $all_ta[] = $tmp;
        }
        return $all_ta;
    }
}
