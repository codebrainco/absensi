<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas_model extends Base_model
{
    public $table = 'kelas';
    public $primary_key = 'id';

    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
        $this->has_one['siswa'] = array('Siswa_model', 'kelas_id', 'id');
        $this->has_one['siswa_lm1'] = array('Siswa_model', 'lintas_minat1', 'id');
        $this->has_one['siswa_lm2'] = array('Siswa_model', 'lintas_minat2', 'id');
    }

    public function count_siswa_res($query, $kelas_field)
    {
        $result = $query
            ->from('kelas')
            ->where('kelas.id', $this->kelas_id)
            ->join('siswa', 'siswa.'.$kelas_field.' = '.$this->kelas_id, 'INNER')
            ->get();
        return $result;
    }

    public function count_siswa($kelas_id)
    {
        $this->kelas_id = $kelas_id;
        $query = $this->db
            ->select('COUNT(siswa.kelas_id) as jumlah_siswa');
        return $this->count_siswa_res($query, 'kelas_id')->row()->jumlah_siswa;
    }

    public function count_siswa_lm($kelas_id)
    {
        $this->kelas_id = $kelas_id;
        $query1 = $this->db->select('COUNT(siswa.lintas_minat1) as jumlah_siswa');
        $jumlah_siswa1 = $this->count_siswa_res($query1, 'lintas_minat1')->row()->jumlah_siswa ?: 0;

        $query2 = $this->db->select('COUNT(siswa.lintas_minat2) as jumlah_siswa');
        $jumlah_siswa2 = $this->count_siswa_res($query2, 'lintas_minat2')->row()->jumlah_siswa ?: 0;

        return ($jumlah_siswa1+$jumlah_siswa2);
    }

    public function kelas_non_lm()
    {
        $query = $this->db->from($this->table)->where('lm', 0)->get();
        return $query->result();
    }

    public function get_all_kelas($with_count = false)
    {
        $query = $this->order_by('kelas');
        if ($with_count) {
            $query->with_siswa('fields:*count*');
        }
        return $query->where('lm', 0)->get_all() ?: array();
    }

    public function get_all_kelas_lm($with_count = false)
    {
        $query = $this->order_by('kelas');
        if ($with_count) {
            $query->with_siswa_lm1('fields:*count*');
            $query->with_siswa_lm2('fields:*count*');
        }
        return $query->where('lm', 1)->get_all() ?: array();
    }

    public function get_kelas($kelas_id)
    {
        $data = $this->get($kelas_id);
        return ! empty($data) ? $data->kelas : null;
    }

    public function is_lm($kelas_id)
    {
        $kelas = $this->get($kelas_id);
        if ($kelas->lm == 0) {
            return false;
        } else {
            return true;
        }
    }
}
