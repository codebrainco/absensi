<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jurusan_model extends Base_model
{
    public $table = 'jurusan';
    public $primary_key = 'id';

    public $protected_attributes = array('id');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    public function get_jurusan($jurusan_id = null)
    {
        return $this->get($jurusan_id)->jurusan ?: null;
    }

    public function get_all_jurusan()
    {
        $CI =& get_instance();
        $data = $this->get_all();
        $list_all_jurusan = array();
        if ($data) {
            foreach ($data as $value) {
                $list_all_jurusan[$value->id] = $value->jurusan;
            }
        } else {
            if ($this->ion_auth->is_admin()) {
                if (uri_string() !== 'admin/master/jurusan') {
                    $CI->add_message('Jurusan  kosong', true, 2);
                    redirect('admin/master/jurusan','refresh');
                }
            } else {
                show_error('Tahun Ajaran aktif belum di set, silahkan hubungi administrator');
            }
        }

        return $list_all_jurusan ?: array();
    }
}
