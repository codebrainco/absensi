<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Account_model extends Base_Model
{
    public $table = 'account';
    public $primary_key = 'id';
    public $protected_attributes = array('id', 'username');
    public $before_update = array('filter');

    public function __construct()
    {
        parent::__construct();
        $this->timestamps = false;
    }

    /**
     * To filter password for query
     *
     * @param  object $account detail of inserted/updated object
     * @return object          filtered data
     */
    protected function filter($account)
    {
        unset($account->currpass);
        unset($account->passconf);

        return $account;
    }

    public function edit_account($account)
    {
        $CI =& get_instance();
        $this->loader->library('form_validation');

        if ($this->form_validation->run('account_info') === true) {
            $input_data = $CI->prep_input_obj('identity', 'email', 'password', 'passconf', 'currpass');
            $input_data->username = $input_data->identity;
            unset($input_data->identity);

            $account_data = $this->get($account->account_id);

            // check if password is same as database
            if (password_verify($input_data->currpass, $account_data->password)) {
                // is form password and passconf empty ?
                if (empty($input_data->password) || empty($input_data->passconf)) {
                    // unset password so it wont be updated
                    unset($input_data->password);
                } else {
                    // is password and password confirmation match ?
                    if ($input_data->password != $input_data->passconf) {
                        $CI->add_message('Konfirmasi password baru tidak sama', false, 2);
                        return false;
                    } else {
                        // hash the NEW password
                        $input_data->password = password_hash($input_data->password, PASSWORD_DEFAULT);
                    }
                }

                if ( ! $this->is_unique(array('username' => $input_data->username), $account->account_id)) {
                    $CI->add_message('Username tidak unique', false, 2);
                    return false;
                }

                // if it go smoothly, return true and data will be updated
                $this->update($input_data, $account_data->id);

                $CI->add_message('Akun data berhasil di update', true, 1);
                return true;
            } else {
                $CI->add_message('Password salah, harap masukan masukan password dengan benar pada kolom Current Password', false, 2);
                return false;
            }
        } else {
            $CI->add_message(validation_errors(), false, 3);
            return false;
        }
    }

    public function my_insert($input_data)
    {
        $this->ion_auth->register($input_data['identity'], $input_data['password'], $input_data['email'], array(), array($input_data['group']));

        $id = $this->db->select('id')->where('username', $input_data['identity'])->get('account')->row();

        return $id->id;
    }

    public function username_login($credential)
    {
        if ($this->ion_auth->login($credential['identity'], $credential['password']))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function email_login($credential)
    {
        $siswa = $this->where('email', $credential['identity'])->get();
        if ($siswa)
        {
            if ($this->ion_auth->login($siswa->username, $credential['password']))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public function nis_login($credential)
    {
        $siswa = $this->db->from('siswa')->where('nis', $credential['identity'])->get()->row();
        if ($siswa)
        {
            $tanggal_lahir = nice_date($siswa->tanggal_lahir, 'dmY');
            $this->account->update(array('password', password_hash($tanggal_lahir, PASSWORD_DEFAULT)), $siswa->account_id);
            if ($this->ion_auth->login($siswa->username, $credential['password']))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
}
