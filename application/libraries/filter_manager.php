<?php
class Filter_manager
{

    public $filter;

    public function __construct()
    {
        $this->filter = array();
    }

    public function load($index, $sources)
    {
        $this->CI =& get_instance();
        $tmp_options = array();
        $config = $this->config($index);

        if ($config !== null) {
            $tmp_options = array();
            $tmp_options['all'] = $config['placeholder'];
            $selected = $this->CI->input->get();
            if ( ! empty($sources)) {
                foreach ($sources as $source) {
                    $tmp_options[$source->{$config['value']}] = $source->{$config['html']};
                }
            }
            $this->filter[] = form_dropdown($config['name'], $tmp_options, isset($selected[$config['name']]) ? $selected[$config['name']] : $config['default_selected'], array('class' => 'form-control input-sm'));
        }

        return $this;
    }


    public function config($index)
    {
        $this->CI->load->library('loader');

        $this->CI->loader->model('config', 'tahun_ajaran');
        $ta_aktif = $this->CI->get_ta_aktif()->id;

        $config = array(
            'tahun_ajaran' => array(
                'name' => 'tahun_ajaran_id',
                'placeholder' => 'Semua Tahun Akademik',
                'value' => 'id',
                'html' => 'tahun_ajaran',
                'default_selected' => $ta_aktif,
                ),
            'guru' => array(
                'name' => 'account_id',
                'placeholder' => 'Semua Guru',
                'value' => 'account_id',
                'html' => 'nama',
                'default_selected' => '',
                ),
            'kelas' => array(
                'name' => 'kelas_id',
                'placeholder' => 'Semua Kelas',
                'value' => 'id',
                'html' => 'kelas',
                'default_selected' => '',
                ),
            );
        return $config[$index];
    }

    public function get()
    {
        return $this->filter;
    }
}
