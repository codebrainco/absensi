<?php
class Loader
{

    public $filter;

    public function __construct()
    {
        $this->filter = array();
    }

    public function model_config($model_index)
    {
        $config = array(
            'absensi_kbm' => 'abs_kbm_m',
            'account' => 'acc_m',
            'config' => 'config_m',
            'guru_matpel_jadwal' => 'guru_matpel_jadwal_m',
            'guru_matpel' => 'guru_matpel_m',
            'guru_matpel_lm' => 'guru_matpel_lm_m',
            'guru' => 'guru_m',
            'jurusan' => 'jurusan_m',
            'kegiatan_siswa' => 'kegiatan_siswa_m',
            'kelas' => 'kelas_m',
            'matpel' => 'matpel_m',
            'siswa' => 'siswa_m',
            'tahun_ajaran' => 'ta_m',
            'disabled_date' => 'ddate_m',
            'berita_acara' => 'ba_m',
            'guru_piket_jadwal' => 'gpiket_jdw_m',
            );
        return $config[$model_index];
    }

    public function model($models)
    {
        $this->CI =& get_instance();

        if ( ! is_array($models)) {
            $models = func_get_args();
        }

        foreach ($models as $model) {
            $this->CI->load->model($model.'_model', $this->model_config($model));
        }
    }

    public function library($libraries)
    {
        $this->CI =& get_instance();

        if ( ! is_array($libraries)) {
            $libraries = func_get_args();
        }

        foreach ($libraries as $library) {
            $this->CI->load->library($library);
        }
    }

    public function helper($helpers)
    {
        $this->CI =& get_instance();

        if ( ! is_array($helpers)) {
            $helpers = func_get_args();
        }

        foreach ($helpers as $helper) {
            $this->CI->load->helper($helper);
        }
    }
}
