<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller']                    = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override']                          = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes']                  = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

/* Home Controller */
$route['default_controller']                        = 'Home';
$route['login']                                     = 'Home/view_login';
// $route['daftar']                                 = 'Home/view_register';
$route['interface']                                 = 'Home/view_interface';

$route['admin/logout']                              = 'Admin/Account/logout';
$route['siswa/logout']                              = 'Siswa/Dashboard/logout';

/* Admin Controller */
$route['admin']                                     = 'Admin/Dashboard';
$route['admin/dashboard']                           = 'Admin/Dashboard';

$route['admin/make_admin/(:num)']                   = 'Admin/admin_add/$1';
$route['admin/remove_admin/(:num)']                 = 'Admin/admin_remove/$1';

$route['admin/account/guru']                        = 'Admin/Account/guru';
$route['admin/account/guru/edit/(:num)']            = 'Admin/Account/guru_edit/$1';
$route['admin/account/guru/add']                    = 'Admin/Account/guru_edit/0';
$route['admin/account/guru/remove/(:num)']          = 'Admin/Account/guru_remove/$1';

$route['admin/account/siswa']                       = 'Admin/Account/siswa';
$route['admin/account/siswa/edit/(:num)']           = 'Admin/Account/siswa_edit/$1';
$route['admin/account/siswa/add']                   = 'Admin/Account/siswa_edit/0';
$route['admin/account/siswa/remove/(:num)']         = 'Admin/Account/siswa_remove/$1';

$route['admin/account/bulk_add']                    = 'Admin/Bulk_add/account';

$route['admin/attendance/detail/(:num)']            = 'Admin/Report/detail_attendance/$1';
$route['admin/attendance/detail_lm/(:num)']         = 'Admin/Report/detail_attendance/$1/1';

$route['admin/kbm/guru_matpel']                     = 'Admin/Guru_matpel';
$route['admin/kbm/guru_matpel/edit/(:num)']         = 'Admin/Guru_matpel/edit/$1';
$route['admin/kbm/guru_matpel/add']                 = 'Admin/Guru_matpel/edit/0';
$route['admin/kbm/guru_matpel/remove/(:num)']       = 'Admin/Guru_matpel/remove/$1';

$route['admin/kbm/guru_matpel_lm']                  = 'Admin/Guru_matpel/lm';
$route['admin/kbm/guru_matpel_lm/remove/(:num)']    = 'Admin/Guru_matpel/remove/$1/1';
$route['admin/kbm/guru_matpel_lm/edit/(:num)']      = 'Admin/Guru_matpel/lm_edit/$1/1';
$route['admin/kbm/guru_matpel_lm/add']              = 'Admin/Guru_matpel/lm_edit/0';

$route['admin/kbm/jadwal/edit/(:num)']              = 'Admin/Jadwal/edit/$1';
$route['admin/kbm/jadwal_lm/edit/(:num)']           = 'Admin/Jadwal/edit/$1/1';

$route['admin/kbm/ba/edit/(:num)']                  = 'Admin/Berita_acara/index/$1';
$route['admin/kbm/ba/edit/(:num)/(:any)']           = 'Admin/Berita_acara/edit/$1/$2';

$route['admin/jadwal_pelajaran']                    = 'Admin/Jadwal_pelajaran';

$route['admin/kbm/absensi/edit/(:num)']             = 'Admin/Absensi/index/$1';
$route['admin/kbm/absensi_lm/edit/(:num)']          = 'Admin/Absensi/index/$1/1';

$route['admin/master/kelas']                        = 'Admin/Kelas/kelas_normal';
$route['admin/master/kelas/edit/(:num)']            = 'Admin/Kelas/kelas_edit/$1';
$route['admin/master/kelas/add']                    = 'Admin/Kelas/kelas_edit/0';
$route['admin/master/kelas/remove/(:num)']          = 'Admin/Kelas/remove/$1';
$route['admin/master/kelas/siswa/(:num)']           = 'Admin/Kelas/kelas_siswa/$1';

$route['admin/master/kelas_lm']                     = 'Admin/Kelas/kelas_lm';
$route['admin/master/kelas_lm/edit/(:num)']         = 'Admin/Kelas/kelas_lm_edit/$1';
$route['admin/master/kelas_lm/add']                 = 'Admin/Kelas/kelas_lm_edit/0';
$route['admin/master/kelas_lm/remove/(:num)']       = 'Admin/Kelas/remove/$1/1';
$route['admin/master/kelas_lm/siswa/(:num)']        = 'Admin/Kelas/kelas_lm_siswa/$1';

$route['admin/master/jurusan']                      = 'Admin/Jurusan';
$route['admin/master/jurusan/edit/(:num)']          = 'Admin/Jurusan/edit/$1';
$route['admin/master/jurusan/add']                  = 'Admin/Jurusan/edit/0';
$route['admin/master/jurusan/remove/(:num)']        = 'Admin/Jurusan/remove/$1';

$route['admin/master/matpel']                       = 'Admin/Matpel';
$route['admin/master/matpel/edit/(:num)']           = 'Admin/Matpel/edit/$1';
$route['admin/master/matpel/add']                   = 'Admin/Matpel/edit/0';
$route['admin/master/matpel/remove/(:num)']         = 'Admin/Matpel/remove/$1';

$route['admin/master/libur']                        = 'Admin/Libur';
$route['admin/master/libur/edit/(:num)']            = 'Admin/Libur/edit/$1';
$route['admin/master/libur/add']                    = 'Admin/Libur/edit/0';
$route['admin/master/libur/remove/(:num)']          = 'Admin/Libur/remove/$1';

$route['admin/master/ta']                           = 'Admin/Tahun_ajaran';
$route['admin/master/ta/edit/(:num)']               = 'Admin/Tahun_ajaran/edit/$1';
$route['admin/master/ta/add']                       = 'Admin/Tahun_ajaran/edit/0';
$route['admin/master/ta/remove/(:num)']             = 'Admin/Tahun_ajaran/remove/$1';
$route['admin/master/ta/activate/(:num)']           = 'Admin/Tahun_ajaran/activate/$1';

$route['admin/master/guru_piket']                   = 'Admin/Guru_piket';
$route['admin/master/guru_piket/add']               = 'Admin/Guru_piket/edit/0';
$route['admin/master/guru_piket/remove/(:num)']     = 'Admin/Guru_piket/remove/$1';

$route['admin/report/guru']                         = 'Admin/Report/guru_mengajar';
$route['admin/report/guru/detail/(:num)']           = 'Admin/Report/kehadiran_guru_mengajar/$1';
$route['admin/report/siswa']                        = 'Admin/Report/siswa';
$route['admin/report/siswa/detail/(:num)']          = 'Admin/Report/siswa_detail/$1';
$route['admin/report/siswa/detail/(:num)/(:num)']   = 'Admin/Report/siswa_ba/$1/$2';

$route['admin/config']                              = 'Admin/Config';

$route['admin/purge']                               = 'Admin/Purge';

/* Guru Controller */
$route['guru']                                      = 'Guru/Dashboard';
$route['guru/dashboard']                            = 'Guru/Dashboard';
$route['guru/profile']                              = 'Guru/Profile';
$route['guru/logout']                               = 'Guru/Profile/Logout';

$route['guru/report/attendance']                    = 'Guru/Report/attendance';
$route['guru/report/attendance/detail/(:num)']      = 'Guru/Report/detail_attendance/$1';

// $route['guru/manage']                            = 'Guru/view_manage';
$route['guru/manage/absen']                         = 'Guru/Absen';
$route['guru/manage/absen_lm']                      = 'Guru/Absen/index/1';
$route['guru/manage/absen/(:num)']                  = 'Guru/Absen/absensi/$1';
$route['guru/manage/absen_lm/(:num)']               = 'Guru/Absen/absensi/$1/1';

$route['guru/manage/piket']                         = 'Guru/Guru_piket';
$route['guru/manage/piket/(:num)']                  = 'Guru/Guru_piket/edit/$1';


/* Siswa Controller */
$route['siswa']                                     = 'Siswa/Dashboard';
$route['siswa/dashboard']                           = 'Siswa/Dashboard';
$route['siswa/profile']                             = 'Siswa/Profile';
$route['siswa/report/attendance']                   = 'Siswa/Report/attendance';
$route['siswa/report/attendance/detail/(:num)']     = 'Siswa/Report/detail_attendance/$1';
// $route['siswa/manage']                           = 'Siswa/view_manage';

/* AJAX */
// $route['public/ajax/check_kelas/(:num)']         = 'Public_Controller/check_kelas/$1';
// $route['public/ajax/check_matpel/(:any)/(:num)'] = 'Public_Controller/check_matpel/$1/$2';
// $route['public/ajax/check_tahun/(:any)']         = 'Public_Controller/check_tahun/$1';
// $route['public/ajax/check_bulan/(:any)/(:any)']  = 'Public_Controller/check_bulan/$1/$2';
// $route['public/ajax/update_absen']               = 'Admin/bulk_update_absen';
$route['public/ajax/update_absen']                  = 'Public_Controller/bulk_update_absen';

$route['404_override']                              = '';
$route['translate_uri_dashes']                      = false;
