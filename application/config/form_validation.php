<?php
$config = array(
    'oneTime' => array(
        array(
            'field' => 'oneTime',
            'label' => 'Tanggal',
            'rules' => 'required',
            )
        ),
    'mingguan' => array(
        array(
            'field' => 'mingguan[]',
            'label' => 'Hari',
            'rules' => 'required',
            )
        ),
    'bulanan' => array(
        array(
            'field' => 'bulanan',
            'label' => 'Tanggal',
            'rules' => 'required',
            )
        ),
    'absensi' => array(
        array(
            'field' => 'kelas',
            'label' => 'Kelas',
            'rules' => 'required',
            ),
        array(
            'field' => 'guru_matpel_id',
            'label' => 'Matpel',
            'rules' => 'required',
            ),
        array(
            'field' => 'tahun',
            'label' => 'Tahun',
            'rules' => 'required',
            ),
        array(
            'field' => 'bulan',
            'label' => 'Bulan',
            'rules' => 'required',
            ),
        ),
    'account_info' => array(
        array(
            'field' => 'identity',
            'label' => 'Username',
            'rules' => 'required',
            ),
        array(
            'field' => 'email',
            'label' => 'E-Mail',
            'rules' => 'required|valid_email',
            ),
        ),
    'guru_info' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required|alpha_numeric_spaces',
        ),
        ),
    'siswa_info' => array(
        array(
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'required',
        ),
        array(
            'field' => 'nis',
            'label' => 'NIS',
            'rules' => 'required|integer',
        ),
        array(
            'field' => 'nisn',
            'label' => 'NISN',
            'rules' => 'integer',
        ),
        array(
            'field' => 'no_ijazah_smp',
            'label' => 'No Ijazah SMP',
            'rules' => 'integer',
        ),
        array(
            'field' => 'no_seri_skhun',
            'label' => 'No Seri SKHUN',
            'rules' => 'integer',
        ),
        array(
            'field' => 'nik',
            'label' => 'NIK',
            'rules' => 'integer',
        ),
        array(
            'field' => 'alamat_kode_pos',
            'label' => 'Kode Pos',
            'rules' => 'integer',
        ),
        array(
            'field' => 'no_telp_rumah',
            'label' => 'No Telp Rumah',
            'rules' => 'integer',
        ),
        array(
            'field' => 'no_hp',
            'label' => 'No Hp',
            'rules' => 'integer',
        ),
        array(
            'field' => 'no_hp_ortu',
            'label' => 'No Hp Orang Tua',
            'rules' => 'integer',
        ),
        array(
            'field' => 'email',
            'label' => 'Nama',
            'rules' => 'valid_email',
        ),
        array(
            'field' => 'no_kps',
            'label' => 'No KPS',
            'rules' => 'integer',
        ),
        array(
            'field' => 'tinggi',
            'label' => 'Tinggi',
            'rules' => 'integer',
        ),
        array(
            'field' => 'berat',
            'label' => 'Berat',
            'rules' => 'integer',
        ),
        array(
            'field' => 'lintas_minat1',
            'label' => 'Lintas Minat 1',
            'rules' => 'differs[lintas_minat2]',
        ),
        array(
            'field' => 'lintas_minat2',
            'label' => 'Lintas Minat 2',
            'rules' => 'differs[lintas_minat1]',
        ),
        ),
    'login' => array(
        array(
            'field' => 'identity',
            'label' => 'Identity',
            'rules' => 'required',
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => "required|min_length[6]"
        ),
    ),
    'tahun_ajaran' => array(
        array(
            'field' => 'tahun_ajaran',
            'label' => 'Tahun Akademik',
            'rules' => 'required',
        ),
        array(
            'field' => 'begin_date',
            'label' => 'Tanggal Mulai',
            'rules' => 'required',
        ),
        array(
            'field' => 'end_date',
            'label' => 'Tanggal Berakhir',
            'rules' => 'required',
        ),
    ),
    'hari_libur' => array(
        array(
            'field' => 'tanggal',
            'label' => 'Tanggal',
            'rules' => 'required',
        ),
        array(
            'field' => 'keterangan',
            'label' => 'Keterangan',
            'rules' => 'required',
        ),
    ),
);
