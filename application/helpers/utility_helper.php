<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
    Function Name : asset_url
    Description   : with base_url that codeigniter already have, this make
                                    child function for asset
    Parameters    :
        - $url [String] = URL to File
 */

if ( ! function_exists('asset_url')) {
    function asset_url($url = null)
    {
        if ($url == null) {
            return base_url().'assets/';
        } else {
            return base_url().'assets/'.$url;
        }
    }
}

/*
    Function Name : public_asset
    Description   : return public folder
    Parameters    :
        - $url [String] = URL to File
 */

if ( ! function_exists('public_asset')) {
    function public_asset($url = null)
    {
        if ($url == null) {
            return asset_url().'public/';
        } else {
            return asset_url().'public/'.$url;
        }
    }
}

if ( ! function_exists('isset_val')) {
    function isset_val($var, $properties = null)
    {
        if (isset($var) && $var !== false && $var !== null) {
            if ($properties === null) {
                return $var;
            } elseif (is_array($var)) {
                if (isset($var[$properties])) {
                    return $var[$properties];
                } else {
                    return false;
                }
            } elseif (property_exists($var, $properties)) {
                return $var->$properties;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
